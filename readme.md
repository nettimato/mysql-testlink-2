# MySQL database for Testlink 1.9.14 container

This repository contains only docker container files for MySQL server with preseeded Testlink 1.9.14 database. 

* You should read [Testlink installation steps](https://gitlab.com/JAMKIT/Testlink/blob/master/readme.md)



* What is MySQL? Please [Browse and read](https://www.mysql.com/)
* What is Testlink? Please [browse and read](http://www.guru99.com/testlink-tutorial-complete-guide.html)
* What is docker container? Please [browse and read](https://www.docker.com/)



### Requirements: 


* We have been using Ubuntu 16.04 server
* Install [docker](https://www.docker.com/) + [docker compose](https://docs.docker.com/compose/) tools using *apt-get*
