# TestLink Open Source Project - http://testlink.sourceforge.net/
# This script is distributed under the GNU General Public License 2 or later.
# ---------------------------------------------------------------------------------------
# @filesource testlink_create_tables.sql
#
# SQL script - create all DB tables for MySQL
# tables are in alphabetic order  
#
# ATTENTION: do not use a different naming convention, that one already in use.
#
# IMPORTANT NOTE:
# each NEW TABLE added here NEED TO BE DEFINED in object.class.php getDBTables()
#
# IMPORTANT NOTE - DATETIME or TIMESTAMP
# Extracted from MySQL Manual
#
# The TIMESTAMP column type provides a type that you can use to automatically 
# mark INSERT or UPDATE operations with the current date and time. 
# If you have multiple TIMESTAMP columns in a table, only the first one is updated automatically.
#
# Knowing this is clear that we can use in interchangable way DATETIME or TIMESTAMP
#
# Naming convention for column regarding date/time of creation or change
#
# Right or wrong from TL 1.7 we have used
#
# creation_ts
# modification_ts
#
# Then no other naming convention has to be used as:
# create_ts, modified_ts
#
# CRITIC:
# Because this file will be processed during installation doing text replaces
# to add TABLE PREFIX NAME, any NEW DDL CODE added must be respect present
# convention regarding case and spaces between DDL keywords.
# 
# ---------------------------------------------------------------------------------------
# @internal revisions
#
# ---------------------------------------------------------------------------------------
CREATE DATABASE testlink;

USE testlink;

-- MySQL dump 10.13  Distrib 5.7.15, for Linux (x86_64)
--
-- Host: localhost    Database: testlink
-- ------------------------------------------------------
-- Server version	5.7.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assignment_status`
--

DROP TABLE IF EXISTS `assignment_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignment_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL DEFAULT 'unknown',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assignment_status`
--

LOCK TABLES `assignment_status` WRITE;
/*!40000 ALTER TABLE `assignment_status` DISABLE KEYS */;
INSERT INTO `assignment_status` VALUES (1,'open'),(2,'closed'),(3,'completed'),(4,'todo_urgent'),(5,'todo');
/*!40000 ALTER TABLE `assignment_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assignment_types`
--

DROP TABLE IF EXISTS `assignment_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignment_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_table` varchar(30) DEFAULT '',
  `description` varchar(100) NOT NULL DEFAULT 'unknown',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assignment_types`
--

LOCK TABLES `assignment_types` WRITE;
/*!40000 ALTER TABLE `assignment_types` DISABLE KEYS */;
INSERT INTO `assignment_types` VALUES (1,'testplan_tcversions','testcase_execution'),(2,'tcversions','testcase_review');
/*!40000 ALTER TABLE `assignment_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attachments`
--

DROP TABLE IF EXISTS `attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_id` int(10) unsigned NOT NULL DEFAULT '0',
  `fk_table` varchar(250) DEFAULT '',
  `title` varchar(250) DEFAULT '',
  `description` varchar(250) DEFAULT '',
  `file_name` varchar(250) NOT NULL DEFAULT '',
  `file_path` varchar(250) DEFAULT '',
  `file_size` int(11) NOT NULL DEFAULT '0',
  `file_type` varchar(250) NOT NULL DEFAULT '',
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `content` longblob,
  `compression_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `attachments_idx1` (`fk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attachments`
--

LOCK TABLES `attachments` WRITE;
/*!40000 ALTER TABLE `attachments` DISABLE KEYS */;
/*!40000 ALTER TABLE `attachments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `builds`
--

DROP TABLE IF EXISTS `builds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `builds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `testplan_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT 'undefined',
  `notes` text,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `is_open` tinyint(1) NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT NULL,
  `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `release_date` date DEFAULT NULL,
  `closed_on_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`testplan_id`,`name`),
  KEY `testplan_id` (`testplan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Available builds';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `builds`
--

LOCK TABLES `builds` WRITE;
/*!40000 ALTER TABLE `builds` DISABLE KEYS */;
INSERT INTO `builds` VALUES (1,641,'Test Build','<div class=\"versionTableRow\" data-reactid=\".1.0.1.3.0\" style=\"display: table-row; color: rgb(34, 26, 38); font-family: sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);\">\r\n	<div class=\"versionTableCell-left\" data-reactid=\".1.0.1.3.0.0\" style=\"display: table-cell; width: 183.636px;\">\r\n		Client versio</div>\r\n	<div class=\"versionTableCell\" data-reactid=\".1.0.1.3.0.1\" style=\"display: table-cell;\">\r\n		v1.2.129</div>\r\n</div>\r\n<div class=\"versionTableRow\" data-reactid=\".1.0.1.3.1\" style=\"display: table-row; color: rgb(34, 26, 38); font-family: sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);\">\r\n	<div class=\"versionTableCell-left\" data-reactid=\".1.0.1.3.1.0\" style=\"display: table-cell; width: 183.636px;\">\r\n		API versio</div>\r\n	<div class=\"versionTableCell\" data-reactid=\".1.0.1.3.1.1\" style=\"display: table-cell;\">\r\n		v0.4.44</div>\r\n</div>\r\n<div class=\"versionTableRow\" data-reactid=\".1.0.1.3.2\" style=\"display: table-row; color: rgb(34, 26, 38); font-family: sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);\">\r\n	<div class=\"versionTableCell-left\" data-reactid=\".1.0.1.3.2.0\" style=\"display: table-cell; width: 183.636px;\">\r\n		IMG versio</div>\r\n	<div class=\"versionTableCell\" data-reactid=\".1.0.1.3.2.1\" style=\"display: table-cell;\">\r\n		v0.1.10</div>\r\n</div>\r\n<p>\r\n	&nbsp;</p>\r\n',1,1,NULL,'2016-11-10 14:23:16','2016-11-10',NULL);
/*!40000 ALTER TABLE `builds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cfield_build_design_values`
--

DROP TABLE IF EXISTS `cfield_build_design_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cfield_build_design_values` (
  `field_id` int(10) NOT NULL DEFAULT '0',
  `node_id` int(10) NOT NULL DEFAULT '0',
  `value` varchar(4000) NOT NULL DEFAULT '',
  PRIMARY KEY (`field_id`,`node_id`),
  KEY `idx_cfield_build_design_values` (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfield_build_design_values`
--

LOCK TABLES `cfield_build_design_values` WRITE;
/*!40000 ALTER TABLE `cfield_build_design_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `cfield_build_design_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cfield_design_values`
--

DROP TABLE IF EXISTS `cfield_design_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cfield_design_values` (
  `field_id` int(10) NOT NULL DEFAULT '0',
  `node_id` int(10) NOT NULL DEFAULT '0',
  `value` varchar(4000) NOT NULL DEFAULT '',
  PRIMARY KEY (`field_id`,`node_id`),
  KEY `idx_cfield_design_values` (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfield_design_values`
--

LOCK TABLES `cfield_design_values` WRITE;
/*!40000 ALTER TABLE `cfield_design_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `cfield_design_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cfield_execution_values`
--

DROP TABLE IF EXISTS `cfield_execution_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cfield_execution_values` (
  `field_id` int(10) NOT NULL DEFAULT '0',
  `execution_id` int(10) NOT NULL DEFAULT '0',
  `testplan_id` int(10) NOT NULL DEFAULT '0',
  `tcversion_id` int(10) NOT NULL DEFAULT '0',
  `value` varchar(4000) NOT NULL DEFAULT '',
  PRIMARY KEY (`field_id`,`execution_id`,`testplan_id`,`tcversion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfield_execution_values`
--

LOCK TABLES `cfield_execution_values` WRITE;
/*!40000 ALTER TABLE `cfield_execution_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `cfield_execution_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cfield_node_types`
--

DROP TABLE IF EXISTS `cfield_node_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cfield_node_types` (
  `field_id` int(10) NOT NULL DEFAULT '0',
  `node_type_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_id`,`node_type_id`),
  KEY `idx_custom_fields_assign` (`node_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfield_node_types`
--

LOCK TABLES `cfield_node_types` WRITE;
/*!40000 ALTER TABLE `cfield_node_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `cfield_node_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cfield_testplan_design_values`
--

DROP TABLE IF EXISTS `cfield_testplan_design_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cfield_testplan_design_values` (
  `field_id` int(10) NOT NULL DEFAULT '0',
  `link_id` int(10) NOT NULL DEFAULT '0' COMMENT 'point to testplan_tcversion id',
  `value` varchar(4000) NOT NULL DEFAULT '',
  PRIMARY KEY (`field_id`,`link_id`),
  KEY `idx_cfield_tplan_design_val` (`link_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfield_testplan_design_values`
--

LOCK TABLES `cfield_testplan_design_values` WRITE;
/*!40000 ALTER TABLE `cfield_testplan_design_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `cfield_testplan_design_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cfield_testprojects`
--

DROP TABLE IF EXISTS `cfield_testprojects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cfield_testprojects` (
  `field_id` int(10) unsigned NOT NULL DEFAULT '0',
  `testproject_id` int(10) unsigned NOT NULL DEFAULT '0',
  `display_order` smallint(5) unsigned NOT NULL DEFAULT '1',
  `location` smallint(5) unsigned NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `required_on_design` tinyint(1) NOT NULL DEFAULT '0',
  `required_on_execution` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_id`,`testproject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfield_testprojects`
--

LOCK TABLES `cfield_testprojects` WRITE;
/*!40000 ALTER TABLE `cfield_testprojects` DISABLE KEYS */;
/*!40000 ALTER TABLE `cfield_testprojects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_fields`
--

DROP TABLE IF EXISTS `custom_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_fields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  `label` varchar(64) NOT NULL DEFAULT '' COMMENT 'label to display on user interface',
  `type` smallint(6) NOT NULL DEFAULT '0',
  `possible_values` varchar(4000) NOT NULL DEFAULT '',
  `default_value` varchar(4000) NOT NULL DEFAULT '',
  `valid_regexp` varchar(255) NOT NULL DEFAULT '',
  `length_min` int(10) NOT NULL DEFAULT '0',
  `length_max` int(10) NOT NULL DEFAULT '0',
  `show_on_design` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1=> show it during specification design',
  `enable_on_design` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1=> user can write/manage it during specification design',
  `show_on_execution` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '1=> show it during test case execution',
  `enable_on_execution` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '1=> user can write/manage it during test case execution',
  `show_on_testplan_design` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `enable_on_testplan_design` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_custom_fields_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_fields`
--

LOCK TABLES `custom_fields` WRITE;
/*!40000 ALTER TABLE `custom_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_version`
--

DROP TABLE IF EXISTS `db_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_version` (
  `version` varchar(50) NOT NULL DEFAULT 'unknown',
  `upgrade_ts` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `notes` text,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_version`
--

LOCK TABLES `db_version` WRITE;
/*!40000 ALTER TABLE `db_version` DISABLE KEYS */;
INSERT INTO `db_version` VALUES ('DB 1.9.14','2016-11-10 13:21:42','TestLink 1.9.14');
/*!40000 ALTER TABLE `db_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` int(10) unsigned NOT NULL DEFAULT '0',
  `log_level` smallint(5) unsigned NOT NULL DEFAULT '0',
  `source` varchar(45) DEFAULT NULL,
  `description` text NOT NULL,
  `fired_at` int(10) unsigned NOT NULL DEFAULT '0',
  `activity` varchar(45) DEFAULT NULL,
  `object_id` int(10) unsigned DEFAULT NULL,
  `object_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `fired_at` (`fired_at`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,1,32,'GUI','string \'show_hide_direct_link\' is not localized for locale \'fi_FI\'  - using en_GB',1478784166,'LOCALIZATION',0,NULL),(2,1,32,'GUI','string \'login\' is not localized for locale \'fi_FI\'  - using en_GB',1478784166,'LOCALIZATION',0,NULL),(3,1,32,'GUI','string \'demo_usage\' is not localized for locale \'fi_FI\'  - using en_GB',1478784166,'LOCALIZATION',0,NULL),(4,1,32,'GUI','string \'demo_mode_suggested_user\' is not localized for locale \'fi_FI\'  - using en_GB',1478784166,'LOCALIZATION',0,NULL),(5,1,32,'GUI','string \'demo_mode_suggested_password\' is not localized for locale \'fi_FI\'  - using en_GB',1478784166,'LOCALIZATION',0,NULL),(6,2,32,'GUI','string \'unknown_authentication_method\' is not localized for locale \'fi_FI\'  - using en_GB',1478784169,'LOCALIZATION',0,NULL),(7,3,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:21:\"audit_login_succeeded\";s:6:\"params\";a:2:{i:0;s:5:\"admin\";i:1;s:10:\"172.17.0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478784169,'LOGIN',1,'users'),(8,4,2,'GUI','No project found: Assume a new installation and redirect to create it',1478784169,NULL,0,NULL),(9,5,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:25:\"audit_testproject_created\";s:6:\"params\";a:1:{i:0;s:5:\"muumi\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478784177,'CREATE',1,'testprojects'),(10,6,2,'GUI','E_NOTICE\nUndefined index: id - in /var/www/html/testlink/gui/templates_c/63e7660b3a6460240f9666f2a1d6034e687cbb11.file.issueTrackerEdit.tpl.php - Line 106',1478784748,'PHP',0,NULL),(11,6,2,'GUI','E_NOTICE\nUndefined index: id - in /var/www/html/testlink/gui/templates_c/63e7660b3a6460240f9666f2a1d6034e687cbb11.file.issueTrackerEdit.tpl.php - Line 191',1478784748,'PHP',0,NULL),(12,7,2,'GUI','E_NOTICE\nUndefined index: id - in /var/www/html/testlink/gui/templates_c/63e7660b3a6460240f9666f2a1d6034e687cbb11.file.issueTrackerEdit.tpl.php - Line 106',1478784765,'PHP',0,NULL),(13,7,2,'GUI','E_NOTICE\nUndefined index: id - in /var/www/html/testlink/gui/templates_c/63e7660b3a6460240f9666f2a1d6034e687cbb11.file.issueTrackerEdit.tpl.php - Line 191',1478784765,'PHP',0,NULL),(14,8,2,'GUI','E_NOTICE\nUndefined index: id - in /var/www/html/testlink/gui/templates_c/63e7660b3a6460240f9666f2a1d6034e687cbb11.file.issueTrackerEdit.tpl.php - Line 106',1478784774,'PHP',0,NULL),(15,8,2,'GUI','E_NOTICE\nUndefined index: id - in /var/www/html/testlink/gui/templates_c/63e7660b3a6460240f9666f2a1d6034e687cbb11.file.issueTrackerEdit.tpl.php - Line 191',1478784774,'PHP',0,NULL),(16,9,2,'GUI','E_NOTICE\nUndefined index: id - in /var/www/html/testlink/gui/templates_c/63e7660b3a6460240f9666f2a1d6034e687cbb11.file.issueTrackerEdit.tpl.php - Line 106',1478784780,'PHP',0,NULL),(17,9,2,'GUI','E_NOTICE\nUndefined index: id - in /var/www/html/testlink/gui/templates_c/63e7660b3a6460240f9666f2a1d6034e687cbb11.file.issueTrackerEdit.tpl.php - Line 191',1478784780,'PHP',0,NULL),(18,10,2,'GUI','E_NOTICE\nUndefined index: id - in /var/www/html/testlink/gui/templates_c/63e7660b3a6460240f9666f2a1d6034e687cbb11.file.issueTrackerEdit.tpl.php - Line 106',1478784793,'PHP',0,NULL),(19,10,2,'GUI','E_NOTICE\nUndefined index: id - in /var/www/html/testlink/gui/templates_c/63e7660b3a6460240f9666f2a1d6034e687cbb11.file.issueTrackerEdit.tpl.php - Line 191',1478784793,'PHP',0,NULL),(20,11,2,'GUI','E_WARNING\nSoapClient::SoapClient(): php_network_getaddresses: getaddrinfo failed: Name or service not known - in /var/www/html/testlink/lib/issuetrackerintegration/mantissoapInterface.class.php - Line 118',1478784859,'PHP',0,NULL),(21,11,2,'GUI','E_WARNING\nSoapClient::SoapClient(http://www.mantisbt.org/api/soap/mantisconnect.php?wsdl): failed to open stream: php_network_getaddresses: getaddrinfo failed: Name or service not known - in /var/www/html/testlink/lib/issuetrackerintegration/mantissoapInterface.class.php - Line 118',1478784859,'PHP',0,NULL),(22,11,1,'GUI','SOAP Fault: (code: WSDL, string: SOAP-ERROR: Parsing WSDL: Couldn\'t load from \'http://www.mantisbt.org/api/soap/mantisconnect.php?wsdl\' : failed to load external entity \"http://www.mantisbt.org/api/soap/mantisconnect.php?wsdl\"\n)',1478784859,NULL,0,NULL),(23,12,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:25:\"audit_testproject_created\";s:6:\"params\";a:1:{i:0;s:12:\"Freenest 1.4\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478785568,'CREATE',2,'testprojects'),(24,13,2,'GUI','E_NOTICE\nUndefined index: help - in /var/www/html/testlink/gui/templates_c/c126309d81c6087932793b988c561185a5b2a23e.file.projectEdit.tpl.php - Line 133',1478785570,'PHP',0,NULL),(25,14,2,'GUI','E_NOTICE\nUndefined index: help - in /var/www/html/testlink/gui/templates_c/c126309d81c6087932793b988c561185a5b2a23e.file.projectEdit.tpl.php - Line 133',1478787590,'PHP',0,NULL),(26,15,2,'GUI','E_WARNING\narray_keys() expects parameter 1 to be array, null given - in /var/www/html/testlink/lib/functions/testproject.class.php - Line 1223',1478787606,'PHP',0,NULL),(27,15,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:40:\"audit_all_user_roles_removed_testproject\";s:6:\"params\";a:1:{i:0;s:5:\"muumi\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787606,'ASSIGN',1,'testprojects'),(28,15,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:25:\"audit_testproject_deleted\";s:6:\"params\";a:1:{i:0;s:5:\"muumi\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787606,'DELETE',1,'testprojects'),(29,16,2,'GUI','E_NOTICE\nUndefined index: help - in /var/www/html/testlink/gui/templates_c/c126309d81c6087932793b988c561185a5b2a23e.file.projectEdit.tpl.php - Line 133',1478787608,'PHP',0,NULL),(30,17,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:23:\"audit_testproject_saved\";s:6:\"params\";a:1:{i:0;s:11:\"Contriboard\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787635,'UPDATE',2,'testprojects'),(31,18,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:22:\"audit_testplan_created\";s:6:\"params\";a:2:{i:0;s:11:\"Contriboard\";i:1;s:21:\"Acceptance Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787666,'CREATED',640,'testplans'),(32,19,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:22:\"audit_testplan_created\";s:6:\"params\";a:2:{i:0;s:11:\"Contriboard\";i:1;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787700,'CREATED',641,'testplans'),(33,20,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:35:\"TC-408 : Robot Framework Scenario 1\";i:1;s:1:\"1\";i:2;s:21:\"Acceptance Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787718,'ASSIGN',640,'testplans'),(34,20,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:35:\"TC-409 : Robot Framework Scenario 2\";i:1;s:1:\"1\";i:2;s:21:\"Acceptance Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787718,'ASSIGN',640,'testplans'),(35,20,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:35:\"TC-410 : Robot Framework Scenario 3\";i:1;s:1:\"1\";i:2;s:21:\"Acceptance Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787718,'ASSIGN',640,'testplans'),(36,20,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:35:\"TC-411 : Robot Framework Scenario 4\";i:1;s:1:\"1\";i:2;s:21:\"Acceptance Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787718,'ASSIGN',640,'testplans'),(37,20,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:29:\"TC-412 : Scenario 3 execution\";i:1;s:1:\"1\";i:2;s:21:\"Acceptance Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787718,'ASSIGN',640,'testplans'),(38,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:79:\"TC-229 : Verify Testing magnet feature functionality with 2 simultaneous users.\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787741,'ASSIGN',641,'testplans'),(39,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:51:\"TC-216 : Verify Naming boards ~200 characters long.\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787741,'ASSIGN',641,'testplans'),(40,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:55:\"TC-53 : Verify User can go back to workspace from board\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787741,'ASSIGN',641,'testplans'),(41,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:38:\"TC-52 : Verify User can delete a board\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787741,'ASSIGN',641,'testplans'),(42,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:49:\"TC-51 : Verify User can close the board edit menu\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787741,'ASSIGN',641,'testplans'),(43,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:29:\"TC-136 : Verify UTF-8 Support\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787741,'ASSIGN',641,'testplans'),(44,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:44:\"TC-339 : Verify Changing the name of a board\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787741,'ASSIGN',641,'testplans'),(45,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:42:\"TC-340 : Verify Changing boards background\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787741,'ASSIGN',641,'testplans'),(46,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:44:\"TC-22 : Verify user can\'t open deleted board\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787741,'ASSIGN',641,'testplans'),(47,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:56:\"TC-62 : Verify Deleting a board while it is being edited\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(48,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:60:\"TC-352 : Verify clicking empty space while creating a ticket\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(49,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:61:\"TC-418 : Verify user can not enter plain board with wrong url\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(50,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:75:\"TC-355 : Verify clicking empty space while workflow templates window opened\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(51,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:38:\"TC-70 : Verify Open board to a new tab\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(52,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:29:\"TC-413 : Verify Board Scaling\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(53,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:37:\"TC-414 : Verify Umlauts in board name\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(54,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:44:\"TC-424 : Verify board members work correctly\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(55,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:43:\"TC-268 : Verify Board creation and deletion\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(56,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:28:\"TC-371 : Verify Rename board\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(57,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:40:\"TC-237 : Verify Check board name updates\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(58,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:36:\"TC-276 : Verify Creating empty board\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(59,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:44:\"TC-427 : Verify shared questboard visibility\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(60,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:46:\"TC-428 : Verify register with facebook account\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(61,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:47:\"TC-126 : Verify Account creating and Login <!!>\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(62,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:61:\"TC-134 : Verify Registration with password under 8 characters\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(63,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:29:\"TC-88 : Verify Testing logout\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(64,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:58:\"TC-372 : Verify Registering with existing email & password\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(65,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:105:\"TC-220 : Verify Escaping critical special characters in password field, while registering to Contriboard.\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(66,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:28:\"TC-373 : Verify Registration\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(67,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:35:\"TC-374 : Verify Login to an account\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(68,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:33:\"TC-380 : Verify Changing password\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(69,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:29:\"TC-84 : Verify Password match\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(70,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:57:\"TC-231 : Verify user has valid email in registration form\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(71,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:48:\"TC-271 : Verify Logging in with invalid password\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(72,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:41:\"TC-243 : Verify Access check after logout\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(73,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:50:\"TC-142 : Verify Modify Content with Old User-Token\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(74,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:44:\"TC-422 : Verify register with google account\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(75,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:46:\"TC-425 : Verify avatar changing work correctly\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(76,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:45:\"TC-385 : Sending message to technical support\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(77,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:30:\"TC-386 : Changing localization\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(78,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:30:\"TC-387 : Changing the language\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(79,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:49:\"TC-284 : Verify that user can change ticket color\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(80,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:28:\"TC-17 : Verify Board sharing\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(81,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:99:\"TC-222 : Verify Editing board name and providing link for another user(before the name is changed))\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(82,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:55:\"TC-258 : Verify Viewing deleted board by sharing link 2\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(83,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:56:\"TC-248 : Verify board not viewable after link is cleared\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(84,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:37:\"TC-236 : Verify Clear board share URL\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(85,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:76:\"TC-304 : Verify that guest users are logged out when shared link is removed.\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(86,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:41:\"TC-253 : Verify Creating multiple tickets\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(87,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:28:\"TC-185 : Verify Snap to Grid\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(88,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:51:\"TC-350 : Verify Moving tickets outside of the board\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(89,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:60:\"TC-367 : Verify Creating a ticket with a huge amount of text\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(90,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:60:\"TC-223 : Verify Typing ticket headings ~200 characters long.\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(91,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:60:\"TC-396 : Verify moving ticket using 2 or more browsers /tabs\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(92,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:57:\"TC-80 : Verify Create ticket with special char in heading\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(93,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:47:\"TC-285 : Verify that changing ticket name works\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(94,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:39:\"TC-49 : Verify User can delete a ticket\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(95,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:46:\"TC-397 : Verify Simultaneous editing of ticket\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(96,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:34:\"TC-79 : Verify Change ticket color\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(97,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:50:\"TC-47 : Verify User can edit the color of a ticket\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(98,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:62:\"TC-260 : Verify Modifying a ticket when owner delete the board\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(99,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:68:\"TC-261 : Verify Modifying a ticket when owner clear the sharing link\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(100,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:40:\"TC-24 : Verify ticket name word wrapping\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(101,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:48:\"TC-57 : Verify Tickets overlapping functionality\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(102,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:55:\"TC-263 : Verify Multiple users deleting the same ticket\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(103,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:36:\"TC-145 : Verify Move ticket position\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787742,'ASSIGN',641,'testplans'),(104,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:34:\"TC-67 : Verify Ticket minimap test\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(105,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:57:\"TC-61 : Verify Deleting a ticket while it is being edited\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(106,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:35:\"TC-82 : Verify Minimap ticket color\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(107,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:32:\"TC-405 : Verify Edit ticket text\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(108,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:37:\"TC-241 : Verify Empty ticket creation\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(109,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:54:\"TC-93 : Verify Ticket interaction with template change\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(110,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:29:\"TC-419 : Verify Markdown text\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(111,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:48:\"TC-420 : Verify that markdown works as designed.\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(112,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:32:\"TC-426 : Verify ticket reviewing\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(113,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:31:\"TC-20 : Verify export JSON-file\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(114,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:30:\"TC-18 : Verify export CSV-file\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(115,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:29:\"TC-407 : Verify export dialog\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(116,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:28:\"TC-423 : Verify image export\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(117,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:32:\"TC-429 : Verify ticket duplicate\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(118,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:30:\"TC-421 : Verify adding comment\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(119,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:40:\"TC-247 : Verify guest live viewing works\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(120,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:51:\"TC-40 : Verify Guest can edit the color of a ticket\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(121,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:50:\"TC-41 : Verify Guest can edit the name of a ticket\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(122,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:40:\"TC-42 : Verify Guest can delete a ticket\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(123,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:39:\"TC-43 : Verify Guest can access a board\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(124,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:32:\"TC-415 : Verify Help Layer works\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(125,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:63:\"TC-416 : Verify Help Layer works with different browsers/device\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(126,21,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:26:\"audit_tc_added_to_testplan\";s:6:\"params\";a:3:{i:0;s:21:\"TC-417 : Verify slide\";i:1;s:1:\"1\";i:2;s:28:\"Functional System Tests V0.1\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787743,'ASSIGN',641,'testplans'),(127,22,16,'GUI','O:18:\"tlMetaStringHelper\":4:{s:5:\"label\";s:19:\"audit_build_created\";s:6:\"params\";a:3:{i:0;s:11:\"Contriboard\";i:1;s:28:\"Functional System Tests V0.1\";i:2;s:10:\"Test Build\";}s:13:\"bDontLocalize\";b:0;s:14:\"bDontFireEvent\";b:0;}',1478787796,'CREATE',1,'builds');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `execution_bugs`
--

DROP TABLE IF EXISTS `execution_bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `execution_bugs` (
  `execution_id` int(10) unsigned NOT NULL DEFAULT '0',
  `bug_id` varchar(64) NOT NULL DEFAULT '0',
  PRIMARY KEY (`execution_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `execution_bugs`
--

LOCK TABLES `execution_bugs` WRITE;
/*!40000 ALTER TABLE `execution_bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `execution_bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `execution_tcsteps`
--

DROP TABLE IF EXISTS `execution_tcsteps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `execution_tcsteps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `execution_id` int(10) unsigned NOT NULL DEFAULT '0',
  `tcstep_id` int(10) unsigned NOT NULL DEFAULT '0',
  `notes` text,
  `status` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `execution_tcsteps_idx1` (`execution_id`,`tcstep_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `execution_tcsteps`
--

LOCK TABLES `execution_tcsteps` WRITE;
/*!40000 ALTER TABLE `execution_tcsteps` DISABLE KEYS */;
/*!40000 ALTER TABLE `execution_tcsteps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `executions`
--

DROP TABLE IF EXISTS `executions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `executions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `build_id` int(10) NOT NULL DEFAULT '0',
  `tester_id` int(10) unsigned DEFAULT NULL,
  `execution_ts` datetime DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `testplan_id` int(10) unsigned NOT NULL DEFAULT '0',
  `tcversion_id` int(10) unsigned NOT NULL DEFAULT '0',
  `tcversion_number` smallint(5) unsigned NOT NULL DEFAULT '1',
  `platform_id` int(10) unsigned NOT NULL DEFAULT '0',
  `execution_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 -> manual, 2 -> automated',
  `execution_duration` decimal(6,2) DEFAULT NULL COMMENT 'NULL will be considered as NO DATA Provided by user',
  `notes` text,
  PRIMARY KEY (`id`),
  KEY `executions_idx1` (`testplan_id`,`tcversion_id`,`platform_id`,`build_id`),
  KEY `executions_idx2` (`execution_type`),
  KEY `executions_idx3` (`tcversion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `executions`
--

LOCK TABLES `executions` WRITE;
/*!40000 ALTER TABLE `executions` DISABLE KEYS */;
/*!40000 ALTER TABLE `executions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory`
--

DROP TABLE IF EXISTS `inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `testproject_id` int(10) unsigned NOT NULL,
  `owner_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `ipaddress` varchar(255) NOT NULL,
  `content` text,
  `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modification_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `inventory_idx1` (`testproject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory`
--

LOCK TABLES `inventory` WRITE;
/*!40000 ALTER TABLE `inventory` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `issuetrackers`
--

DROP TABLE IF EXISTS `issuetrackers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issuetrackers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` int(10) DEFAULT '0',
  `cfg` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `issuetrackers_uidx1` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `issuetrackers`
--

LOCK TABLES `issuetrackers` WRITE;
/*!40000 ALTER TABLE `issuetrackers` DISABLE KEYS */;
INSERT INTO `issuetrackers` VALUES (1,'rest',7,'<!-- Template jirarestInterface -->\r\n<issuetracker>\r\n<username>tluser</username>\r\n<password>tlu$er</password>\r\n<uribase>http://192.168.9.211:8080/</uribase>\r\n<uriapi>http://192.168.9.211:8080/rest/api/latest/</uriapi>\r\n<uriview>http://192.168.9.211:8080/browse/</uriview>\r\n<userinteraction>0</userinteraction>\r\n<!-- 1: User will be able to manage following attributes from GUI -->\r\n<!-- Issue Type, Issue Priority, Affects Versions, Components -->\r\n<!-- 0: values for attributes will be taken FROM this config XML from GUI -->\r\n</issuetracker>'),(2,'asdf',3,'<issuetracker>\r\n<username>MANTIS LOGIN NAME</username>\r\n<password>MANTIS PASSWORD</password>\r\n<uribase>http://www.mantisbt.org/</uribase>\r\n</issuetracker>');
/*!40000 ALTER TABLE `issuetrackers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keywords`
--

DROP TABLE IF EXISTS `keywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keywords` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `keyword` varchar(100) NOT NULL DEFAULT '',
  `testproject_id` int(10) unsigned NOT NULL DEFAULT '0',
  `notes` text,
  PRIMARY KEY (`id`),
  KEY `testproject_id` (`testproject_id`),
  KEY `keyword` (`keyword`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keywords`
--

LOCK TABLES `keywords` WRITE;
/*!40000 ALTER TABLE `keywords` DISABLE KEYS */;
/*!40000 ALTER TABLE `keywords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `milestones`
--

DROP TABLE IF EXISTS `milestones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `milestones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `testplan_id` int(10) unsigned NOT NULL DEFAULT '0',
  `target_date` date DEFAULT NULL,
  `start_date` date NOT NULL DEFAULT '1000-01-01',
  `a` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `b` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `c` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT 'undefined',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_testplan_id` (`name`,`testplan_id`),
  KEY `testplan_id` (`testplan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `milestones`
--

LOCK TABLES `milestones` WRITE;
/*!40000 ALTER TABLE `milestones` DISABLE KEYS */;
/*!40000 ALTER TABLE `milestones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `node_types`
--

DROP TABLE IF EXISTS `node_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `node_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL DEFAULT 'testproject',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `node_types`
--

LOCK TABLES `node_types` WRITE;
/*!40000 ALTER TABLE `node_types` DISABLE KEYS */;
INSERT INTO `node_types` VALUES (1,'testproject'),(2,'testsuite'),(3,'testcase'),(4,'testcase_version'),(5,'testplan'),(6,'requirement_spec'),(7,'requirement'),(8,'requirement_version'),(9,'testcase_step'),(10,'requirement_revision'),(11,'requirement_spec_revision'),(12,'build'),(13,'platform'),(14,'user');
/*!40000 ALTER TABLE `node_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nodes_hierarchy`
--

DROP TABLE IF EXISTS `nodes_hierarchy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nodes_hierarchy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `node_type_id` int(10) unsigned NOT NULL DEFAULT '1',
  `node_order` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pid_m_nodeorder` (`parent_id`,`node_order`)
) ENGINE=InnoDB AUTO_INCREMENT=642 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nodes_hierarchy`
--

LOCK TABLES `nodes_hierarchy` WRITE;
/*!40000 ALTER TABLE `nodes_hierarchy` DISABLE KEYS */;
INSERT INTO `nodes_hierarchy` VALUES (2,'Contriboard',NULL,1,1),(3,'Master Test Plan',2,2,0),(4,'Master Test Plan For Contriboard',3,3,1000),(5,'',4,4,0),(6,'Testcase Pool',2,2,1),(7,'Acceptance Testing',6,2,0),(8,'Robot Framework Scenario 1',7,3,100),(9,'',8,4,0),(10,'Robot Framework Scenario 2',7,3,101),(11,'',10,4,0),(12,'Robot Framework Scenario 3',7,3,102),(13,'',12,4,0),(14,'Robot Framework Scenario 4',7,3,103),(15,'',14,4,0),(16,'Scenario 3 execution',7,3,104),(17,'',16,4,0),(18,'System Testing',6,2,1),(19,'Functional Testing',18,2,0),(20,'Board',19,2,1),(21,'Verify Testing magnet feature functionality with 2 simultaneous users.',20,3,0),(22,'',21,4,0),(23,'',22,9,0),(24,'',22,9,0),(25,'',22,9,0),(26,'',22,9,0),(27,'',22,9,0),(28,'',22,9,0),(29,'',22,9,0),(30,'Verify Naming boards ~200 characters long.',20,3,1),(31,'',30,4,0),(32,'',31,9,0),(33,'',31,9,0),(34,'',31,9,0),(35,'',31,9,0),(36,'',31,9,0),(37,'Verify User can go back to workspace from board',20,3,2),(38,'',37,4,0),(39,'',38,9,0),(40,'',38,9,0),(41,'',38,9,0),(42,'Verify User can delete a board',20,3,3),(43,'',42,4,0),(44,'',43,9,0),(45,'',43,9,0),(46,'',43,9,0),(47,'',43,9,0),(48,'Verify User can close the board edit menu',20,3,4),(49,'',48,4,0),(50,'',49,9,0),(51,'',49,9,0),(52,'',49,9,0),(53,'Verify UTF-8 Support',20,3,5),(54,'',53,4,0),(55,'',54,9,0),(56,'',54,9,0),(57,'',54,9,0),(58,'Verify Changing the name of a board',20,3,6),(59,'',58,4,0),(60,'',59,9,0),(61,'',59,9,0),(62,'',59,9,0),(63,'',59,9,0),(64,'Verify Changing boards background',20,3,7),(65,'',64,4,0),(66,'',65,9,0),(67,'',65,9,0),(68,'',65,9,0),(69,'Verify user can\'t open deleted board',20,3,8),(70,'',69,4,0),(71,'',70,9,0),(72,'',70,9,0),(73,'',70,9,0),(74,'',70,9,0),(75,'Verify Deleting a board while it is being edited',20,3,9),(76,'',75,4,0),(77,'',76,9,0),(78,'',76,9,0),(79,'Verify clicking empty space while creating a ticket',20,3,10),(80,'',79,4,0),(81,'',80,9,0),(82,'',80,9,0),(83,'Verify user can not enter plain board with wrong url',20,3,11),(84,'',83,4,0),(85,'',84,9,0),(86,'',84,9,0),(87,'Verify clicking empty space while workflow templates window opened',20,3,12),(88,'',87,4,0),(89,'',88,9,0),(90,'',88,9,0),(91,'Verify Open board to a new tab',20,3,13),(92,'',91,4,0),(93,'',92,9,0),(94,'Verify Board Scaling',20,3,14),(95,'',94,4,0),(96,'',95,9,0),(97,'',95,9,0),(98,'',95,9,0),(99,'Verify Umlauts in board name',20,3,15),(100,'',99,4,0),(101,'',100,9,0),(102,'',100,9,0),(103,'Verify board members work correctly',20,3,16),(104,'',103,4,0),(105,'',104,9,0),(106,'',104,9,0),(107,'',104,9,0),(108,'',104,9,0),(109,'Workspace',19,2,2),(110,'Verify Board creation and deletion',109,3,1),(111,'',110,4,0),(112,'',111,9,0),(113,'',111,9,0),(114,'',111,9,0),(115,'',111,9,0),(116,'Verify Rename board',109,3,2),(117,'',116,4,0),(118,'',117,9,0),(119,'',117,9,0),(120,'',117,9,0),(121,'',117,9,0),(122,'Verify Check board name updates',109,3,3),(123,'',122,4,0),(124,'',123,9,0),(125,'',123,9,0),(126,'',123,9,0),(127,'',123,9,0),(128,'Verify Creating empty board',109,3,4),(129,'',128,4,0),(130,'',129,9,0),(131,'',129,9,0),(132,'Verify shared questboard visibility',109,3,5),(133,'',132,4,0),(134,'',133,9,0),(135,'',133,9,0),(136,'',133,9,0),(137,'',133,9,0),(138,'Authentication',19,2,3),(139,'Verify register with facebook account',138,3,0),(140,'',139,4,0),(141,'',140,9,0),(142,'',140,9,0),(143,'',140,9,0),(144,'Verify Account creating and Login <!!>',138,3,1),(145,'',144,4,0),(146,'',145,9,0),(147,'',145,9,0),(148,'',145,9,0),(149,'',145,9,0),(150,'',145,9,0),(151,'',145,9,0),(152,'',145,9,0),(153,'',145,9,0),(154,'Verify Registration with password under 8 characters',138,3,2),(155,'',154,4,0),(156,'',155,9,0),(157,'',155,9,0),(158,'',155,9,0),(159,'Verify Testing logout',138,3,3),(160,'',159,4,0),(161,'',160,9,0),(162,'',160,9,0),(163,'',160,9,0),(164,'Verify Registering with existing email & password',138,3,4),(165,'',164,4,0),(166,'',165,9,0),(167,'',165,9,0),(168,'Verify Escaping critical special characters in password field, while registering to Contriboard.',138,3,5),(169,'',168,4,0),(170,'',169,9,0),(171,'',169,9,0),(172,'',169,9,0),(173,'',169,9,0),(174,'',169,9,0),(175,'',169,9,0),(176,'',169,9,0),(177,'Verify Registration',138,3,6),(178,'',177,4,0),(179,'',178,9,0),(180,'',178,9,0),(181,'',178,9,0),(182,'',178,9,0),(183,'Verify Login to an account',138,3,7),(184,'',183,4,0),(185,'',184,9,0),(186,'',184,9,0),(187,'',184,9,0),(188,'Verify Changing password',138,3,8),(189,'',188,4,0),(190,'',189,9,0),(191,'',189,9,0),(192,'Verify Password match',138,3,9),(193,'',192,4,0),(194,'',193,9,0),(195,'',193,9,0),(196,'',193,9,0),(197,'',193,9,0),(198,'',193,9,0),(199,'',193,9,0),(200,'',193,9,0),(201,'',193,9,0),(202,'Verify user has valid email in registration form',138,3,10),(203,'',202,4,0),(204,'',203,9,0),(205,'',203,9,0),(206,'',203,9,0),(207,'Verify Logging in with invalid password',138,3,11),(208,'',207,4,0),(209,'',208,9,0),(210,'',208,9,0),(211,'',208,9,0),(212,'Verify Access check after logout',138,3,12),(213,'',212,4,0),(214,'',213,9,0),(215,'',213,9,0),(216,'Verify Modify Content with Old User-Token',138,3,13),(217,'',216,4,0),(218,'',217,9,0),(219,'',217,9,0),(220,'',217,9,0),(221,'',217,9,0),(222,'',217,9,0),(223,'',217,9,0),(224,'',217,9,0),(225,'',217,9,0),(226,'',217,9,0),(227,'',217,9,0),(228,'',217,9,0),(229,'',217,9,0),(230,'',217,9,0),(231,'',217,9,0),(232,'',217,9,0),(233,'',217,9,0),(234,'Verify register with google account',138,3,14),(235,'',234,4,0),(236,'',235,9,0),(237,'',235,9,0),(238,'',235,9,0),(239,'Verify avatar changing work correctly',138,3,15),(240,'',239,4,0),(241,'',240,9,0),(242,'',240,9,0),(243,'',240,9,0),(244,'',240,9,0),(245,'',240,9,0),(246,'',240,9,0),(247,'',240,9,0),(248,'',240,9,0),(249,'',240,9,0),(250,'Administration',19,2,4),(251,'Sending message to technical support',250,3,0),(252,'',251,4,0),(253,'',252,9,0),(254,'',252,9,0),(255,'',252,9,0),(256,'Localization',19,2,5),(257,'Changing localization',256,3,0),(258,'',257,4,0),(259,'',258,9,0),(260,'',258,9,0),(261,'',258,9,0),(262,'Changing the language',256,3,6),(263,'',262,4,0),(264,'',263,9,0),(265,'',263,9,0),(266,'Color picker',19,2,7),(267,'Verify that user can change ticket color',266,3,101),(268,'',267,4,0),(269,'',268,9,0),(270,'',268,9,0),(271,'',268,9,0),(272,'',268,9,0),(273,'New Relic integration',19,2,8),(274,'Sharing boards by url',19,2,11),(275,'Verify Board sharing',274,3,0),(276,'',275,4,0),(277,'',276,9,0),(278,'',276,9,0),(279,'',276,9,0),(280,'',276,9,0),(281,'',276,9,0),(282,'',276,9,0),(283,'',276,9,0),(284,'',276,9,0),(285,'Verify Editing board name and providing link for another user(before the name is changed))',274,3,2),(286,'',285,4,0),(287,'',286,9,0),(288,'',286,9,0),(289,'',286,9,0),(290,'',286,9,0),(291,'',286,9,0),(292,'',286,9,0),(293,'',286,9,0),(294,'',286,9,0),(295,'',286,9,0),(296,'',286,9,0),(297,'',286,9,0),(298,'Verify Viewing deleted board by sharing link 2',274,3,5),(299,'',298,4,0),(300,'',299,9,0),(301,'',299,9,0),(302,'',299,9,0),(303,'Verify board not viewable after link is cleared',274,3,9),(304,'',303,4,0),(305,'',304,9,0),(306,'',304,9,0),(307,'',304,9,0),(308,'',304,9,0),(309,'Verify Clear board share URL',274,3,11),(310,'',309,4,0),(311,'',310,9,0),(312,'',310,9,0),(313,'Verify that guest users are logged out when shared link is removed.',274,3,12),(314,'',313,4,0),(315,'',314,9,0),(316,'',314,9,0),(317,'',314,9,0),(318,'',314,9,0),(319,'',314,9,0),(320,'',314,9,0),(321,'',314,9,0),(322,'',314,9,0),(323,'',314,9,0),(324,'',314,9,0),(325,'',314,9,0),(326,'',314,9,0),(327,'',314,9,0),(328,'',314,9,0),(329,'Ticket Life Cycle',19,2,12),(330,'Verify Creating multiple tickets',329,3,2),(331,'',330,4,0),(332,'',331,9,0),(333,'',331,9,0),(334,'',331,9,0),(335,'',331,9,0),(336,'',331,9,0),(337,'',331,9,0),(338,'Verify Snap to Grid',329,3,3),(339,'',338,4,0),(340,'',339,9,0),(341,'',339,9,0),(342,'',339,9,0),(343,'Verify Moving tickets outside of the board',329,3,4),(344,'',343,4,0),(345,'',344,9,0),(346,'',344,9,0),(347,'',344,9,0),(348,'Verify Creating a ticket with a huge amount of text',329,3,5),(349,'',348,4,0),(350,'',349,9,0),(351,'',349,9,0),(352,'',349,9,0),(353,'Verify Typing ticket headings ~200 characters long.',329,3,6),(354,'',353,4,0),(355,'',354,9,0),(356,'',354,9,0),(357,'',354,9,0),(358,'',354,9,0),(359,'',354,9,0),(360,'Verify moving ticket using 2 or more browsers /tabs',329,3,8),(361,'',360,4,0),(362,'',361,9,0),(363,'',361,9,0),(364,'',361,9,0),(365,'',361,9,0),(366,'',361,9,0),(367,'Verify Create ticket with special char in heading',329,3,9),(368,'',367,4,0),(369,'',368,9,0),(370,'',368,9,0),(371,'Verify that changing ticket name works',329,3,10),(372,'',371,4,0),(373,'',372,9,0),(374,'',372,9,0),(375,'',372,9,0),(376,'',372,9,0),(377,'Verify User can delete a ticket',329,3,11),(378,'',377,4,0),(379,'',378,9,0),(380,'',378,9,0),(381,'',378,9,0),(382,'',378,9,0),(383,'Verify Simultaneous editing of ticket',329,3,13),(384,'',383,4,0),(385,'',384,9,0),(386,'',384,9,0),(387,'',384,9,0),(388,'',384,9,0),(389,'',384,9,0),(390,'Verify Change ticket color',329,3,14),(391,'',390,4,0),(392,'',391,9,0),(393,'',391,9,0),(394,'',391,9,0),(395,'',391,9,0),(396,'',391,9,0),(397,'',391,9,0),(398,'',391,9,0),(399,'Verify User can edit the color of a ticket',329,3,15),(400,'',399,4,0),(401,'',400,9,0),(402,'',400,9,0),(403,'',400,9,0),(404,'',400,9,0),(405,'Verify Modifying a ticket when owner delete the board',329,3,16),(406,'',405,4,0),(407,'',406,9,0),(408,'',406,9,0),(409,'',406,9,0),(410,'Verify Modifying a ticket when owner clear the sharing link',329,3,17),(411,'',410,4,0),(412,'',411,9,0),(413,'',411,9,0),(414,'',411,9,0),(415,'Verify ticket name word wrapping',329,3,19),(416,'',415,4,0),(417,'',416,9,0),(418,'Verify Tickets overlapping functionality',329,3,20),(419,'',418,4,0),(420,'',419,9,0),(421,'Verify Multiple users deleting the same ticket',329,3,21),(422,'',421,4,0),(423,'',422,9,0),(424,'',422,9,0),(425,'',422,9,0),(426,'',422,9,0),(427,'Verify Move ticket position',329,3,22),(428,'',427,4,0),(429,'',428,9,0),(430,'Verify Ticket minimap test',329,3,23),(431,'',430,4,0),(432,'',431,9,0),(433,'',431,9,0),(434,'',431,9,0),(435,'',431,9,0),(436,'Verify Deleting a ticket while it is being edited',329,3,25),(437,'',436,4,0),(438,'',437,9,0),(439,'',437,9,0),(440,'Verify Minimap ticket color',329,3,26),(441,'',440,4,0),(442,'',441,9,0),(443,'',441,9,0),(444,'Verify Edit ticket text',329,3,28),(445,'',444,4,0),(446,'',445,9,0),(447,'',445,9,0),(448,'',445,9,0),(449,'',445,9,0),(450,'Verify Empty ticket creation',329,3,30),(451,'',450,4,0),(452,'',451,9,0),(453,'',451,9,0),(454,'Verify Ticket interaction with template change',329,3,31),(455,'',454,4,0),(456,'',455,9,0),(457,'',455,9,0),(458,'',455,9,0),(459,'Verify Markdown text',329,3,32),(460,'',459,4,0),(461,'Verify that markdown works as designed.',329,3,33),(462,'',461,4,0),(463,'',462,9,0),(464,'Verify ticket reviewing',329,3,34),(465,'',464,4,0),(466,'',465,9,0),(467,'',465,9,0),(468,'',465,9,0),(469,'',465,9,0),(470,'',465,9,0),(471,'',465,9,0),(472,'',465,9,0),(473,'',465,9,0),(474,'',465,9,0),(475,'',465,9,0),(476,'Export methods',19,2,13),(477,'Verify export JSON-file',476,3,0),(478,'',477,4,0),(479,'',478,9,0),(480,'',478,9,0),(481,'',478,9,0),(482,'Verify export CSV-file',476,3,1),(483,'',482,4,0),(484,'',483,9,0),(485,'',483,9,0),(486,'',483,9,0),(487,'Verify export dialog',476,3,2),(488,'',487,4,0),(489,'',488,9,0),(490,'Verify image export',476,3,3),(491,'',490,4,0),(492,'',491,9,0),(493,'',491,9,0),(494,'',491,9,0),(495,'Verify ticket duplicate',476,3,4),(496,'',495,4,0),(497,'',496,9,0),(498,'',496,9,0),(499,'',496,9,0),(500,'',496,9,0),(501,'Commenting System',19,2,17),(502,'Verify adding comment',501,3,0),(503,'',502,4,0),(504,'',503,9,0),(505,'',503,9,0),(506,'Guest User',19,2,19),(507,'Verify guest live viewing works',506,3,0),(508,'',507,4,0),(509,'',508,9,0),(510,'',508,9,0),(511,'',508,9,0),(512,'',508,9,0),(513,'Verify Guest can edit the color of a ticket',506,3,1),(514,'',513,4,0),(515,'',514,9,0),(516,'',514,9,0),(517,'',514,9,0),(518,'Verify Guest can edit the name of a ticket',506,3,2),(519,'',518,4,0),(520,'',519,9,0),(521,'',519,9,0),(522,'',519,9,0),(523,'Verify Guest can delete a ticket',506,3,3),(524,'',523,4,0),(525,'',524,9,0),(526,'',524,9,0),(527,'',524,9,0),(528,'Verify Guest can access a board',506,3,4),(529,'',528,4,0),(530,'',529,9,0),(531,'',529,9,0),(532,'Help Layer',19,2,23),(533,'Verify Help Layer works',532,3,0),(534,'',533,4,0),(535,'Verify Help Layer works with different browsers/device',532,3,1),(536,'',535,4,0),(537,'Verify slide',532,3,2),(538,'',537,4,0),(539,'Non-Functional testing',18,2,1),(540,'Performance testing',539,2,0),(541,'Board Creation Load Test (10000)',540,3,3),(542,'',541,4,0),(543,'User Load Test (200)',540,3,4),(544,'',543,4,0),(545,'Scalability testing',539,2,1),(546,'Usability testing',539,2,2),(547,'Usability Checklist',546,3,1000),(548,'',547,4,0),(549,'Stability testing',539,2,3),(550,'Stability Test Bench - Vagrant',549,3,1000),(551,'',550,4,0),(552,'Load Testing',539,2,4),(553,'Locust Load Generation',552,3,5),(554,'',553,4,0),(555,'Stress Testing',539,2,5),(556,'Stressing with Noise Generator',555,3,1000),(557,'',556,4,0),(558,'System Integration',6,2,2),(559,'New Relic Integration',558,2,1),(560,'User Voice Integration',558,2,2),(561,'Stripe.com integration',558,2,3),(562,'Module/Unit Testing',6,2,3),(563,'Exploratory Testing',6,2,4),(564,'Exploratory Check List',563,3,1000),(565,'',564,4,0),(566,'Drafts',2,2,2),(567,'resolution scalability with safari',566,3,3),(568,'',567,4,0),(569,'',568,9,0),(570,'',568,9,0),(571,'',568,9,0),(572,'',568,9,0),(573,'',568,9,0),(574,'',568,9,0),(575,'',568,9,0),(576,'',568,9,0),(577,'',568,9,0),(578,'',568,9,0),(579,'',568,9,0),(580,'',568,9,0),(581,'resolution scalability with firefox',566,3,9),(582,'',581,4,0),(583,'',582,9,0),(584,'',582,9,0),(585,'',582,9,0),(586,'',582,9,0),(587,'',582,9,0),(588,'',582,9,0),(589,'',582,9,0),(590,'',582,9,0),(591,'',582,9,0),(592,'',582,9,0),(593,'',582,9,0),(594,'',582,9,0),(595,'resolution scalability with opera',566,3,10),(596,'',595,4,0),(597,'',596,9,0),(598,'',596,9,0),(599,'',596,9,0),(600,'',596,9,0),(601,'',596,9,0),(602,'',596,9,0),(603,'',596,9,0),(604,'',596,9,0),(605,'',596,9,0),(606,'',596,9,0),(607,'',596,9,0),(608,'',596,9,0),(609,'resolution scalability with internet explorer',566,3,15),(610,'',609,4,0),(611,'',610,9,0),(612,'',610,9,0),(613,'',610,9,0),(614,'',610,9,0),(615,'',610,9,0),(616,'',610,9,0),(617,'',610,9,0),(618,'',610,9,0),(619,'',610,9,0),(620,'',610,9,0),(621,'',610,9,0),(622,'',610,9,0),(623,'resolution scalability with google chrome',566,3,18),(624,'',623,4,0),(625,'',624,9,0),(626,'',624,9,0),(627,'',624,9,0),(628,'',624,9,0),(629,'',624,9,0),(630,'',624,9,0),(631,'',624,9,0),(632,'',624,9,0),(633,'',624,9,0),(634,'',624,9,0),(635,'',624,9,0),(636,'',624,9,0),(637,'Stress test',566,3,111),(638,'',637,4,0),(639,'',638,9,0),(640,'Acceptance Tests V0.1',2,5,0),(641,'Functional System Tests V0.1',2,5,0);
/*!40000 ALTER TABLE `nodes_hierarchy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `object_keywords`
--

DROP TABLE IF EXISTS `object_keywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `object_keywords` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_id` int(10) unsigned NOT NULL DEFAULT '0',
  `fk_table` varchar(30) DEFAULT '',
  `keyword_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `object_keywords`
--

LOCK TABLES `object_keywords` WRITE;
/*!40000 ALTER TABLE `object_keywords` DISABLE KEYS */;
/*!40000 ALTER TABLE `object_keywords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `platforms`
--

DROP TABLE IF EXISTS `platforms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `platforms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `testproject_id` int(10) unsigned NOT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_platforms` (`testproject_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `platforms`
--

LOCK TABLES `platforms` WRITE;
/*!40000 ALTER TABLE `platforms` DISABLE KEYS */;
/*!40000 ALTER TABLE `platforms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `req_coverage`
--

DROP TABLE IF EXISTS `req_coverage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `req_coverage` (
  `req_id` int(10) NOT NULL,
  `testcase_id` int(10) NOT NULL,
  `author_id` int(10) unsigned DEFAULT NULL,
  `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `review_requester_id` int(10) unsigned DEFAULT NULL,
  `review_request_ts` timestamp NULL DEFAULT NULL,
  KEY `req_testcase` (`req_id`,`testcase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='relation test case ** requirements';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `req_coverage`
--

LOCK TABLES `req_coverage` WRITE;
/*!40000 ALTER TABLE `req_coverage` DISABLE KEYS */;
/*!40000 ALTER TABLE `req_coverage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `req_relations`
--

DROP TABLE IF EXISTS `req_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `req_relations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source_id` int(10) unsigned NOT NULL,
  `destination_id` int(10) unsigned NOT NULL,
  `relation_type` smallint(5) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT NULL,
  `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `req_relations`
--

LOCK TABLES `req_relations` WRITE;
/*!40000 ALTER TABLE `req_relations` DISABLE KEYS */;
/*!40000 ALTER TABLE `req_relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `req_revisions`
--

DROP TABLE IF EXISTS `req_revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `req_revisions` (
  `parent_id` int(10) unsigned NOT NULL,
  `id` int(10) unsigned NOT NULL,
  `revision` smallint(5) unsigned NOT NULL DEFAULT '1',
  `req_doc_id` varchar(64) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `scope` text,
  `status` char(1) NOT NULL DEFAULT 'V',
  `type` char(1) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `is_open` tinyint(1) NOT NULL DEFAULT '1',
  `expected_coverage` int(10) NOT NULL DEFAULT '1',
  `log_message` text,
  `author_id` int(10) unsigned DEFAULT NULL,
  `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifier_id` int(10) unsigned DEFAULT NULL,
  `modification_ts` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `req_revisions_uidx1` (`parent_id`,`revision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `req_revisions`
--

LOCK TABLES `req_revisions` WRITE;
/*!40000 ALTER TABLE `req_revisions` DISABLE KEYS */;
/*!40000 ALTER TABLE `req_revisions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `req_specs`
--

DROP TABLE IF EXISTS `req_specs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `req_specs` (
  `id` int(10) unsigned NOT NULL,
  `testproject_id` int(10) unsigned NOT NULL,
  `doc_id` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `req_spec_uk1` (`doc_id`,`testproject_id`),
  KEY `testproject_id` (`testproject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Dev. Documents (e.g. System Requirements Specification)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `req_specs`
--

LOCK TABLES `req_specs` WRITE;
/*!40000 ALTER TABLE `req_specs` DISABLE KEYS */;
/*!40000 ALTER TABLE `req_specs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `req_specs_revisions`
--

DROP TABLE IF EXISTS `req_specs_revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `req_specs_revisions` (
  `parent_id` int(10) unsigned NOT NULL,
  `id` int(10) unsigned NOT NULL,
  `revision` smallint(5) unsigned NOT NULL DEFAULT '1',
  `doc_id` varchar(64) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `scope` text,
  `total_req` int(10) NOT NULL DEFAULT '0',
  `status` int(10) unsigned DEFAULT '1',
  `type` char(1) DEFAULT NULL,
  `log_message` text,
  `author_id` int(10) unsigned DEFAULT NULL,
  `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifier_id` int(10) unsigned DEFAULT NULL,
  `modification_ts` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `req_specs_revisions_uidx1` (`parent_id`,`revision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `req_specs_revisions`
--

LOCK TABLES `req_specs_revisions` WRITE;
/*!40000 ALTER TABLE `req_specs_revisions` DISABLE KEYS */;
/*!40000 ALTER TABLE `req_specs_revisions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `req_versions`
--

DROP TABLE IF EXISTS `req_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `req_versions` (
  `id` int(10) unsigned NOT NULL,
  `version` smallint(5) unsigned NOT NULL DEFAULT '1',
  `revision` smallint(5) unsigned NOT NULL DEFAULT '1',
  `scope` text,
  `status` char(1) NOT NULL DEFAULT 'V',
  `type` char(1) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `is_open` tinyint(1) NOT NULL DEFAULT '1',
  `expected_coverage` int(10) NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT NULL,
  `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifier_id` int(10) unsigned DEFAULT NULL,
  `modification_ts` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `log_message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `req_versions`
--

LOCK TABLES `req_versions` WRITE;
/*!40000 ALTER TABLE `req_versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `req_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reqmgrsystems`
--

DROP TABLE IF EXISTS `reqmgrsystems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reqmgrsystems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` int(10) DEFAULT '0',
  `cfg` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `reqmgrsystems_uidx1` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reqmgrsystems`
--

LOCK TABLES `reqmgrsystems` WRITE;
/*!40000 ALTER TABLE `reqmgrsystems` DISABLE KEYS */;
/*!40000 ALTER TABLE `reqmgrsystems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requirements`
--

DROP TABLE IF EXISTS `requirements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requirements` (
  `id` int(10) unsigned NOT NULL,
  `srs_id` int(10) unsigned NOT NULL,
  `req_doc_id` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `requirements_req_doc_id` (`srs_id`,`req_doc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requirements`
--

LOCK TABLES `requirements` WRITE;
/*!40000 ALTER TABLE `requirements` DISABLE KEYS */;
/*!40000 ALTER TABLE `requirements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rights`
--

DROP TABLE IF EXISTS `rights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `rights_descr` (`description`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rights`
--

LOCK TABLES `rights` WRITE;
/*!40000 ALTER TABLE `rights` DISABLE KEYS */;
INSERT INTO `rights` VALUES (18,'cfield_management'),(17,'cfield_view'),(22,'events_mgt'),(36,'exec_delete'),(35,'exec_edit_notes'),(41,'exec_testcases_assigned_to_me'),(31,'issuetracker_management'),(32,'issuetracker_view'),(29,'keyword_assignment'),(9,'mgt_modify_key'),(12,'mgt_modify_product'),(11,'mgt_modify_req'),(7,'mgt_modify_tc'),(16,'mgt_testplan_create'),(30,'mgt_unfreeze_req'),(13,'mgt_users'),(20,'mgt_view_events'),(8,'mgt_view_key'),(10,'mgt_view_req'),(6,'mgt_view_tc'),(21,'mgt_view_usergroups'),(24,'platform_management'),(25,'platform_view'),(26,'project_inventory_management'),(27,'project_inventory_view'),(33,'reqmgrsystem_management'),(34,'reqmgrsystem_view'),(28,'req_tcase_link_management'),(14,'role_management'),(19,'system_configuration'),(43,'testplan_add_remove_platforms'),(2,'testplan_create_build'),(1,'testplan_execute'),(3,'testplan_metrics'),(40,'testplan_milestone_overview'),(4,'testplan_planning'),(45,'testplan_set_urgent_testcases'),(46,'testplan_show_testcases_newest_versions'),(37,'testplan_unlink_executed_testcases'),(44,'testplan_update_linked_testcase_versions'),(5,'testplan_user_role_assignment'),(38,'testproject_delete_executed_testcases'),(39,'testproject_edit_executed_testcases'),(42,'testproject_metrics_dashboard'),(23,'testproject_user_role_assignment'),(15,'user_role_assignment');
/*!40000 ALTER TABLE `rights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `risk_assignments`
--

DROP TABLE IF EXISTS `risk_assignments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `risk_assignments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `testplan_id` int(10) unsigned NOT NULL DEFAULT '0',
  `node_id` int(10) unsigned NOT NULL DEFAULT '0',
  `risk` char(1) NOT NULL DEFAULT '2',
  `importance` char(1) NOT NULL DEFAULT 'M',
  PRIMARY KEY (`id`),
  UNIQUE KEY `risk_assignments_tplan_node_id` (`testplan_id`,`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `risk_assignments`
--

LOCK TABLES `risk_assignments` WRITE;
/*!40000 ALTER TABLE `risk_assignments` DISABLE KEYS */;
/*!40000 ALTER TABLE `risk_assignments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_rights`
--

DROP TABLE IF EXISTS `role_rights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_rights` (
  `role_id` int(10) NOT NULL DEFAULT '0',
  `right_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`role_id`,`right_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_rights`
--

LOCK TABLES `role_rights` WRITE;
/*!40000 ALTER TABLE `role_rights` DISABLE KEYS */;
INSERT INTO `role_rights` VALUES (4,3),(4,6),(4,7),(4,8),(4,9),(4,10),(4,11),(5,3),(5,6),(5,8),(6,1),(6,2),(6,3),(6,6),(6,7),(6,8),(6,9),(6,11),(6,25),(6,27),(7,1),(7,3),(7,6),(7,8),(8,1),(8,2),(8,3),(8,4),(8,5),(8,6),(8,7),(8,8),(8,9),(8,10),(8,11),(8,12),(8,13),(8,14),(8,15),(8,16),(8,17),(8,18),(8,19),(8,20),(8,21),(8,22),(8,23),(8,24),(8,25),(8,26),(8,27),(8,30),(8,31),(8,32),(8,33),(8,34),(8,35),(8,36),(8,37),(8,38),(8,39),(8,40),(8,41),(8,42),(8,43),(8,44),(8,45),(8,46),(9,1),(9,2),(9,3),(9,4),(9,5),(9,6),(9,7),(9,8),(9,9),(9,10),(9,11),(9,15),(9,16),(9,24),(9,25),(9,26),(9,27);
/*!40000 ALTER TABLE `role_rights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL DEFAULT '',
  `notes` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_rights_roles_descr` (`description`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'<reserved system role 1>',NULL),(2,'<reserved system role 2>',NULL),(3,'<no rights>',NULL),(4,'test designer',NULL),(5,'guest',NULL),(6,'senior tester',NULL),(7,'tester',NULL),(8,'admin',NULL),(9,'leader',NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tcsteps`
--

DROP TABLE IF EXISTS `tcsteps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tcsteps` (
  `id` int(10) unsigned NOT NULL,
  `step_number` int(11) NOT NULL DEFAULT '1',
  `actions` text,
  `expected_results` text,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `execution_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 -> manual, 2 -> automated',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tcsteps`
--

LOCK TABLES `tcsteps` WRITE;
/*!40000 ALTER TABLE `tcsteps` DISABLE KEYS */;
INSERT INTO `tcsteps` VALUES (23,1,'<p>\n	Both users select the same board by clicking in the center of it.</p>\n','<p>\n	Same board view is displayed to both users.</p>\n',1,1),(24,2,'<p>\n	One of the users creates a new ticket by pressing the Create ticket button and names it.</p>\n','<p>\n	New ticket is created.</p>\n',1,1),(25,3,'<p>\n	The same user presses the magnet icon on the top toolbar.</p>\n','<p>\n	Magnet feature becomes active.</p>\n',1,1),(26,4,'<p>\n	This user now moves the ticket to a random position on the board.</p>\n','<p>\n	The ticket slides into the correct position assigned by the user.</p>\n',1,1),(27,5,'<p>\n	The other user refreshes his/hers browser window.</p>\n','<p>\n	Ticket is displayed on the same position for both users.</p>\n',1,1),(28,6,'<p>\n	Repeat 10 times alternating between users.</p>\n','<p>\n	See steps 2-5.</p>\n',1,1),(29,7,'<p>\n	If tickets are displayed at the right position for both users every time: PASS , otherwise FAIL.</p>\n','<p>\n	Result:PASS</p>\n',1,1),(32,1,'<p>\n	Create new board by clicking the &quot;+&quot;-button on the middle</p>\n','<p>\n	Board creating view is displayed.</p>\n',1,1),(33,2,'<p>\n	Edit the board from &quot;pencil&quot;-button and type into the text field a board name ~200 characters long with spaces and press Done.</p>\n','<p>\n	Board appears on the main view and its name is displayed properly.</p>\n',1,1),(34,3,'<p>\n	Create new board again and this time dont use spaces.</p>\n','<p>\n	Board appears on the main view and its name is displayed properly.</p>\n',1,1),(35,4,'<p>\n	Repeat steps 1-3 by creating 5 without and 5 names with spaces.</p>\n','<p>\n	See steps 1-3.</p>\n',1,1),(36,5,'<p>\n	If overly long board names are cut properly every time and they dont escape the element: PASS , otherwise FAIL.</p>\n','<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	Long name escapes or error--&gt; FAIL.</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\"><span style=\"font-size: 11.6999998092651px;\">Long names cut properly and dont escape the board.&nbsp;</span>&nbsp;</span>--&gt; PASS.</div>\n<div>\n	&nbsp;</div>\n',1,1),(39,1,'<p>\n	Click a board</p>\n','<p>\n	You are inside a board</p>\n',1,1),(40,2,'<p>\n	Click the workspace-button</p>\n','<p>\n	You should be back to workspace</p>\n',1,1),(41,3,'<p>\n	Go back to board and press the Contriboard-button upper left corner</p>\n','<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<span style=\"font-size: 11.6999998092651px;\">User can&#39;t go back to workspace --&gt; FAIL.</span></div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">User is successfully back to workspace&nbsp;</span>--&gt; PASS.</div>\n<div>\n	&nbsp;</div>\n',1,1),(44,1,'<p>\n	Press the &quot;tick&quot;-button</p>\n','<p>\n	More option should be available at the top bar</p>\n',1,1),(45,2,'<p>\n	Press the &quot;trashbin&quot;-button</p>\n','<p>\n	Deletion-menu should show up</p>\n',1,1),(46,3,'<p>\n	Press &quot;delete&quot;-button</p>\n','<p>\n	Board should now be deleted from workspace</p>\n',1,1),(47,4,'<p>\n	Log out and log back in</p>\n','<p>\n	Board should be gone forever from the workspace</p>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	The board wasn&#39;t deleted --&gt; FAIL.</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">The board was deleted forever&nbsp;</span>--&gt; PASS.</div>\n',1,1),(50,1,'<p>\n	Open the edit menu of a board</p>\n','<p>\n	Edit menu should show up</p>\n',1,1),(51,2,'<p>\n	Click the X-button to close the menu</p>\n','<p>\n	Menu should be closed</p>\n',1,1),(52,3,'<p>\n	Open the menu again and close it by clicking outside of the menu</p>\n','<p>\n	Menu should be closed</p>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	Menu didn&#39;t close by pressing the &quot;close&quot;-button --&gt; FAIL.</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\"><span style=\"font-size: 11.6999998092651px;\">Menu didn&#39;t close by pressing outside of the menu</span>&nbsp;--&gt; FAIL.</span></div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">Menu closed and data was unchanged&nbsp;</span>--&gt; PASS.</div>\n',1,1),(55,1,'<p>\n	Click Create Ticket button</p>\n','<p>\n	Ticket Creation Dialog is shown</p>\n',1,1),(56,2,'<p>\n	Write&nbsp;<span style=\"color: rgb(37, 37, 37); font-family: sans-serif; font-size: 14px; line-height: 22.3999996185303px; background-color: rgb(255, 255, 255);\">片仮名 as the name</span></p>\n','<p>\n	name textbox reads&nbsp;<span style=\"color: rgb(37, 37, 37); font-family: sans-serif; font-size: 14px; line-height: 22.3999996185303px; background-color: rgb(255, 255, 255);\">片仮名</span></p>\n',1,1),(57,3,'<p>\n	Click Create button</p>\n','<p>\n	A new ticket is created with the name of&nbsp;<span style=\"color: rgb(37, 37, 37); font-family: sans-serif; font-size: 14px; line-height: 22.3999996185303px; background-color: rgb(255, 255, 255);\">片仮名</span></p>\n<p>\n	<span style=\"font-size: 16px; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">PASS/FAIL if the UTF-8 string shows correctly, FAIL otherwise.</span></p>\n<p>\n	&nbsp;</p>\n',1,1),(60,1,'<p>\n	The user clicks the &#39;Correct&#39; symbol in the upper right corner of a box representing a board</p>\n','<p>\n	The board is now highlighted with green colour and a symbol representing paper and a pen appears next to the &#39;+ Create board&#39; button</p>\n',1,1),(61,2,'<p>\n	The user clicks the paper and pen symbol</p>\n','<p>\n	A &#39;Edit board&#39; pop up appears</p>\n',1,1),(62,3,'<p>\n	The user highlights all the text in the &#39;Name&#39; field on the pop up and writes &#39;test&#39; to the field</p>\n','<p>\n	The &#39;Name&#39; field now displays text &#39;test&#39;</p>\n',1,1),(63,4,'<p>\n	The user clicks the &#39;Save changes&#39; button on the pop up</p>\n','<p>\n	The pop up closes and the new name of the modified board is displayed by the board on the board list</p>\n',1,1),(66,1,'<p>\n	Open board</p>\n','<p>\n	Board opens</p>\n<p>\n	&nbsp;</p>\n',1,1),(67,2,'<p>\n	click on the landscape button on top left</p>\n','<p>\n	background dialog opens.</p>\n',1,1),(68,3,'<p>\n	select new workflow template for board and click apply</p>\n','<p>\n	Selected template is now the background of board</p>\n',1,1),(71,1,'<p>\n	Create a board</p>\n','<p>\n	A board is created</p>\n',1,1),(72,2,'<p>\n	Duplicate browser tab</p>\n','<p>\n	2 tabs with same view is open</p>\n',1,1),(73,3,'<p>\n	Delete the board in first tab</p>\n','<p>\n	The tab was removed</p>\n',1,1),(74,4,'<p>\n	Open the board in second tab</p>\n','<p>\n	The board doesn&#39;t open or cause grey overlay</p>\n',1,1),(77,1,'<p>\n	Share the link to another user, who opens the <em>Edit board</em> function.</p>\n','',1,1),(78,2,'<p>\n	Attempt to delete the board while the other user is editing it.</p>\n','<div>\n	Board is deleted and no errors occur OR board cannot be deleted while it is being edited ► PASS.</div>\n<div>\n	System crash OR some errors occur with any of the board users ► FAIL.</div>\n',1,1),(81,1,'<p>\n	Douple click the board to create an ticket</p>\n','<p>\n	Ticket appears</p>\n',1,1),(82,2,'<p>\n	Click somewhere else than in that popup window</p>\n','<p>\n	popup window disapears.</p>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	Popup-ticket doesnt fade or other error --&gt; FAIL.</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\"><span style=\"font-size: 11.6999998092651px;\">Popup-Ticket fades, user only sees board and 1 ticket.</span>&nbsp;</span>--&gt; PASS.</div>\n<div>\n	&nbsp;</div>\n',1,1),(85,1,'<p>\n	Copy the url of the board. Go back to workspace and delete the board you created.</p>\n','<p>\n	You are taken to the workspace.</p>\n',1,1),(86,2,'<p>\n	Paste the url on your browser and press enter.</p>\n','<p>\n	The board does NOT exist, user should be noticed that board does not exist. User should not &nbsp;be able to enter the boardview. User should be taken back to workspace.</p>\n',1,1),(89,1,'<p>\n	-click workflow templates icon in the upper right of the screen</p>\n','<p>\n	-popup window appears</p>\n',1,1),(90,2,'<p>\n	-click somewhere else than in that popup window</p>\n','<div>\n	-popup window disapears</div>\n<div>\n	&nbsp;</div>\n<div>\n	-workflow template stays the same as before</div>\n<div>\n	&nbsp;</div>\n',1,1),(93,1,'<p>\n	CTRL + left click on a board</p>\n','<p>\n	PASS: A new tab is opened. Opened tab shows the board page and the original page is still fully usable.</p>\n<p>\n	FAIL: A new tab is not opened or the original page does not work</p>\n',1,1),(96,1,'<p>\n	Create three tickets. Position first ticket on the downside of the board, second ticket on the right side of the board and last one on the rightdownside of the board.</p>\n','<p>\n	tickets should move</p>\n',1,1),(97,2,'<p>\n	Press &quot;Pencil&quot;-button</p>\n','<p>\n	&quot;Edit board&quot;-view opens</p>\n',1,1),(98,3,'<p>\n	Give board size width: 5 and height: 5</p>\n<p>\n	Press done</p>\n','<p>\n	Board size should change to smaller and tickets should stay on the board.&nbsp;</p>\n',1,1),(101,1,'<p>\n	Select &quot;pencil&quot;-button</p>\n','<p>\n	&quot;EditBoard&quot;-view opens</p>\n',1,1),(102,2,'<p>\n	Give board a random name with &auml;&ouml;&aring;-letters. press done</p>\n','<p>\n	Dialog should close and board is renamed</p>\n',1,1),(105,1,'<p>\n	Open share-tab and press &quot;share&quot;-button.&nbsp;</p>\n','<p>\n	Url will appear.</p>\n',1,1),(106,2,'<p>\n	Copy paste the url in to incognito tab and log in as quest 3 times in 3 different incognito tabs.</p>\n','<p>\n	User will have 3 incogntio tabs open and one main user tab</p>\n',1,1),(107,3,'<p>\n	Go to your &quot;logged in&quot;-tab and press &quot;board members&quot;-button.</p>\n','<p>\n	Board member dialog shows.</p>\n',1,1),(108,4,'<p>\n	Compare the quest names you used in incognito tabs to the board member list.</p>\n','<p>\n	If every name matches --&gt; Pass</p>\n<p>\n	If one or more name is missing or wrong --&gt; Fail</p>\n',1,1),(112,1,'<p>\n	Log in and create 5 boards (text doesn&#39;t matter)</p>\n','<p>\n	5 boards created</p>\n',1,1),(113,2,'<p>\n	Log out and log in again</p>\n','<p>\n	5 created boards should be visible</p>\n',1,1),(114,3,'<p>\n	Remove 2 first created boards and log out, log in again</p>\n','<p>\n	3 last created boards should be visible</p>\n',1,1),(115,4,'<p>\n	Remove 2 last created boards and log out, log in again</p>\n','<p>\n	<span style=\"font-size: 11.6999998092651px;\">There is more or less boards open. --&gt; FAIL.</span></p>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">3rd created board should be visible&nbsp;</span>--&gt; PASS.</div>\n',1,1),(118,1,'<p>\n	Click check button on board</p>\n','<p>\n	Board activates</p>\n',1,1),(119,2,'<p>\n	Click edit from top row</p>\n','<p>\n	A dialog opens</p>\n',1,1),(120,3,'<p>\n	Change name from textfield</p>\n','<p>\n	Name changes in textfield</p>\n',1,1),(121,4,'<p>\n	Click save changes</p>\n','<p>\n	dialog closes and board name changes</p>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<strong>Verdict:</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	Name did not change or it is wrong --&gt; FAIL.</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">Board&#39;s name changed&nbsp;</span>--&gt; PASS.</div>\n',1,1),(124,1,'<p>\n	Select a board and open it</p>\n','<p>\n	Board should open.</p>\n',1,1),(125,2,'<p>\n	Copy URL from &quot;shareboard&quot;-dialog. Open other tab in incognito mode. Paste the URL and login as guest.</p>\n','<p>\n	Board should be shown.</p>\n',1,1),(126,3,'<p>\n	In the normal tab(not incognito) select a board to edit and edit the board&#39;s name and change the name. Press &quot;Done&quot;-button.</p>\n','<p>\n	Board name should be updated and settings saved. Both&nbsp;incognito and normal tab.</p>\n',1,1),(127,4,'<p>\n	Check on the board on the incognito&nbsp;tab that the board name has changed.</p>\n','<p>\n	<span style=\"font-size: 11.6999998092651px;\">Board name&nbsp;</span><strong style=\"font-size: 11.6999998092651px;\">does not</strong><span style=\"font-size: 11.6999998092651px;\">&nbsp;matches to the one that was set in normal tab.--&gt; FAIL.</span></p>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">Board name in incognito tab&nbsp;matches the one that was set in normal tab.&nbsp;</span>--&gt; PASS.</div>\n',1,1),(130,1,'<p>\n	Click &quot;+&quot;-button in the middle of the screen</p>\n','<p>\n	New board should be created.</p>\n',1,1),(131,2,'<p>\n	Open the board.</p>\n','<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<span style=\"font-size: 11.6999998092651px;\">Nameless empty board was&nbsp;</span><strong style=\"font-size: 11.6999998092651px;\">not</strong><span style=\"font-size: 11.6999998092651px;\">&nbsp;created or errors--&gt; FAIL.</span></div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">Nameless empty board was created&nbsp;</span>--&gt; PASS.</div>\n<div>\n	&nbsp;</div>\n',1,1),(134,1,'<p>\n	User 1 creates an board. And share&#39;s the &nbsp;shared url of that board to the user 2.</p>\n','',1,1),(135,2,'<p>\n	User 2 opens incognito tab and logs is. After that he connects to the shared url to the board.</p>\n','<p>\n	User 2 is taken to the board.</p>\n',1,1),(136,3,'<p>\n	User 2 goes back to workspace from boardview.</p>\n','<p>\n	User 2 is taken to the workspace.</p>\n',1,1),(137,4,'<p>\n	User 2 should see the board he connected in the boardlist.</p>\n','<p>\n	New shared board should be visible in the boardlist.</p>\n',1,1),(141,1,'<p>\n	Register with google account</p>\n','<p>\n	Contriboard account is created via using google account</p>\n',1,1),(142,2,'<p>\n	Navigate to user panel from top right corner and verify that avatar and username are imported as they should.</p>\n','<p>\n	Login process should import avatar and username from google.</p>\n',1,1),(143,3,'<p>\n	Log out.</p>\n','<p>\n	User is logged out.</p>\n',1,1),(146,1,'<p>\n	<font color=\"#333333\" face=\"Helvetica Neue, Helvetica, Segoe UI, Arial, freesans, sans-serif\"><span style=\"line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Press &quot;register&quot;-button</span></font></p>\n','<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Register opens.</span></p>\n',1,1),(147,2,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(248, 248, 248);\">Type an unused random email to email field (for ex. test@mail.1)</span></p>\n','',1,1),(148,3,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Type a random password to password field (for ex. password). Use approved password type.</span></p>\n','',1,1),(149,4,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(248, 248, 248);\">Click Register button</span></p>\n','<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(248, 248, 248);\">Account is created and user is redirected to workspace.</span></p>\n',1,1),(150,5,'<p>\n	Log out from contriboard</p>\n','<p>\n	User is logged out.</p>\n',1,1),(151,6,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Type email of just registered account to email field</span></p>\n','',1,1),(152,7,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(248, 248, 248);\">Type password of just registered account to password field</span></p>\n','',1,1),(153,8,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Click Sign In</span></p>\n','<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Pass --&gt;User is logged in and redirected to workspace page.&nbsp;</span></p>\n<p>\n	<font color=\"#333333\" face=\"Helvetica Neue, Helvetica, Segoe UI, Arial, freesans, sans-serif\"><span style=\"line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Fail--&gt; something else</span></font></p>\n',1,1),(156,1,'<p>\n	Type an unused email to email field (a.random@a.a)</p>\n','',1,1),(157,2,'<p>\n	Type seven characters to password field and password again field (1234567)</p>\n','',1,1),(158,3,'<p>\n	Click Register button</p>\n','<p>\n	Validation failed message is shown at the top of the page and account is not created.</p>\n<div style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; margin: 0px; padding: 0px; background-color: rgb(238, 238, 238);\">\n	Verdict:</div>\n<div style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; margin: 0px; padding: 0px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n<div style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; margin: 0px; padding: 0px; background-color: rgb(238, 238, 238);\">\n	Account is created or validation message isnt shown--&gt; FAIL.</div>\n<div style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; margin: 0px; padding: 0px; background-color: rgb(238, 238, 238);\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">Account is NOT created and validation message is shown&nbsp;</span>--&gt; PASS.</div>\n',1,1),(161,1,'<p>\n	Press &quot;profile&quot;-button on right top corner and select &quot;Logout&quot;</p>\n','<p>\n	PASS, if user was logged out to the front page</p>\n<p>\n	FAIL, if user was not logged out</p>\n',1,1),(162,2,'<p>\n	Login again and create a board with &quot;+&quot;-button.</p>\n','<p>\n	You are redirected to the new board workspace.</p>\n',1,1),(163,3,'<p>\n	Press &quot;profile&quot;-button on right top corner and select &quot;Logout&quot;</p>\n','<p>\n	PASS, if user was logged out to the front page</p>\n<p>\n	FAIL, if user was not logged out</p>\n',1,1),(166,2,'<p>\n	Press Register button</p>\n','<p>\n	user is taken to register view</p>\n',1,1),(167,3,'<p>\n	Register with email&nbsp;<a href=\"mailto:testi123@gmail.com\" style=\"box-sizing: border-box; text-decoration: none; font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background: rgb(255, 255, 255);\">testi123@gmail.com</a><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;and password: 12345678</span></p>\n','<p>\n	User should not be able to register because email is already in use</p>\n<div style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; margin: 0px; padding: 0px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n<div style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; margin: 0px; padding: 0px; background-color: rgb(238, 238, 238);\">\n	User should is able to register or other error--&gt; FAIL.</div>\n<div style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; margin: 0px; padding: 0px; background-color: rgb(238, 238, 238);\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">User is&nbsp;<b>NOT</b>&nbsp;able to register because email is already in use</span>--&gt; PASS.</div>\n',1,1),(170,1,'<p>\n	Press register on Contriboard login screen.</p>\n','<p>\n	Registeration view is displayed.</p>\n',1,1),(171,2,'<p>\n	Type into the email field <a href=\"mailto:something@something.com\">something@something.com</a></p>\n','<p>\n	Input is shown as it is in the field.</p>\n',1,1),(172,3,'<p>\n	Type special characters into the password field, for example: some;thing&gt; , atleast 8 characters long.</p>\n','<p>\n	Input is hidden.</p>\n',1,1),(173,4,'<p>\n	Type the same text for the Confirm password field.</p>\n','<p>\n	Input is also hidden for this field.</p>\n',1,1),(174,5,'<p>\n	Press register button</p>\n','<p>\n	The registeration should fail, because it contains critical special characters.</p>\n',1,1),(175,6,'<p>\n	Repeat these steps 10 times while using different inputs, containing special characters, for the password fields.</p>\n','<p>\n	See steps 1-5.</p>\n',1,1),(176,7,'<p>\n	If registeration fails every time and the special characters are escaped properly each time: PASS , otherwise FAILED.</p>\n','<p>\n	Failed if register succeeded.</p>\n<p>\n	Passed if register failed.</p>\n',1,1),(179,1,'<p>\n	Click &#39;Register&#39; on the login screen.</p>\n','<p>\n	A registering form opens.</p>\n',1,1),(180,2,'<p>\n	Enter email and password.</p>\n','<p>\n	Checks if the password is adequate.</p>\n',1,1),(181,3,'<p>\n	Confirm password.</p>\n','<p>\n	Checks that passwords match and warns if not.</p>\n',1,1),(182,4,'<p>\n	Click &#39;Register&#39;.</p>\n','<p>\n	Confirmation is given that registration succeeded. User is redirected to the first page.</p>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	User is not created and/or redirected to the first page --&gt; FAIL.</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\"><span style=\"font-size: 11.6999998092651px;\">User is created and redirected to the first page.</span>&nbsp;</span>--&gt; PASS.</div>\n<div>\n	&nbsp;</div>\n',1,1),(185,1,'<p>\n	The tester goes to login page (&#39;http://contriboard.n4sjamk.org/login&#39;) with his/her browser</p>\n','<p>\n	The login page is loaded</p>\n',1,1),(186,2,'<p>\n	The tester writes &#39;testing@testing.fi&#39; to the Email field and &#39;testingtesting&#39; to the Password field on the login page</p>\n','<p>\n	The user input is is registered to the text fields</p>\n',1,1),(187,3,'<p>\n	The tester clicks the &#39;Sign in&#39; button on the page</p>\n','<p>\n	Contriboard board view is loaded</p>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	User is&nbsp;<strong>NOT&nbsp;</strong>logged in&nbsp;--&gt; FAIL.</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">User is logged in</span>--&gt; PASS.</div>\n<div>\n	&nbsp;</div>\n',1,1),(190,1,'<p>\n	Write your old password and the new password 2 times in the correct fields. Press submit</p>\n','<p>\n	Password should change.</p>\n',1,1),(191,2,'<p>\n	Log out and log in using the new password</p>\n','<p>\n	User should log in. If log in works with new password --&gt; pass</p>\n<p>\n	If log in does not work with new password --&gt; fail</p>\n',1,1),(194,1,'<p>\n	Create new user with email of your selection. Write email.</p>\n','',1,1),(195,2,'<p>\n	Type &quot;SalaSana123&quot; to password field</p>\n','<p>\n	Password-field should show 11 dots</p>\n',1,1),(196,3,'<p>\n	Type &quot;PassWord123&quot; to Verify password -field</p>\n','<p>\n	Field should show 11 dots</p>\n',1,1),(197,4,'<p>\n	Click register or submit, depending if your registering or in the profile tab</p>\n','<p>\n	Info about password missmatch should be shown</p>\n',1,1),(198,5,'<p>\n	type &quot;salasana123&quot; in both password fields and press register</p>\n','<p>\n	new user is created</p>\n',1,1),(199,6,'<p>\n	Go to profile in right up corner. and there go to &quot;password&quot;-tab. Repeat steps 2-4 (verify your old password with &quot; SalaSana123 &quot;)</p>\n','<p>\n	Info about missmatch should be shown</p>\n',1,1),(200,7,'<p>\n	type &quot;salis123123&quot; in both password fields and press submit.(verify your old password with &quot; SalaSana123 &quot;) Log out.</p>\n','<p>\n	Users password should be changed and should be taken to login page</p>\n',1,1),(201,8,'<p>\n	login again using &quot;salis123123&quot; as a password</p>\n','<p>\n	User is logged in -&gt; PASS</p>\n<p>\n	User is not logged in -&gt; Fail&nbsp;</p>\n',1,1),(204,2,'<p>\n	Enter invalid format email address &#39;thisinnotmyemail@&#39;</p>\n','<p>\n	Text should appear in input.</p>\n',1,1),(205,3,'<p>\n	Enter valid password to both password fields &#39;boardbassword2015&#39;</p>\n','<p>\n	Password should be input and characters should be hidden.</p>\n',1,1),(206,4,'<p>\n	Click on register button</p>\n','<p>\n	Error message of invalid email should appear.</p>\n<p>\n	PASS: if &#39;Invalid email&#39; message appears<br />\n	FAIL: if no error message is shown or the user is allowed to register</p>\n',1,1),(209,1,'<p>\n	Login to test account with right credentials</p>\n','<p>\n	Should have access to contriboard</p>\n',1,1),(210,2,'<p>\n	Log out</p>\n','<p>\n	User logs out</p>\n',1,1),(211,3,'<p>\n	Try to login to test account with invalid password</p>\n','<p>\n	Login error message should pop up = PASS</p>\n<p>\n	Something else happens = FAIL</p>\n',1,1),(214,2,'<p>\n	Logout from normal tab, leave&nbsp;incognito tab open.</p>\n','<p>\n	You should be logged out in the normal tab.</p>\n',1,1),(215,3,'<p>\n	Check on the incognito tab that you arent able to move tickets, create new tickets or boards. First action should log you out.</p>\n','<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	User is still logged in --&gt; FAIL.</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">Normal and incognito&nbsp;</span><span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">tabs are both logged out&nbsp;</span>--&gt; PASS.</div>\n',1,1),(218,1,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Open login page in one browser (</span><a href=\"http://contriboard.n4sjamk.org/login\" style=\"box-sizing: border-box; color: rgb(65, 131, 196); text-decoration: none; font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background: rgb(255, 255, 255);\">http://contriboard.n4sjamk.org/login</a><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">)</span></p>\n','<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Login page is loaded</span></p>\n',1,1),(219,2,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(248, 248, 248);\">Type credentials (</span><a href=\"mailto:testcase0@mailinator.com\" style=\"box-sizing: border-box; color: rgb(65, 131, 196); text-decoration: none; font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background: rgb(248, 248, 248);\">testcase0@mailinator.com</a><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(248, 248, 248);\">:salasana1) on the login page and click Sign In</span></p>\n','<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(248, 248, 248);\">User is logged in and redirected to workspace (</span><a href=\"http://contriboard.n4sjamk.org/main/workspace\" style=\"box-sizing: border-box; color: rgb(65, 131, 196); text-decoration: none; font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background: rgb(248, 248, 248);\">http://contriboard.n4sjamk.org/main/workspace</a><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(248, 248, 248);\">)</span></p>\n',1,1),(220,3,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Click a Board (testboard0)</span></p>\n','<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">User is redirected to Board page (</span><a href=\"http://contriboard.n4sjamk.org/main/board/54c4da003e97971100f4f099\" style=\"box-sizing: border-box; color: rgb(65, 131, 196); text-decoration: none; font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background: rgb(255, 255, 255);\">http://contriboard.n4sjamk.org/main/board/54c4da003e97971100f4f099</a><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">)</span></p>\n',1,1),(221,4,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(248, 248, 248);\">Copy Board address (</span><a href=\"http://contriboard.n4sjamk.org/main/board/54c4da003e97971100f4f099\" style=\"box-sizing: border-box; color: rgb(65, 131, 196); text-decoration: none; font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background: rgb(248, 248, 248);\">http://contriboard.n4sjamk.org/main/board/54c4da003e97971100f4f099</a><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(248, 248, 248);\">)</span></p>\n','',1,1),(222,5,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Open web developer tools (ex. Chrome &amp; Developer Tools) and find tb-access-token-user key from local storage and copy it (eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU0YzRkOTQyM2U5Nzk3MTEwMGY0ZjA5OCIsInR5cGUiOiJ1c2VyIiwidXNlcm5hbWUiOiJ0ZXN0Y2FzZTBAbWFpbGluYXRvci5jb20iLCJpYXQiOjE0MjI0NzM5ODh9.5HqDnunWiksPrQ6TqNp5jw0equoYgcviBJo0SZ3BNis)</span></p>\n','',1,1),(223,6,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(248, 248, 248);\">Click Log out button to log out</span></p>\n','<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(248, 248, 248);\">User is logged out and redirected to login page and tb-access-token-user is removed from local storage</span></p>\n',1,1),(224,7,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Paste copied Board address to first browser address bar and navigate there</span></p>\n','<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">User is not let on the Board page and is redirected to login page</span></p>\n',1,1),(225,8,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(248, 248, 248);\">Create manually tb-access-token-user key to local storage and paste the value of stored user-token</span></p>\n','',1,1),(226,9,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Paste copied Board address to first browser address bar and navigate there</span></p>\n','<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">User is not let on the Board page and is redirected to login page</span></p>\n',1,1),(227,10,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(248, 248, 248);\">Click Create ticket button</span></p>\n','<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(248, 248, 248);\">Not possible to do</span></p>\n',1,1),(228,11,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Give it a random header and click create</span></p>\n','<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Not possible to do</span></p>\n',1,1),(229,12,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(248, 248, 248);\">Click Log out button</span></p>\n','<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Not possible to do</span></p>\n',1,1),(230,13,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Type credentials (</span><a href=\"mailto:testcase0@mailinator.com\" style=\"box-sizing: border-box; color: rgb(65, 131, 196); text-decoration: none; font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background: rgb(255, 255, 255);\">testcase0@mailinator.com</a><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">:salasana1) on the login page and click Sign In</span></p>\n','<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">User is logged in and redirected to workspace (</span><a href=\"http://contriboard.n4sjamk.org/main/workspace\" style=\"box-sizing: border-box; color: rgb(65, 131, 196); text-decoration: none; font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background: rgb(255, 255, 255);\">http://contriboard.n4sjamk.org/main/workspace</a><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">)</span></p>\n',1,1),(231,14,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(248, 248, 248);\">Click a Board (testboard0) and check modifications</span></p>\n','<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(248, 248, 248);\">User is redirected to Board page and the Board is not modified</span></p>\n',1,1),(232,15,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Open web developer tools (ex. Chrome &amp; Developer Tools) and find tb-access-token-user key from local storage and see if it&#39;s different from the stored one</span></p>\n','<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Token value is different</span></p>\n',1,1),(233,16,'<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(248, 248, 248);\">Repeat steps from #6 to #15 to see that if relogin prevents the old user token exploit</span></p>\n','<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(248, 248, 248);\">Old user-token cannot be used</span></p>\n',1,1),(236,1,'<p>\n	Register with google account</p>\n','<p>\n	Contriboard account is created via using google account</p>\n',1,1),(237,2,'<p>\n	Navigate to user panel from top right corner and verify that avatar and username are imported as they should.</p>\n','<p>\n	Login process should import avatar and username from google.</p>\n',1,1),(238,3,'<p>\n	Log out.</p>\n','<p>\n	User is logged out.</p>\n',1,1),(241,1,'<p>\n	Navigate to profile page through top right corner menu.</p>\n','<p>\n	Profile page should open</p>\n',1,1),(242,2,'<p>\n	Open a new tab in your browser and copy this link to your browsers address bar and make sure it is working:<br />\n	https://lh3.googleusercontent.com/-91my08K--PI/AAAAAAAAAAI/AAAAAAAAAAA/rKaOeVpHZkA/photo.jpg</p>\n','<p>\n	Link should redirect to a working avatar</p>\n',1,1),(243,3,'<p>\n	In Contriboard profile page, copy this link to &quot;Your avatar&quot; line and submit:&nbsp; https://lh3.googleusercontent.com/-91my08K--PI/AAAAAAAAAAI/AAAAAAAAAAA/rKaOeVpHZkA/photo.jpg</p>\n','<p>\n	You should see the avatar changing in section &quot;Your avatar&quot;</p>\n',1,1),(244,4,'<p>\n	Navigate back to main menu through &quot;&lt;- Workspace&quot; -button.</p>\n','<p>\n	Button redirects to previous page</p>\n',1,1),(245,5,'<p>\n	Create board</p>\n','<p>\n	Board created</p>\n',1,1),(246,6,'<p>\n	Check board members tab if users avatar change succeed. &quot;Board members&quot; -tab can be found from button located in top right corner.</p>\n','<p>\n	Board members tab opens and you can see the new avatar under your username.</p>\n',1,1),(247,7,'<p>\n	Create ticket</p>\n','<p>\n	Ticket created</p>\n',1,1),(248,8,'<p>\n	Write a comment to ticket and check if the new avatar is also applyed as signature for your comment.</p>\n','<p>\n	New avatar is shown in front of the comment.</p>\n',1,1),(249,9,'','',1,1),(253,1,'<p>\n	Go to main page</p>\n','',1,1),(254,2,'<p>\n	Click on questionmark on bottom left corner</p>\n','<p>\n	Textfield is opened</p>\n',1,1),(255,3,'<p>\n	Write some text to textfield and click &quot;next&quot;</p>\n','<p>\n	Your message is sent to technical support</p>\n',1,1),(259,1,'<p>\n	Click on localization button on left (below log out button)</p>\n','',1,1),(260,2,'<p>\n	Select another language</p>\n','',1,1),(261,3,'<p>\n	Log out</p>\n','<p>\n	login &amp; register pages should now be on selected language</p>\n',1,1),(264,1,'<p>\n	The user clicks the weird symbol with letter &#39;A&#39; on the toolbar on the left border of Contriboard</p>\n','<p>\n	A list of flags representing languages appears to the toolbar</p>\n',1,1),(265,2,'<p>\n	The user clicks a flag symbol representing a language</p>\n','<p>\n	The strings displayed in the contriboard change according to the selected language if the user did not choose the previously used language</p>\n',1,1),(269,1,'<p>\n	login with exiting account</p>\n','<p>\n	You get in to contriboard</p>\n',1,1),(270,2,'<p>\n	Create new board or use existing</p>\n','<p>\n	New board or existing board opens</p>\n',1,1),(271,3,'<p>\n	Create new ticket or use existing</p>\n','<p>\n	Ticket options opens</p>\n',1,1),(272,4,'<p>\n	Click color you wish to have and press apply</p>\n','<p>\n	Ticket color should be changed</p>\n',1,1),(277,1,'<p>\n	Press the &quot;Share board&quot;-button&nbsp;</p>\n','<p>\n	&quot;Share board&quot;-dialog appears.</p>\n',1,1),(278,2,'<p>\n	Press &quot;Share&quot;-button</p>\n','<p>\n	Link appears appears to &quot;Shared link&quot;-textbox</p>\n',1,1),(279,3,'<p>\n	Write the URL to second browser and open it. Log in as guest.</p>\n','<p>\n	The other browser should open the same board</p>\n',1,1),(280,4,'<p>\n	Go to your first browser and press &quot;Hide&quot;-button on the &quot;Share board&quot;-dialog</p>\n','<p>\n	URL should disappear from &quot;Shared link&quot;-textbox</p>\n',1,1),(281,5,'<p>\n	Open your second browser again and try to open the same URL as before. Log in as quest if you can.</p>\n','<p>\n	You should not be able to see the board. Browser takes you to &quot;Login&quot;-page.</p>\n',1,1),(282,6,'<p>\n	Go to your first browser and press &quot;Share&quot;-button.</p>\n','<p>\n	URL should appear to &quot;Shared Link&quot;-textbox</p>\n',1,1),(283,7,'<p>\n	Open your second browser and use the<strong> first </strong>URL of this testcase. Login as guest.</p>\n','<p>\n	Browser should open the board.</p>\n',1,1),(284,8,'<p>\n	Leave the website in your second browser and then copy the new URL from your first browser. Paste this in your second browser and login as guest.</p>\n','<p>\n	<span style=\"font-size: 11.6999998092651px;\">You cannot join the same board with one of the URL&#39;s --&gt; FAIL.</span></p>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">Every URL takes you to same board&nbsp;</span>--&gt; PASS.</div>\n',1,1),(287,1,'<p>\n	Create board by pressing the Create board button on top right corner and name it as you like and press Create.</p>\n','<p>\n	Board is created and appears.</p>\n',1,1),(288,2,'<p>\n	Select the board with the check icon.</p>\n','<p>\n	Board is selected and the select icon is active.</p>\n',1,1),(289,3,'<p>\n	Click the note icon on top toolbar.</p>\n','<p>\n	Edit board view opens up.</p>\n',1,1),(290,4,'<p>\n	Press generate.</p>\n','<p>\n	Url is generated.</p>\n',1,1),(291,5,'<p>\n	Press copy.</p>\n','<p>\n	Url is copied to the users clipboard.</p>\n',1,1),(292,6,'<p>\n	Give the board a new name.</p>\n','<p>\n	Input is shown in the correct field.</p>\n',1,1),(293,7,'<p>\n	Press Save changes.</p>\n','<p>\n	New name is saved.</p>\n',1,1),(294,8,'<p>\n	Open new browser window and paste the generated link into the address bar.</p>\n','<p>\n	Contriboard login screen is displayed.</p>\n',1,1),(295,9,'<p>\n	log in as quest.</p>\n','<p>\n	User is signed in to Contriboard</p>\n',1,1),(296,10,'<p>\n	Repeat 10 times with different boards.</p>\n','<p>\n	See steps 1-9.</p>\n',1,1),(297,11,'<p>\n	Complete the test</p>\n','<p>\n	If correct board shows up with changed name everytime: PASS otherwise FAIL.</p>\n',1,1),(300,1,'<p>\n	User 1 selects a board for modifying and copys the sharing link.</p>\n','',1,1),(301,2,'<p>\n	User 1 deletes the board and shares the link for User 2.</p>\n','',1,1),(302,3,'<p>\n	User 2 opens the link.</p>\n','<p>\n	If another user is not able to view deleted board, test will PASS. If other way, test will FAIL.</p>\n',1,1),(305,1,'<p>\n	Select the board &quot;Testi 2&quot;, and go to its settings.</p>\n','',1,1),(306,2,'<p>\n	Generate a link for the board and copy that link to clipboard.</p>\n','',1,1),(307,3,'<p>\n	Clear the link for this board.</p>\n','',1,1),(308,4,'<p>\n	Paste the link into a browser and attempt to view the board.</p>\n','<p>\n	Cannot view board.</p>\n',1,1),(311,1,'<p>\n	Press &quot;Share&quot;-button, then &quot;Share&quot;-button again and copy paste the url to igocnito tab in your browser.</p>\n','<p>\n	Url opens the board in your&nbsp;igocnito tab</p>\n',1,1),(312,2,'<p>\n	Press hide and try to open the URL in your igocnito tab</p>\n','<p>\n	URL disappear and you cannot access the page anymore in your igocnito tab --&gt; Pass</p>\n<p>\n	URL doesnt disappear or error --&gt; Fail</p>\n',1,1),(315,1,'<p>\n	Create new board.</p>\n','',1,1),(316,2,'<p>\n	Select the new board and click Edit Board</p>\n','<p>\n	Edit board dialog should open</p>\n',1,1),(317,3,'<p>\n	Generate new sharing link and copy it.</p>\n','',1,1),(318,4,'<p>\n	Click Save changes.</p>\n','',1,1),(319,5,'<p>\n	Open new private/incognito window in your browser.</p>\n','',1,1),(320,6,'<p>\n	Paste the shared link to the new window.</p>\n','<p>\n	Contriboard guest login page should open</p>\n',1,1),(321,7,'<p>\n	Log in with username &quot;Tester&quot;</p>\n','<p>\n	Shared board should open.</p>\n',1,1),(322,8,'<p>\n	Create new ticket with heading &quot;test&quot;</p>\n','',1,1),(323,9,'<p>\n	Go back to original window.</p>\n','',1,1),(324,10,'<p>\n	Select the board you created previously and click Edit Board.</p>\n','',1,1),(325,11,'<p>\n	Click clear.</p>\n','<p>\n	Sharing link should disappear.</p>\n',1,1),(326,12,'<p>\n	Click Save changes</p>\n','',1,1),(327,13,'<p>\n	Go to the private/incognito window.</p>\n','',1,1),(328,14,'<p>\n	Try to move the ticket you created</p>\n','<p>\n	Guest user should be logged out and forwarded to log in page.</p>\n',1,1),(332,1,'<p>\n	Select <em>Create ticket -</em>button</p>\n','<p>\n	The dialog for creating a ticket will open.</p>\n',1,1),(333,2,'<p>\n	Pick up blue for background color and write to <em>Add a heading</em> -field &quot;Ticket 1&quot;. Then select <em>Create</em> -button.</p>\n','<p>\n	A new ticket is created and will appear on the board.</p>\n',1,1),(334,3,'<p>\n	Select <em>Create ticket</em> -button.</p>\n','<p style=\"margin-top: 0px;\">\n	The dialog for creating a ticket will open.</p>\n',1,1),(335,4,'<p>\n	Pick up blue for background color and write to&nbsp;<em>Add a heading</em>&nbsp;-field &quot;Ticket 2&quot;. Then select&nbsp;<em>Create</em>&nbsp;-button.</p>\n','<p style=\"margin-top: 0px;\">\n	A new ticket is created and will appear on the board.</p>\n',1,1),(336,5,'<p>\n	Select&nbsp;<em>Create ticket -</em>button</p>\n','<p style=\"margin-top: 0px;\">\n	The dialog for creating a ticket will open.</p>\n',1,1),(337,6,'<p>\n	Pick up blue for background color and write to&nbsp;<em>Add a heading</em>&nbsp;-field &quot;Ticket 3&quot;. Then select&nbsp;<em>Create</em>&nbsp;-button.</p>\n','<p>\n	A new ticket is created and will appear on the board.</p>\n<p>\n	If user is able to create several tickets to the board, test will PASS. If the tickets are not created, test will FAIL.</p>\n',1,1),(340,1,'<p>\n	Create four tickets by douple clicking the board four times.</p>\n','<p>\n	Four tickets are created&nbsp;</p>\n',1,1),(341,2,'<p>\n	Click &quot;Snap To Grid&quot;-button on left top corner. It looks like a magnet.</p>\n','<p>\n	&quot;Snap to grip&quot;-button should be highlighted</p>\n',1,1),(342,3,'<p>\n	Drag the tickets around and see if the magnet works.</p>\n','<p>\n	Tickets should rearrange themselfs when user moves them around. They should &quot;stick&quot; next to each other if moved near eachother.</p>\n',1,1),(345,1,'<p>\n	The user drags a ticket by pressing left button of a mouse</p>\n','<p>\n	The ticket is attached to mouse pointer</p>\n',1,1),(346,2,'<p>\n	The user drags the ticket to the border of the board</p>\n','<p>\n	The ticket follows the mouse pointer</p>\n',1,1),(347,3,'<p>\n	The user tries to move the ticket over the board border and releases the left button of the mouse</p>\n','<p>\n	The ticket returns within the board borders after the left mouse button is released</p>\n',1,1),(350,1,'<p>\n	Click &#39;Create a new ticket&#39;.</p>\n','<p>\n	Ticket creation opens.</p>\n',1,1),(351,2,'<p>\n	Paste or write a ridiculously huge amount of text in it.</p>\n','',1,1),(352,3,'<p>\n	Click &#39;Create&#39;.</p>\n','<p>\n	Warns that the ticket has too much text. Does not allow creation.</p>\n',1,1),(355,1,'<p>\n	Select a board by clicking in the middle of it in order to display it.</p>\n','<p>\n	Board opens up.</p>\n',1,1),(356,2,'<p>\n	User clicks the Create Ticket button located in upper right corner of the layout.</p>\n','<p>\n	Ticket creating view opens.</p>\n',1,1),(357,3,'<p>\n	Type into the heading field a text ~200 characters long (alternating between usage of spaces and without) and press Create button.</p>\n','<p>\n	Ticket is created and is displayed on the board. Not escaping the element.</p>\n',1,1),(358,4,'<p>\n	Repeat steps 2-3 10 times.</p>\n','<p>\n	See steps 2-3.</p>\n',1,1),(359,5,'<p>\n	If the ticket appears and is displayed properly without any problems: PASS , otherwise FAIL.</p>\n','<p>\n	Result: FAILED, if the heading includes spaces, the text is properly shortened and cut, but when the heading has no spaces, it escapes the ticket element everytime. For example, if the title is a one long compund word over 30 characters long, this might be a problem.</p>\n',1,1),(362,1,'<p>\n	Select a board</p>\n','',1,1),(363,2,'<p>\n	Open board view with 2 browsers</p>\n','',1,1),(364,3,'<p>\n	Create new ticket</p>\n','',1,1),(365,4,'<p>\n	Move ticket around the board view in other browser</p>\n','',1,1),(366,5,'<p>\n	Refresh the other browser</p>\n','<p>\n	Tickets new location should be same as in the browser in which is was moved</p>\n',1,1),(369,1,'<p>\n	1. Click &quot;Create ticket&quot; -button</p>\n','<p>\n	Add ticket -dialog should appear</p>\n',1,1),(370,2,'<p>\n	Add the following string to heading:&nbsp;&aring;指事字&amp;joo#ei(=1) &ouml;</p>\n<p>\n	Press &quot;create&quot;</p>\n','<p>\n	Ticket should become visible to top-left corner of the board.</p>\n<p>\n	Heading should be shown correctly (no url-encoded chars etc.)</p>\n',1,1),(373,1,'<p>\n	login with existing account</p>\n','<p>\n	You get in contriboard first page</p>\n',1,1),(374,2,'<p>\n	Select existing board by double clicking it</p>\n','<p>\n	Board opens</p>\n',1,1),(375,4,'<p>\n	Select existing ticket and click it</p>\n','<p>\n	Ticket options opens</p>\n',1,1),(376,5,'<p>\n	Write new name and press apply</p>\n','<p>\n	Ticket name should be changed</p>\n',1,1),(379,1,'<p>\n	Select a ticket you want to delete by clicking the &quot;tick&quot;-button on the ticket</p>\n','<p>\n	More options should show up on the top bar</p>\n',1,1),(380,2,'<p>\n	Click the &quot;trashbin&quot;-button</p>\n','<p>\n	Deletion menu should show up</p>\n',1,1),(381,3,'<p>\n	Press the &quot;delete&quot;-button in the deletion menu</p>\n','<p>\n	Ticket should now be deleted</p>\n',1,1),(382,4,'<p>\n	Log out and log back in to check if the ticket is deleted</p>\n','<p>\n	Ticket should be gone forever</p>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	Couldn&#39;t delete a ticket --&gt; FAIL.</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">A ticket has been deleted&nbsp;</span>--&gt; PASS.</div>\n',1,1),(385,1,'<p>\n	Create board</p>\n','',1,1),(386,2,'<p>\n	Create new ticket</p>\n','',1,1),(387,3,'<p>\n	Open ticket with both browsers</p>\n','<p>\n	Ticket is opened and can be edited</p>\n',1,1),(388,4,'<p>\n	Edit ticket and save changes in one &nbsp;browser</p>\n','',1,1),(389,5,'<p>\n	refresh ticket view with other browser</p>\n','<p>\n	Ticket should have info saved in previous step</p>\n',1,1),(392,1,'<p>\n	Click the ticket with heading &quot;joo&quot;</p>\n','<p>\n	Ticket edit -dialog should become visible. Dialog&#39;s top should be blue.</p>\n',1,1),(393,2,'<p>\n	Click the color yellow</p>\n','<p>\n	Edit-dialog&#39;s top should become yellow</p>\n',1,1),(394,3,'<p>\n	Press &quot;apply&quot;</p>\n','<p>\n	Tickets top should change from blue to yellow</p>\n',1,1),(395,4,'<p>\n	Click the ticket again for editing</p>\n','<p>\n	Edit dialog should pop up, top should be yellow</p>\n',1,1),(396,5,'<p>\n	Click purple</p>\n','<p>\n	Dialog&#39;s top should become purple</p>\n',1,1),(397,6,'<p>\n	Click cancel</p>\n','<p>\n	Dialog should close. Ticket&#39;s color should still be yellow.</p>\n',1,1),(398,7,'<p>\n	Click the ticket again for editing</p>\n','<p>\n	Edit dialog should pop up, top of the dialog should be yellow</p>\n',1,1),(401,1,'<p>\n	Select a ticket which color you want to change</p>\n','<p>\n	Contriboard should show the ticket edit menu</p>\n',1,1),(402,2,'<p>\n	Choose a color from the color palette</p>\n','<p>\n	Contriboard should display the change immediately</p>\n',1,1),(403,3,'<p>\n	Click the &quot;apply&quot;-button to save the changes</p>\n','<p>\n	Ticket color should be now changed</p>\n',1,1),(404,4,'<p>\n	Log out and log back in to check if the color of the ticket is changed</p>\n','<p>\n	The ticket should still have the new color</p>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	Couldn&#39;t edit a ticket color --&gt; FAIL.</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">A ticket color has been changed&nbsp;</span>--&gt; PASS.</div>\n<div>\n	&nbsp;</div>\n',1,1),(407,1,'<p>\n	User 2 double click board to create ticket</p>\n','<p>\n	ticket shows</p>\n',1,1),(408,2,'<p>\n	User 1 select the board and delete it.</p>\n','<p>\n	Board should be deleted.</p>\n',1,1),(409,3,'<p>\n	User 2 select&nbsp;<em>Create</em></p>\n','<p>\n	User 2 will be logged out from service.</p>\n<p>\n	If product can handle the situation, test will PASS. If there are unexpected result, test will FAIL.</p>\n',1,1),(412,1,'<p>\n	User 2 select a ticket for modifying.</p>\n','',1,1),(413,2,'<p>\n	User 1 select the board for modifying and <em>Clear </em>the sharing link.</p>\n','',1,1),(414,3,'<p>\n	User 2 select <em>Apply</em>.</p>\n','<p>\n	User 2 will be logged out from service. The ticket will not be modified.</p>\n<p>\n	If another user is not able to modify a ticket after owner has clear the sharing link, test will PASS. If there are unexpected result, test will FAIL.</p>\n',1,1),(417,1,'<p>\n	Create a ticket 100 character long heading without spaces</p>\n','<p>\n	The created ticket&#39;s heading text doesn&#39;t go outside the ticket</p>\n',1,1),(420,1,'<p>\n	Create several tickets on a board and make them overlap each other.</p>\n','<p>\n	Tickets overlap each other with no problems.</p>\n<div>\n	Tickets overlap each other with no issues, moving overlapping tickets works OK ► PASS.</div>\n<div>\n	Tickets flicker while overlapping, or moving them causes problems ► FAIL.</div>\n<div>\n	&nbsp;</div>\n',1,1),(423,1,'<p>\n	User 1 selects the ticket for deleting and select delete.</p>\n','<p>\n	Deleting dialog will open for User 1.</p>\n',1,1),(424,2,'<p>\n	User 2 selects the same ticket for deleting and selects delete.</p>\n','<p>\n	Deleting dialog will open for User 2.</p>\n',1,1),(425,3,'<p>\n	User 1 confirms deleting by selecting Delete button.</p>\n','<p>\n	Ticket will be removed.</p>\n<p>\n	User 1 will view board.</p>\n<p>\n	User 2 will view deleting dialogbox.</p>\n',1,1),(426,4,'<p>\n	User 2 confirms deleting by selecting Delete button.</p>\n','<p>\n	User 2 will view board.</p>\n<p>\n	If product can handle the situation, test will PASS. If there are unexpected result, test will FAIL.</p>\n',1,1),(429,1,'<p>\n	Drag and drop selected ticket</p>\n','<p>\n	Selected ticket is moved to dragged position</p>\n<p>\n	Selected ticket is moved to new position --&gt; Pass</p>\n<p>\n	Selected ticket is <strong>NOT</strong> moved to new position --&gt; Fail</p>\n',1,1),(432,1,'<p>\n	Create 3 tickets</p>\n','<p>\n	The tickets show up (might be on top of each other)</p>\n',1,1),(433,2,'<p>\n	Move tickets next to each other in a row</p>\n','<p>\n	The tickets are in a row</p>\n',1,1),(434,3,'<p>\n	Go to board listing view</p>\n','<p>\n	You are in board listing view</p>\n',1,1),(435,4,'<p>\n	Look at the board &quot;mini map&quot;</p>\n','<p>\n	The tickets show up in the board image and they are next to each other in a row</p>\n',1,1),(438,1,'<p>\n	user opens incognito tab with access to the board as quest and opens a ticket for editing it.</p>\n','<p>\n	Ticket opens.</p>\n',1,1),(439,2,'<p>\n	Attempt to delete the ticket with normal user while the quest user(incognito tab) is editing it.</p>\n','<div>\n	Ticket is deleted and no errors occur OR ticket cannot be deleted while it is being edited ► PASS.</div>\n<div>\n	System crash OR some errors occur with any of the ticket users ► FAIL.</div>\n<div>\n	&nbsp;</div>\n',1,1),(442,1,'<p>\n	Open board &quot;minimap&quot; by clicking the X-like icon on top-right corner of the board</p>\n','<p>\n	Minimap should expand and show one red block</p>\n',1,1),(443,2,'<p>\n	Change ticket color to blue</p>\n','<p>\n	Minimap block should turn to blue</p>\n',1,1),(446,1,'<p>\n	Click ticket</p>\n','<p>\n	A dialog opens</p>\n',1,1),(447,2,'<p>\n	Activate text field</p>\n','<p>\n	Text field is activated</p>\n',1,1),(448,3,'<p>\n	Change the text</p>\n','<p>\n	Text is changed</p>\n',1,1),(449,4,'<p>\n	Click Apply</p>\n','<p>\n	Dialog closes and ticket text changes --&gt; Pass</p>\n<p>\n	&nbsp;</p>\n',1,1),(452,1,'<p>\n	Click create ticket.</p>\n','<p>\n	Ticket create window should pop up.</p>\n',1,1),(453,2,'<p>\n	Do not fill any fields, Click Create.</p>\n','<p>\n	PASS: empty ticket is created.</p>\n<p>\n	FAIL: ticket is not created or error.</p>\n<p>\n	&nbsp;</p>\n',1,1),(456,1,'<p>\n	Select &nbsp;a template (dont care which one)</p>\n','<p>\n	Template loads properly</p>\n',1,1),(457,2,'<p>\n	Create couple tickets</p>\n','<p>\n	Tickets created properly</p>\n',1,1),(458,3,'<p>\n	Switch template to anything else than the previous one</p>\n','<p>\n	Template changes, tickets stay the same</p>\n<p>\n	If tickets work properly -&gt; PASS</p>\n<p>\n	If tickets do something wonky -&gt; FAIL</p>\n',1,1),(463,1,'<p>\n	Use markdown formatting and test that every &quot;code&quot; works&nbsp;</p>\n','',1,1),(466,1,'<p>\n	Create three tickets with headers: Ticket1, Ticket2 and Ticket 3. Also add this content without quotes to each tickets:<br />\n	<br />\n	&quot;Testaajallaainaraikaskossupullokuuumottelee</p>\n<p>\n	repussaodotteleeetestaajaanauttimaansit&auml;el&auml;m&auml;neliksiiiri&auml;&quot;</p>\n','<p>\n	Tickets created and contents added</p>\n',1,1),(467,2,'<p>\n	Choose different colors to each tickets: Ticket1=Blue, Ticket2=Yellow, Ticket3=Red.</p>\n','<p>\n	Tickets have different colors</p>\n',1,1),(468,3,'<p>\n	Open test enviroment to an other incognito window in your browser. Share board under testing and copy the link to your incognito browser.<br />\n	<br />\n	Enter board as a guest.</p>\n','<p>\n	Board under test open in two browser windows.</p>\n',1,1),(469,4,'<p>\n	Open ticket review in one of the browsers</p>\n','<p>\n	Ticket review opened</p>\n',1,1),(470,5,'<p>\n	Verify that ticket content is shown in review</p>\n','<p>\n	Ticket content is being presented as it should.</p>\n',1,1),(471,6,'<p>\n	Verify tickets have right colors in ticket review.</p>\n','<p>\n	Tickets have right colors.</p>\n',1,1),(472,7,'<p>\n	Open blue ticket in ticket review browser and use another incognito browser tab to write a comment to the blue ticket(Ticket1) while reviewing.</p>\n<p>\n	As the comment is added it should also apply to the ticket review window in real time.</p>\n','<p>\n	Comment added to ticket review when reviewing and commenting at the same time.</p>\n',1,1),(473,8,'<p>\n	Verify that ticket navigation using arrows in left and right end of page are working</p>\n','<p>\n	Navigation is working</p>\n',1,1),(474,9,'<p>\n	Verify that ticket navigation using swipe is working using different types of devices (touchscreen, mouse etc)</p>\n','<p>\n	Swipe feature is working</p>\n',1,1),(475,10,'<p>\n	Exit ticket review feature using X button</p>\n','<p>\n	Exit button is working</p>\n',1,1),(479,1,'<p>\n	Press &quot;Export board&quot;-button&nbsp;</p>\n','<p>\n	&quot;Export board&quot;-dialog opens</p>\n',1,1),(480,2,'<p>\n	Choose JSON from dropbox.</p>\n','<p>\n	&quot;JSON&quot; should be visible in dropbox&nbsp;</p>\n',1,1),(481,3,'<p>\n	Press &quot;Export&quot;-button</p>\n','<p>\n	Your browser should start downloading the file you exported.</p>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;You couldnt download the file--&gt; FAIL.</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">&nbsp;</span><span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">You succesfully downloaded the file&nbsp;</span><span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">--&gt; PASS.</span></div>\n',1,1),(484,1,'<p>\n	Press &quot;Export board&quot;-button&nbsp;</p>\n','<p>\n	&quot;Export board&quot;-dialog opens</p>\n',1,1),(485,2,'<p>\n	Choose CSV from dropbox.</p>\n','<p>\n	&quot;CSV&quot; should be visible in dropbox&nbsp;</p>\n',1,1),(486,3,'<p>\n	Press &quot;Export&quot;-button</p>\n','<p>\n	Your browser should start downloading the file you exported.<span style=\"font-size: 11.6999998092651px;\">&nbsp;</span></p>\n<p>\n	<span style=\"font-size: 11.6999998092651px;\">You couldnt download the file--&gt; FAIL.</span></p>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">&nbsp;</span><span style=\"font-size: 11.6999998092651px;\">You succesfully downloaded the file&nbsp;</span><span style=\"font-size: 11.6999998092651px;\">--&gt; PASS.</span></div>\n',1,1),(489,1,'<p>\n	Press &quot;export&quot;-button</p>\n','<p>\n	Pass = Export dialog shows.</p>\n<p>\n	Fail = Export dialog does NOT show or error.</p>\n',1,1),(492,1,'<p>\n	Press &quot;export&quot;-button</p>\n','<p>\n	export dialog opens</p>\n',1,1),(493,2,'<p>\n	choose image from drop box and press export</p>\n','<p>\n	file should start download.</p>\n',1,1),(494,3,'<p>\n	Check that image exported matches users board&nbsp;</p>\n','<p>\n	If picture looks the same as the board --&gt; Pass</p>\n<p>\n	If picture does not look the same as the board --&gt; Fail</p>\n',1,1),(497,1,'<p>\n	Create ticket</p>\n','<p>\n	Ticket appears</p>\n',1,1),(498,2,'<p>\n	Open the ticket</p>\n','<p>\n	Ticket opens</p>\n',1,1),(499,3,'<p>\n	Write something on the ticket.</p>\n','<p>\n	Text appears.</p>\n',1,1),(500,4,'<p>\n	Press dublicate ticket.</p>\n','<p>\n	Pass --&gt; Ticket dublicates with the same text on it.</p>\n<p>\n	Fail --&gt; something else</p>\n',1,1),(504,1,'<p>\n	Write something in to the comment field.</p>\n','<p>\n	Text should apper in the textfield&nbsp;</p>\n',1,1),(505,2,'<p>\n	&nbsp;Press &quot;Add Comment&quot;-button.</p>\n','<p>\n	Comment should be added below and show user, comment and timestamp.</p>\n',1,1),(509,1,'<p>\n	Create new ticket as the logged in user.</p>\n','<p>\n	A ticket appears in the guest view.</p>\n',1,1),(510,2,'<p>\n	Move the ticket.</p>\n','<p>\n	The ticket moves in the guest view.</p>\n',1,1),(511,3,'<p>\n	Change the name of the ticket.</p>\n','<p>\n	The name is changed in the guest view.</p>\n',1,1),(512,4,'<p>\n	Delete the ticket.</p>\n','<p>\n	Ticket vanishes from the guest view.</p>\n',1,1),(515,1,'<p>\n	Select a ticket which color you want to change</p>\n','<p>\n	Contriboard should show the ticket edit menu</p>\n',1,1),(516,2,'<p>\n	Choose a color from the color palette</p>\n','<p>\n	Contriboard should display the change immediately</p>\n',1,1),(517,3,'<p>\n	Click the &quot;apply&quot;-button to save the changes</p>\n','<p>\n	Ticket color should be now changed</p>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	Couldn&#39;t edit a ticket color --&gt; FAIL.</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">A ticket color has been changed&nbsp;</span>--&gt; PASS.</div>\n',1,1),(520,1,'<p>\n	Select the ticket which description you want to edit</p>\n','<p>\n	Contriboard should show the menu for the ticket</p>\n',1,1),(521,2,'<p>\n	Click the ticket name and edit it</p>\n','<p>\n	Success</p>\n',1,1),(522,3,'<p>\n	Click the &quot;apply&quot;-button to save changes</p>\n','<p>\n	The name is now changed on the board</p>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	Couldn&#39;t edit a ticket name --&gt; FAIL.</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">A ticket has been renamed&nbsp;</span>--&gt; PASS.</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n',1,1),(525,1,'<p>\n	Press the &quot;tick&quot;-button</p>\n','<p>\n	More option should be available at the top bar</p>\n',1,1),(526,2,'<p>\n	Press the &quot;trashbin&quot;-button</p>\n','<p>\n	Deletion-menu should show up</p>\n',1,1),(527,3,'<p>\n	Press &quot;delete&quot;-button</p>\n','<p>\n	Ticket should now be deleted from board</p>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	The&nbsp;<span style=\"font-size: 11.6999998092651px;\">ticket</span>&nbsp;wasn&#39;t deleted --&gt; FAIL.</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">The&nbsp;<span style=\"font-size: 11.6999998092651px;\">ticket</span>&nbsp;was deleted forever&nbsp;</span>--&gt; PASS.</div>\n',1,1),(530,1,'<p>\n	Enter the shared-link to your web-browser</p>\n','<p>\n	You should see the log in for guests</p>\n',1,1),(531,2,'<p>\n	Enter a name and press the &quot;sign in&quot;-button</p>\n','<p>\n	You should see the board-view</p>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	Guest cannot access a board --&gt; FAIL.</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<span style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">Guest can access a board&nbsp;</span>--&gt; PASS.</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n',1,1),(569,1,'<p>\n	-set screen resolution to 720p</p>\n','',1,1),(570,2,'<p>\n	-open contriboard</p>\n','<p>\n	-contriboard opened</p>\n',1,1),(571,3,'<p>\n	-check that every text and module looks right and is scaled correctly.</p>\n','',1,1),(572,4,'<p>\n	-set screen resolution to 1080p</p>\n','',1,1),(573,5,'<p>\n	-open contriboard</p>\n','',1,1),(574,6,'<p>\n	-check that every text and module looks right and is scaled correctly.</p>\n','',1,1),(575,7,'<p>\n	-set screen resolution to 1440p</p>\n','',1,1),(576,8,'<p>\n	-open contriboard</p>\n','',1,1),(577,9,'<p>\n	-check that every text and module looks right and is scaled correctly.</p>\n','',1,1),(578,10,'<p>\n	-set screen resolution to 2160p</p>\n','',1,1),(579,11,'<p>\n	-open contriboard</p>\n','',1,1),(580,12,'<p>\n	-check that every text and module looks right and is scaled correctly.</p>\n','',1,1),(583,1,'<p>\n	-set screen resolution to 720p</p>\n','',1,1),(584,2,'<p>\n	-open contriboard</p>\n','',1,1),(585,3,'<p>\n	-check that every text and module looks right and is scaled correctly.</p>\n','',1,1),(586,4,'<p>\n	-set screen resolution to 1080p</p>\n','',1,1),(587,5,'<p>\n	-open contriboard</p>\n','',1,1),(588,6,'<p>\n	-check that every text and module looks right and is scaled correctly.</p>\n','',1,1),(589,7,'<p>\n	-set screen resolution to 1440p</p>\n','',1,1),(590,8,'<p>\n	-open contriboard</p>\n','',1,1),(591,9,'<p>\n	-check that every text and module looks right and is scaled correctly.</p>\n','',1,1),(592,10,'<p>\n	-set screen resolution to 2160p</p>\n','',1,1),(593,11,'<p>\n	-open contriboard</p>\n','',1,1),(594,12,'<p>\n	-check that every text and module looks right and is scaled correctly.</p>\n','',1,1),(597,1,'<p>\n	-set screen resolution to 720p</p>\n','',1,1),(598,2,'<p>\n	-open contriboard</p>\n','<p>\n	-contriboard opened</p>\n',1,1),(599,3,'<p>\n	-check that every text and module looks right and is scaled correctly.</p>\n','',1,1),(600,4,'<p>\n	-set screen resolution to 1080p</p>\n','',1,1),(601,5,'<p>\n	-open contriboard</p>\n','',1,1),(602,6,'<p>\n	-check that every text and module looks right and is scaled correctly.</p>\n','',1,1),(603,7,'<p>\n	-set screen resolution to 1440p</p>\n','',1,1),(604,8,'<p>\n	-open contriboard</p>\n','',1,1),(605,9,'<p>\n	-check that every text and module looks right and is scaled correctly.</p>\n','',1,1),(606,10,'<p>\n	-set screen resolution to 2160p</p>\n','',1,1),(607,11,'<p>\n	-open contriboard</p>\n','',1,1),(608,12,'<p>\n	-check that every text and module looks right and is scaled correctly.</p>\n','',1,1),(611,1,'<p>\n	-set screen resolution to 720p</p>\n','',1,1),(612,2,'<p>\n	-open contriboard</p>\n','<p>\n	-contriboard opened</p>\n',1,1),(613,3,'<p>\n	-check that every text and module looks right and is scaled correctly.</p>\n','',1,1),(614,4,'<p>\n	-set screen resolution to 1080p</p>\n','',1,1),(615,5,'<p>\n	-open contriboard</p>\n','',1,1),(616,6,'<p>\n	-check that every text and module looks right and is scaled correctly.</p>\n','',1,1),(617,7,'<p>\n	-set screen resolution to 1440p</p>\n','',1,1),(618,8,'<p>\n	-open contriboard</p>\n','',1,1),(619,9,'<p>\n	-check that every text and module looks right and is scaled correctly.</p>\n','',1,1),(620,10,'<p>\n	-set screen resolution to 2160p</p>\n','',1,1),(621,11,'<p>\n	-open contriboard</p>\n','',1,1),(622,12,'<p>\n	-check that every text and module looks right and is scaled correctly.</p>\n','',1,1),(625,1,'<p>\n	-set screen resolution to 720p</p>\n','',1,1),(626,2,'<p>\n	-open contriboard</p>\n','<p>\n	-contriboard opened</p>\n',1,1),(627,3,'<p>\n	-check that every text and module looks right and is scaled correctly.</p>\n','',1,1),(628,4,'<p>\n	-set screen resolution to 1080p</p>\n','',1,1),(629,5,'<p>\n	-open contriboard</p>\n','',1,1),(630,6,'<p>\n	-check that every text and module looks right and is scaled correctly.</p>\n','',1,1),(631,7,'<p>\n	set screen resolution to 1440p</p>\n','',1,1),(632,8,'<p>\n	-open contriboard</p>\n','',1,1),(633,9,'<p>\n	check that every text and module looks right and is scaled correctly.</p>\n','',1,1),(634,10,'<p>\n	-set screen resolution to 2160p</p>\n','',1,1),(635,11,'<p>\n	-open contriboard</p>\n','',1,1),(636,12,'<div>\n	-check that every text and module looks right and is scaled correctly.</div>\n<div>\n	&nbsp;</div>\n','',1,1),(639,1,'<p>\n	Find a bot to create alot of accounts</p>\n','<p>\n	System either crashes/slows down alot, or works as it should.</p>\n',1,1);
/*!40000 ALTER TABLE `tcsteps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tcversions`
--

DROP TABLE IF EXISTS `tcversions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tcversions` (
  `id` int(10) unsigned NOT NULL,
  `tc_external_id` int(10) unsigned DEFAULT NULL,
  `version` smallint(5) unsigned NOT NULL DEFAULT '1',
  `layout` smallint(5) unsigned NOT NULL DEFAULT '1',
  `status` smallint(5) unsigned NOT NULL DEFAULT '1',
  `summary` text,
  `preconditions` text,
  `importance` smallint(5) unsigned NOT NULL DEFAULT '2',
  `author_id` int(10) unsigned DEFAULT NULL,
  `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updater_id` int(10) unsigned DEFAULT NULL,
  `modification_ts` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `is_open` tinyint(1) NOT NULL DEFAULT '1',
  `execution_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 -> manual, 2 -> automated',
  `estimated_exec_duration` decimal(6,2) DEFAULT NULL COMMENT 'NULL will be considered as NO DATA Provided by user',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tcversions`
--

LOCK TABLES `tcversions` WRITE;
/*!40000 ALTER TABLE `tcversions` DISABLE KEYS */;
INSERT INTO `tcversions` VALUES (5,435,1,1,1,'<p>\n	You will find current &nbsp;master test plan from&nbsp;<a href=\"https://github.com/N4SJAMK/teamboard-meta/wiki/master-test-plan\">GitHub Wiki</a></p>\n','',2,1,'2016-11-10 13:46:54',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(9,408,1,1,1,'<p>\n	Robot framework scenario 1 manually.<br />\n	<br />\n	https://github.com/N4SJAMK/teamboard-test/blob/master/robot-framework/ContriboardTestScenarios/Scenario1.rst<br />\n	<br />\n	&nbsp;</p>\n<p>\n	<iframe allowfullscreen=\"\" frameborder=\"0\" height=\"315\" src=\"https://www.youtube.com/embed/sOd2-sGTPf8\" width=\"560\"></iframe></p>\n','',2,1,'2016-11-10 13:46:54',NULL,'1000-01-01 00:00:00',1,1,2,NULL),(11,409,1,1,1,'<p>\n	Robot framework scenario 2 manually.<br />\n	<br />\n	https://github.com/N4SJAMK/teamboard-test/blob/master/robot-framework/ContriboardTestScenarios/Scenario2.rst</p>\n<p>\n	<iframe allowfullscreen=\"\" frameborder=\"0\" height=\"315\" src=\"https://www.youtube.com/embed/Ks47cmtu9Z0\" width=\"560\"></iframe></p>\n','',2,1,'2016-11-10 13:46:54',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(13,410,1,1,1,'<p>\n	Robot framework scenario 3 manually.<br />\n	<br />\n	https://github.com/N4SJAMK/teamboard-test/blob/master/robot-framework/ContriboardTestScenarios/Scenario3.rst</p>\n<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/uRxTvpyFTi4\" frameborder=\"0\" allowfullscreen></iframe>','',2,1,'2016-11-10 13:46:54',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(15,411,1,1,1,'<p>\n	Robot framework scenario 3 manually.<br />\n	<br />\n	https://github.com/N4SJAMK/teamboard-test/blob/master/robot-framework/ContriboardTestScenarios/Scenario3.rst</p>\n<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/4WVcUBhrg7A\" frameborder=\"0\" allowfullscreen></iframe>','',2,1,'2016-11-10 13:46:54',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(17,412,1,1,1,'<p>\n	Robot framework executing test</p>\n<p>\n	&nbsp;</p>\n<p>\n\n<iframe allowfullscreen=\"\" frameborder=\"0\" height=\"315\" src=\"https://www.youtube.com/embed/8Xwt2pw1pNs\" width=\"560\"></iframe></p>\n','',2,1,'2016-11-10 13:46:54',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(22,229,1,1,1,'<p>\n	Test configuration:</p>\n<p>\n	AMD Phenom(tm) X6 1055T 2.80GHz<br />\n	8Gt RAM<br />\n	Windows 7 64-bit</p>\n<p>\n	Firefox</p>\n','<p>\n	User: <a href=\"mailto:asdf@asdf.fi\">asdf@asdf.fi</a><br />\n	Pass: jorma666<br />\n	Signed in to Contriboard using above info. At least one board created.</p>\n',2,1,'2016-11-10 13:46:54',NULL,'1000-01-01 00:00:00',1,1,1,5.00),(31,216,1,1,1,'<p>\n	Date: 18.02.2015</p>\n<div>\n	&nbsp;</div>\n<div>\n	Comments:</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<div>\n	End State:</div>\n<div>\n	<span style=\"font-size: 11.6999998092651px;\">Long names cut properly and dont escape the board.&nbsp;</span></div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<p>\n	&nbsp;</p>\n','<p>\n	Signed in to Contriboard.</p>\n',2,1,'2016-11-10 13:46:54',NULL,'1000-01-01 00:00:00',1,1,1,5.00),(38,53,1,1,1,'<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<strong style=\"font-size: 11.6999998092651px;\">Date: 21.05.2015</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<strong>Comments:</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	Make sure user can go back to workspace from inside a board</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n','<p>\n	User is logged in to contriboard</p>\n',2,1,'2016-11-10 13:46:54',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(43,52,1,1,1,'<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<strong style=\"font-size: 11.6999998092651px;\">Date: 21.05.2015</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<strong>Comments:</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	Make sure user can delete a board</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n','<p>\n	User is logged in to Contriboard</p>\n',2,1,'2016-11-10 13:46:54',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(49,51,1,1,1,'<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<strong style=\"font-size: 11.6999998092651px;\">Date: 21.05.2015</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<strong>Comments:</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	Make sure user can close the board edit menu by clicking the &quot;cancel&quot;-button or clicking outside of the menu</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<strong>End State:</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	The board edit menu is closed without changing any data</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n','<p>\n	User is logged in to Contriboard.</p>\n',2,1,'2016-11-10 13:46:54',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(54,136,1,1,1,'<p>\n	22.05.2015</p>\n<p>\n	Test Summary:&nbsp;</p>\n<p>\n	Create a ticket with UTF-8 characters in it (片仮名) and verify they show correctly</p>\n<p>\n	Post-condition:&nbsp;</p>\n<p>\n	User is logged in and on the board page (testboard0) and there&#39;s a ticket with the name&nbsp;片仮名</p>\n','<p>\n	<span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">User is logged in and on the board page in Contriboard.</span></p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p style=\"font-size: 16px; box-sizing: border-box; margin-top: 0px; margin-bottom: 16px; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">\n	&nbsp;</p>\n',2,1,'2016-11-10 13:46:54',NULL,'1000-01-01 00:00:00',1,1,1,2.00),(59,339,1,1,1,'<div>\n	Date: 21.02.2015</div>\n<div>\n	&nbsp;</div>\n<div>\n	Comments:</div>\n<div>\n	The user changes the visible name of a board</div>\n','<ul>\n	<li>\n		The user has logged in to Contriboard</li>\n	<li>\n		There is at least one created board by the user account the user uses</li>\n	<li>\n		The user is in the board list view</li>\n</ul>\n',2,1,'2016-11-10 13:46:54',NULL,'1000-01-01 00:00:00',1,1,1,1.00),(65,340,1,1,1,'<div>\n	Date: 21.02.2015</div>\n<div>\n	&nbsp;</div>\n<div>\n	Test for&nbsp;Verify Changing boards background</div>\n','<p>\n	Logged in to Contriboard.</p>\n',2,1,'2016-11-10 13:46:54',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(70,22,1,1,1,'<p>\n	Verify the user can&#39;t open a board that was deleted in another browser tab</p>\n','<p>\n	Be on board listing view</p>\n',2,1,'2016-11-10 13:46:54',NULL,'1000-01-01 00:00:00',1,1,1,1.00),(76,62,1,1,1,'<div>\n	<strong>Date:</strong>&nbsp;21.05.2015</div>\n<div>\n	&nbsp;</div>\n<div>\n	<strong>Comments</strong></div>\n<div>\n	Attempt to delete a board while some other user is editing the board data.</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n','<p>\n	Log in to Contriboard and user has an URL from &quot;share board&quot;-dialog</p>\n',2,1,'2016-11-10 13:46:54',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(80,352,1,1,1,'<p>\n	&nbsp;</p>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<strong style=\"font-size: 11.6999998092651px;\">Date: 18.02.2015</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<strong>Comments:</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	This test verifys what happens when you click empty space while ticket is open.</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n','<div>\n	logged in to contriboard, board and ticket opened</div>\n',2,1,'2016-11-10 13:46:54',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(84,418,1,1,1,'<p>\n	Logged user should not be able to enter a boardview with an url like this: &quot;http://lankku.n4sjamk.org/boards/AnythingYouWantToWrite&quot;&nbsp;</p>\n','<p>\n	User is logged in to contriboard and created an board.</p>\n',2,1,'2016-11-10 13:46:54',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(88,355,1,1,1,'<div>\n	Date: 21.02.2015</div>\n<div>\n	&nbsp;</div>\n<div>\n	Comments:</div>\n<div>\n	Test for clicking empty space while workflow templates window opened</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n','<div>\n	-logged in to contriboard</div>\n<div>\n	-one board opened</div>\n',2,1,'2016-11-10 13:46:55',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(92,70,1,1,1,'<p>\n	Test if opening board to another tab works and the page that opened the tab still works</p>\n','<p>\n	User is logged in and has a board ready</p>\n',2,1,'2016-11-10 13:46:55',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(95,413,1,1,1,'<p>\n	Verify that board scales correctly and tickets are NOT left outside of the board.</p>\n','<p>\n	Logged in to contriboard and opened board</p>\n',2,1,'2016-11-10 13:46:55',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(100,414,1,1,1,'<p>\n	Verify that &quot;&Auml;&Ouml;&Aring;..&quot; can be written as board name.&nbsp;</p>\n','<p>\n	User logged in to contriboard and created an board.</p>\n',2,1,'2016-11-10 13:46:55',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(104,424,1,1,1,'<p>\n	Verify that if more then one users join the board, user will be able to see who is on the board from board members view.</p>\n','<p>\n	Logged in, created an board and inside that board.</p>\n',2,1,'2016-11-10 13:46:55',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(111,268,1,1,1,'<p>\n	&nbsp;</p>\n<div>\n	Date: 18.02.2015</div>\n<div>\n	&nbsp;</div>\n<div>\n	Comments:</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<div>\n	End State:</div>\n<div>\n	&nbsp;</div>\n<div>\n	3rd created board should be visible</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n','<p style=\"box-sizing: border-box;\">\n	User is logged in to Contriboard and there is no created boards</p>\n',2,1,'2016-11-10 13:46:55',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(117,371,1,1,1,'<p>\n	Verify that renaming board is working</p>\n<div>\n	Date: 18.02.2015</div>\n<div>\n	&nbsp;</div>\n<div>\n	Comments:</div>\n<div>\n	Verify that renaming board is working</div>\n<div>\n	&nbsp;</div>\n<div>\n	End State:</div>\n<div>\n	Board&#39;s name changed to selected new one</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<p>\n	&nbsp;</p>\n','<p>\n	Workspace is open and has a board</p>\n',2,1,'2016-11-10 13:46:55',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(123,237,1,1,1,'<p>\n	Test that board name is updated to all users on board.</p>\n<p>\n	&nbsp;</p>\n<div>\n	Date: 18.02.2015</div>\n<div>\n	&nbsp;</div>\n<div>\n	Comments:</div>\n<div>\n	Test that proves; board name is updated to all users on board.</div>\n<div>\n	&nbsp;</div>\n<div>\n	End State:</div>\n<div>\n	Board name matches to the one that was set in incognito tab.</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<p>\n	&nbsp;</p>\n','<p>\n	<span style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">Your browser is open, you have logged in to Contriboard</span><span style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">, created a board with no name.</span></p>\n',2,1,'2016-11-10 13:46:55',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(129,276,1,1,1,'<p>\n	&nbsp;</p>\n<div>\n	Date: 18.02.2015</div>\n<div>\n	&nbsp;</div>\n<div>\n	Comments:</div>\n<div>\n	Creating empty board.</div>\n<div>\n	&nbsp;</div>\n<div>\n	End State:</div>\n<div>\n	User sees new empty nameless board.</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<p>\n	&nbsp;</p>\n','<p style=\"box-sizing: border-box;\">\n	<span style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">Your browser is open, you have logged in.</span></p>\n',2,1,'2016-11-10 13:46:55',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(133,427,1,1,1,'<p>\n	<span style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">This test&#39;s idea is to verify that when user(logged in user) joins another users board, after that he can see the same board on his workspace page.</span></p>\n','<p>\n	Tester creates two account in two browser tabs. One in incognito tab and one in normal.</p>\n',2,1,'2016-11-10 13:46:55',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(140,428,1,1,1,'<p>\n	Create an facebook account or use old one. Register new account to contriboard with facebook account.</p>\n','<p>\n	User has contriboard&#39;s login/register page open. User created/have an facebook account.</p>\n',2,1,'2016-11-10 13:46:55',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(145,126,1,1,1,'<p>\n	&nbsp;</p>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<strong style=\"font-size: 11.6999998092651px;\">Date: 18.02.2015</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<strong>Comments:</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<strong>End State:</strong></div>\n<div>\n	A new account is created and user is logged in with that and redirected to workspace page</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n','<p>\n	Your browser is open, you are on Contriboard&#39;s login page</p>\n',2,1,'2016-11-10 13:46:55',NULL,'1000-01-01 00:00:00',1,1,1,2.00),(155,134,1,1,1,'<p>\n	<span style=\"font-size: 11.6999998092651px;\">Verify Registration with password under 8 characters</span></p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n','<p>\n	Registration page (http://contriboard.n4sjamk.org/register)&nbsp;is opened</p>\n',2,1,'2016-11-10 13:46:55',NULL,'1000-01-01 00:00:00',1,1,1,3.00),(160,88,1,1,1,'<p>\n	&nbsp;</p>\n<div>\n	Date: 18.02.2015</div>\n<div>\n	&nbsp;</div>\n<div>\n	Comments:</div>\n<div>\n	Test for logging out of the contriboard.</div>\n<div>\n	&nbsp;</div>\n<div>\n	End State:</div>\n<div>\n	User logged out</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<p>\n	&nbsp;</p>\n','<p>\n	Your browser is open and you have logged in to Contriboard. No boards open.</p>\n',2,1,'2016-11-10 13:46:55',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(165,372,1,1,1,'<p>\n	&nbsp;</p>\n<div>\n	Date: 18.02.2015</div>\n<div>\n	&nbsp;</div>\n<div>\n	Comments:</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<div>\n	End State:</div>\n<div>\n	User should not be able to register because email is already in use</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n','<p>\n	<span style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">Your browser is open and in registerpage of contriboard.</span></p>\n',2,1,'2016-11-10 13:46:56',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(169,220,1,1,1,'<p>\n	Date: 18.02.2015</p>\n<div>\n	&nbsp;</div>\n<div>\n	Comments:</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<div>\n	End State:</div>\n<div>\n	You cannot register new user while using&nbsp;critical special characters. -&gt; Register failed.</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n','<p>\n	<span style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">Your browser is open, you have opened Contriboard&#39;s registerpage.</span></p>\n',2,1,'2016-11-10 13:46:56',NULL,'1000-01-01 00:00:00',1,1,1,5.00),(178,373,1,1,1,'<p>\n	Date: 18.02.2015</p>\n<div>\n	&nbsp;</div>\n<div>\n	Comments:</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<div>\n	End State:</div>\n<div>\n	&nbsp;</div>\n<div>\n	User is created and redirected to the first page.</div>\n<div>\n	&nbsp;</div>\n<p>\n	&nbsp;</p>\n','<p>\n	<span style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">Your browser is open, you are at Contriboard&#39;s registerpage.</span></p>\n',2,1,'2016-11-10 13:46:56',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(184,374,1,1,1,'<p>\n	Date: 18.02.2015</p>\n<div>\n	&nbsp;</div>\n<div>\n	Comments:</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<div>\n	End State:</div>\n<div>\n	User is redirected to board creation page.</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n','<p>\n	<span style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">Your browser is open and user has working account information(user and password).</span></p>\n',2,1,'2016-11-10 13:46:56',NULL,'1000-01-01 00:00:00',1,1,1,1.00),(189,380,1,1,1,'<p>\n	A test for changing user&#39;s password.</p>\n','<p>\n	An account has been made, user is logged in and profile tab where &quot;password&quot; is selected&nbsp;</p>\n',2,1,'2016-11-10 13:46:56',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(193,84,1,1,1,'<p>\n	Passwords in two password-fields should match when registering and announce it correctly with text.</p>\n<p>\n	&nbsp;</p>\n<p>\n	Tester will do this while registering first time and in the profile tab after registering.</p>\n','<p>\n	User is register-page</p>\n',2,1,'2016-11-10 13:46:56',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(203,231,1,1,1,'<p>\n	<strong style=\"font-size: 11.6999998092651px;\">Date: 18.02.2015</strong></p>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<strong>Comments:</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<strong>End State:</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	User cannot register with non valid email.</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<p>\n	&nbsp;</p>\n','<p>\n	<span style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">Your browser is open and in contriboard&#39;s registerpage</span></p>\n',2,1,'2016-11-10 13:46:56',NULL,'1000-01-01 00:00:00',1,1,1,2.00),(208,271,1,1,1,'<p>\n	<strong style=\"font-size: 11.6999998092651px;\">Date: 18.02.2015</strong></p>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<strong>Comments:</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	Test to try if user can log in with wrong password</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<strong>End State:</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	User cannot log in with wrong password</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n','<p style=\"box-sizing: border-box;\">\n	<span style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">Your browser is open and on contriboard&#39;s loginpage.</span></p>\n',2,1,'2016-11-10 13:46:56',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(213,243,1,1,1,'<p>\n	&nbsp;</p>\n<div>\n	Date: 18.02.2015</div>\n<div>\n	&nbsp;</div>\n<div>\n	Comments:</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<div>\n	End State:</div>\n<div>\n	User is logged out</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n','<p>\n	<span style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">Your browser is open with two tabs(1 normal and 1 incgonito), you have logged in contriboard in both tabs.</span></p>\n',2,1,'2016-11-10 13:46:56',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(217,142,1,1,1,'<p>\n	<strong style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Module Name:</strong><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;Login Session</span></p>\n<p>\n	<strong style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Dependencies:</strong><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;None</span><br style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\" />\n	<strong style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Requirements:</strong><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;Existing user account (</span><a href=\"mailto:testcase0@mailinator.com\" style=\"box-sizing: border-box; color: rgb(65, 131, 196); text-decoration: none; font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background: rgb(255, 255, 255);\">testcase0@mailinator.com</a><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">:salasana1), Existing Board (testboard0)</span><br style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\" />\n	<strong style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Test Data:</strong></p>\n<p>\n	<strong style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Test Summary:</strong><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;Using old user-token to exploit site and edit boards</span></p>\n<p>\n	&nbsp;</p>\n','<p>\n	<strong style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Pre-condition:</strong></p>\n<p>\n	<strong style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; box-sizing: border-box;\">Post-condition:</strong></p>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 16px; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">\n	<strong style=\"box-sizing: border-box;\">Expected Result:</strong>&nbsp;Board cannot be seen and modified with an old user-token.</p>\n',2,1,'2016-11-10 13:46:56',NULL,'1000-01-01 00:00:00',1,1,1,5.00),(235,422,1,1,1,'<p>\n	Create an google account or use old one. Register new account to contriboard with google account.</p>\n','<p>\n	User has contriboard&#39;s login/register page open. User created/have an google account.</p>\n',2,1,'2016-11-10 13:46:56',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(240,425,1,1,1,'<p>\n	Test for changing user avatar</p>\n','<p>\n	Account created and logged in.</p>\n',3,1,'2016-11-10 13:46:56',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(252,385,1,1,1,'','',2,1,'2016-11-10 13:46:56',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(258,386,1,1,1,'','<p>\n	Logged in contriboard</p>\n',2,1,'2016-11-10 13:46:57',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(263,387,1,1,1,'<p>\n	The user changes the language of Contriboard</p>\n','<ul>\n	<li>\n		The user has logged in to Contriboard with username &#39;testing@testing.fi&#39; and password &#39;testingtesting&#39;</li>\n</ul>\n',2,1,'2016-11-10 13:46:57',NULL,'1000-01-01 00:00:00',1,1,1,1.00),(268,284,1,1,1,'<pre style=\"font-family: Arial, Helvetica, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n<code>Testing ticket color changes in Contriboard.\n\n</code></pre>\n','<p>\n	Browser open and in Contriboard&#39;s first page.</p>\n',2,1,'2016-11-10 13:46:57',NULL,'1000-01-01 00:00:00',1,1,1,1.00),(276,17,1,1,1,'<p>\n	Date: 18.02.2015</p>\n<div>\n	&nbsp;</div>\n<div>\n	Comments:</div>\n<div>\n	Make sure that board sharing works, even with &quot;old&quot; URL.</div>\n<div>\n	&nbsp;</div>\n<div>\n	End State:</div>\n<div>\n	&nbsp;</div>\n<div>\n	You have two browsers open, one logged in as user and one as quest.</div>\n<div>\n	&nbsp;</div>\n<p>\n	&nbsp;</p>\n','<p>\n	Your browser is open, you have logged in, created a board and you have opened the board.</p>\n',2,1,'2016-11-10 13:46:57',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(286,222,1,1,1,'<p>\n	22.05.2015</p>\n<p>\n	Verify that URL user created works after editing the name.</p>\n','<p>\n	Signed in to Contriboard.</p>\n',2,1,'2016-11-10 13:46:57',NULL,'1000-01-01 00:00:00',1,1,1,10.00),(299,258,1,1,1,'<div>\n	21.05.2015</div>\n<div>\n	Verifies that user is not able to view the board after the owner has remove it.</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n','<div>\n	User 1 has Contriboard open in browser.</div>\n<div>\n	&nbsp;</div>\n<div>\n	User 1 is viewing boards menu.</div>\n<div>\n	&nbsp;</div>\n<div>\n	User 2 logged in aswell</div>\n',2,1,'2016-11-10 13:46:57',NULL,'1000-01-01 00:00:00',1,1,1,6.00),(304,248,1,1,1,'<p>\n	Verify boards are not viewable after the board&#39;s link has been cleared.</p>\n<p>\n	<strong>Test Configuration:</strong></p>\n<p>\n	Windows 7</p>\n<p>\n	Chrome 40.*</p>\n','<p>\n	Contriboard open in browser, logged with credentials testaaja@grr.la/salasana, looking at the user&#39;s boards.</p>\n',2,1,'2016-11-10 13:46:57',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(310,236,1,1,1,'<p style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	21.05.2015</p>\n<p style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	comments:</p>\n<p style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	Test that board share URL can be removed.</p>\n','',2,1,'2016-11-10 13:46:57',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(314,304,1,1,1,'<p>\n	Testing that guest can&#39;t use board after sharing is removed.</p>\n<p>\n	Test configuration: Windows 7 / Firefox 35.0.1</p>\n<p>\n	Test passed: Guest user can&#39;t access the board after shared link is removed by the board owner.</p>\n','<p>\n	Log In with account: tunnus7@example.com / tunnus77</p>\n',2,1,'2016-11-10 13:46:57',NULL,'1000-01-01 00:00:00',1,1,1,3.00),(331,253,1,1,1,'<p>\n	Verifies that use is able to create multiple tickets on the board.</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n','<p>\n	Contriboard open in browser, logged with credentials with board open</p>\n',2,1,'2016-11-10 13:46:57',NULL,'1000-01-01 00:00:00',1,1,1,4.00),(339,185,1,1,1,'<p>\n	<strong style=\"font-size: 11.6999998092651px;\">Date: 18.02.2015</strong></p>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<strong>Comments:</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	Snap to grid should make it easier to arrange the tickets. This is test for it.</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<p>\n	&nbsp;</p>\n','<p>\n	Browser is open at contriboard, logged in and user has created an board</p>\n',2,1,'2016-11-10 13:46:58',NULL,'1000-01-01 00:00:00',1,1,1,4.00),(344,350,1,1,1,'<div>\n	Date: 21.02.2015</div>\n<div>\n	&nbsp;</div>\n<div>\n	Comments:</div>\n<div>\n	The user tries to move the ticket outside the boundaries of the board</div>\n','<ul>\n	<li>\n		The user has logged in to Contriboard&nbsp;</li>\n	<li>\n		The user has at least one board created.</li>\n	<li>\n		The user has opened a board for viewing.</li>\n</ul>\n',2,1,'2016-11-10 13:46:58',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(349,367,1,1,1,'<p>\n	21.05.2015</p>\n<p>\n	Test for checking the limits of ticket text.</p>\n','<p>\n	An account has been made and logged in. A board has been made.</p>\n',2,1,'2016-11-10 13:46:58',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(354,223,1,1,1,'<p>\n	21.05.2015</p>\n<p>\n	&nbsp;</p>\n','<p>\n	Signed in to Contriboard using above info. At least one board is created.</p>\n',2,1,'2016-11-10 13:46:58',NULL,'1000-01-01 00:00:00',1,1,1,5.00),(361,396,1,1,1,'<p>\n	comments:</p>\n<p>\n	verify that user can move tickets in more then one browser tab.</p>\n','<p>\n	Logged in to contriboard&#39;s first page.</p>\n',2,1,'2016-11-10 13:46:58',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(368,80,1,1,1,'<p>\n	User should be able to add special characters to ticket heading</p>\n','<p>\n	1. User is logged in</p>\n<p>\n	2. User has at least one board</p>\n<p>\n	3. User is in &quot;view board&quot; -mode, where user can add new tickets</p>\n',2,1,'2016-11-10 13:46:58',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(372,285,1,1,1,'<pre style=\"font-family: Arial, Helvetica, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n<code>Testing ticket name changes in Contriboard.\n\nTest Configuration:\nWindows 7\nFirefox</code></pre>\n','<p>\n	Account: testausta@hotmail.fi pw:testausta</p>\n',2,1,'2016-11-10 13:46:58',NULL,'1000-01-01 00:00:00',1,1,1,1.00),(378,49,1,1,1,'<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<strong style=\"font-size: 11.6999998092651px;\">Date: 18.02.2015</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<strong>Comments:</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	Make sure user can delete the tickets</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n','<p>\n	User is logged to Contriboard</p>\n<p>\n	User can create a board</p>\n<p>\n	User can create a ticket</p>\n',2,1,'2016-11-10 13:46:58',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(384,397,1,1,1,'<p>\n	21.05.2015</p>\n<p>\n	Comments:</p>\n<p>\n	&nbsp;</p>\n','<p>\n	<span style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">Logged into contriboard in multiple browsers</span></p>\n',2,1,'2016-11-10 13:46:58',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(391,79,1,1,1,'<p>\n	Comments:</p>\n<p>\n	User should be able to change the color the ticket</p>\n','<p>\n	1. User is logged in&nbsp;</p>\n<p>\n	2. User has a board and is in &quot;board view&quot; -mode, where he/she can edit tickets</p>\n<p>\n	3. User has created a ticket with the heading &quot;joo&quot; and the ticket is visible</p>\n',2,1,'2016-11-10 13:46:58',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(400,47,1,1,1,'<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<strong style=\"font-size: 11.6999998092651px;\">Date: 21.05.2015</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<strong>Comments:</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	Make sure user can edit colors of the tickets</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n','<p>\n	User can log in to Contriboard</p>\n<p>\n	User can create a board</p>\n<p>\n	User can create a ticket</p>\n',2,1,'2016-11-10 13:46:58',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(406,260,1,1,1,'<p>\n	21.05.2015</p>\n<p>\n	Verifies that product can handle the situation when user try to modify a ticket of a board which has been removed by the owner.</p>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n','<div>\n	Two users have Contriboard open in browser.</div>\n<div>\n	&nbsp;</div>\n<div>\n	Logged with credentials or register.</div>\n<div>\n	&nbsp;</div>\n<div>\n	User 1 has shared the link to the board to User 2.</div>\n<div>\n	User 1 is viewing boards menu and User 2 is viewing the board.</div>\n',2,1,'2016-11-10 13:46:58',NULL,'1000-01-01 00:00:00',1,1,1,6.00),(411,261,1,1,1,'<div>\n	22.05.2015</div>\n<div>\n	Verifies that user is not able to modify a ticket after the owner has clear the sharing link.</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n','<div>\n	Two users have Contriboard open in browser.</div>\n<div>\n	&nbsp;</div>\n<div>\n	Logged with credentials</div>\n<div>\n	&nbsp;</div>\n<div>\n	User 1 has shared the link to the board to User 2.</div>\n<div>\n	User 1 is viewing boards menu and User 2 is viewing the board.</div>\n',2,1,'2016-11-10 13:46:59',NULL,'1000-01-01 00:00:00',1,1,1,6.00),(416,24,1,1,1,'','<p>\n	Be in any board</p>\n',2,1,'2016-11-10 13:46:59',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(419,57,1,1,1,'<div>\n	Date: 22.02.2015</div>\n<div>\n	&nbsp;</div>\n<div>\n	Comments</div>\n<div>\n	Create two or more tickets and make them overlap each other.</div>\n<div>\n	&nbsp;</div>\n<div>\n	<div>\n		End State</div>\n	<div>\n		Tickets overlap each other.</div>\n</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n','<p>\n	Log in to Contriboard, create a board and create several tickets.</p>\n',2,1,'2016-11-10 13:46:59',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(422,263,1,1,1,'<p>\n	Verifies that product can handle the situation when multiple users are deleting the same ticket.</p>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<p>\n	&nbsp;</p>\n','<div>\n	Two users have Contriboard open in browser.</div>\n<div>\n	&nbsp;</div>\n<div>\n	Logged with credentials to contriboard.</div>\n<div>\n	&nbsp;</div>\n<div>\n	User 1 has shared the link to the board to User 2.</div>\n<div>\n	Both are viewing the board.</div>\n',2,1,'2016-11-10 13:46:59',NULL,'1000-01-01 00:00:00',1,1,1,4.00),(428,145,1,1,1,'<p>\n	22.05.2015</p>\n<p>\n	Test Summary:&nbsp;Move ticket position</p>\n','<p>\n	User is logged in and on the board page. User created a ticket.</p>\n',2,1,'2016-11-10 13:46:59',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(431,67,1,1,1,'<p>\n	22.05.2015</p>\n<p>\n	Verify that minimap works correctly</p>\n','<p>\n	User is logged in and created an board.</p>\n',2,1,'2016-11-10 13:46:59',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(437,61,1,1,1,'<div>\n	Date: 22.05.2015</div>\n<div>\n	&nbsp;</div>\n<div>\n	Comments</div>\n<div>\n	Attempt to delete a ticket while some other user is editing the ticket data.</div>\n<div>\n	&nbsp;</div>\n<div>\n	<div>\n		End State</div>\n	<div>\n		Ticket is deleted, edited data won&#39;t be sent to server</div>\n</div>\n<p>\n	&nbsp;</p>\n','<p>\n	User is logged in to Contriboard and open a board.</p>\n',2,1,'2016-11-10 13:46:59',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(441,82,1,1,1,'<p>\n	21.05.2015</p>\n<p>\n	Minimap ticket color should always be correct</p>\n','<p style=\"margin-top: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	1. User is logged in (seppo@mailinator.com/seppo12346)</p>\n<p style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	2. User is &quot;inside&quot; a board</p>\n<p style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	3. User has 1 red ticket visible in the board</p>\n',2,1,'2016-11-10 13:46:59',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(445,405,1,1,1,'','<p>\n	User is logged in to contriboard and has board open. User creates one ticket.</p>\n',2,1,'2016-11-10 13:46:59',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(451,241,1,1,1,'<p>\n	22.05.2015</p>\n<p>\n	Test how the board handles tickets created with empty content.</p>\n','<p>\n	User is logged in and created an board.</p>\n',2,1,'2016-11-10 13:46:59',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(455,93,1,1,1,'<p>\n	22.05.2015</p>\n<p>\n	Test to prove that tickets work after changing the background</p>\n','<p>\n	User is logged in to Contriboard and created an board.</p>\n',2,1,'2016-11-10 13:46:59',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(460,419,1,1,1,'<p>\n	Verify that markdown works as designed. Same as it would in word or anyother text editor.</p>\n','<p>\n	User is logged in, created an board and ticket is open.</p>\n',2,1,'2016-11-10 13:46:59',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(462,420,1,1,1,'<p>\n	Verify that markdown works as designed. Same as it would in word or any other text editor.</p>\n<p>\n	Markdown methods:</p>\n<p>\n	# Heading1</p>\n<p>\n	## Heading2</p>\n<p>\n	### Heading3</p>\n<p>\n	<span style=\"font-family: monospace, monospace; font-size: 14px; background-color: rgb(249, 249, 249);\">[link](http://example.com)</span></p>\n<p>\n	<font face=\"monospace, monospace\"><span style=\"font-size: 14px; background-color: rgb(249, 249, 249);\">lists: * and 1</span></font></p>\n<p>\n	<font face=\"monospace, monospace\"><span style=\"font-size: 14px; background-color: rgb(249, 249, 249);\">---example--- for&nbsp;</span></font><span style=\"color: rgb(221, 187, 0); font-family: monospace, monospace; font-size: 14px; background-color: rgb(249, 249, 249);\">&amp;mdash</span></p>\n<p>\n	&nbsp;</p>\n','<p>\n	User is logged in, created an board and ticket is open.</p>\n',2,1,'2016-11-10 13:46:59',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(465,426,1,1,1,'<p>\n	Test to verify that ticket reviewing is working as it should.</p>\n','<p>\n	Account created, logged in and board created.</p>\n',2,1,'2016-11-10 13:46:59',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(478,20,1,1,1,'<p style=\"margin-top: 0px;\">\n	<strong style=\"font-size: 11.6999998092651px;\">Date: 20.05.2015</strong></p>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<strong>Comments:</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	Test that user can export JSON-file</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n','<p>\n	<span style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">Your browser is open, you have logged in, created a board and you have opened the board.</span></p>\n',2,1,'2016-11-10 13:46:59',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(483,18,1,1,1,'<p>\n	Verify that you can export .CSV-file</p>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<strong style=\"font-size: 11.6999998092651px;\">Date: 20.05.2015</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	<strong>Comments:</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	Make sure that board sharing works, even with &quot;old&quot; URL.</div>\n<div style=\"margin: 0px; padding: 0px; font-size: 11.6999998092651px;\">\n	&nbsp;</div>\n<p>\n	&nbsp;</p>\n','<p>\n	<span style=\"font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">Your browser is open, you have logged in, created a board and you have opened the board.</span></p>\n',2,1,'2016-11-10 13:46:59',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(488,407,1,1,1,'<p>\n	22.05.2015</p>\n<p>\n	Verify that export dialog shows up when user presses the &quot;export&quot;-button</p>\n','<p>\n	User is logged in and in board view.</p>\n',2,1,'2016-11-10 13:46:59',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(491,423,1,1,1,'<p>\n	Verify that image export works in contriboard.</p>\n','<p>\n	User is logged in , created board and created few tickets on the board.</p>\n',2,1,'2016-11-10 13:47:00',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(496,429,1,1,1,'<p>\n	This test verify&#39;s ticket cloning works.</p>\n','<p>\n	User is logged, created board and on the board view.</p>\n',2,1,'2016-11-10 13:47:00',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(503,421,1,1,1,'<p>\n	Verify that user can add a comment to a ticket.</p>\n','<p>\n	User is logged in, opened an board, created an ticket and opened it.</p>\n',2,1,'2016-11-10 13:47:00',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(508,247,1,1,1,'<p>\n	Verify that guests can view changes made to a board live.</p>\n<p>\n	<strong>Test Configuration:</strong></p>\n<p>\n	Windows 7</p>\n<p>\n	Chrome 40.*</p>\n','<p>\n	Contriboard open in browser, logged with credentials testaaja@grr.la/salasana, with the board &quot;Testilauta&quot; open.</p>\n<p>\n	Same board open in another browser, opened via guest access.</p>\n',2,1,'2016-11-10 13:47:00',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(514,40,1,1,1,'<div>\n	Date: 18.02.2015</div>\n<div>\n	&nbsp;</div>\n<div>\n	Comments:</div>\n<div>\n	Make sure guest can edit colors of the tickets</div>\n<div>\n	&nbsp;</div>\n<div>\n	End State:</div>\n<div>\n	A ticket color has been edited</div>\n<div>\n	&nbsp;</div>\n','<p>\n	User created an board as logged user in normal tab and user logged in as quest with incognito tab to shared link in the same board.</p>\n',2,1,'2016-11-10 13:47:00',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(519,41,1,1,1,'<div>\n	Date: 22.05.2015</div>\n<div>\n	&nbsp;</div>\n<div>\n	Comments:</div>\n<div>\n	Make sure guest can edit names of the tickets</div>\n<div>\n	&nbsp;</div>\n<div>\n	End State:</div>\n<div>\n	A ticket name has been edited</div>\n<div>\n	&nbsp;</div>\n','<p>\n	User created an board as logged user in normal tab and user logged in as quest with incognito tab to shared link in the same board.</p>\n',2,1,'2016-11-10 13:47:00',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(524,42,1,1,1,'<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<strong>Date: 22.05.2015</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<strong>Comments:</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	Make sure guest can delete a ticket</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	<strong>End State:</strong></div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	The board should be forever ticket</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n<div style=\"margin: 0px; padding: 0px; font-family: \'Trebuchet MS\', Arial, Verdana, sans-serif; font-size: 11.6999998092651px; background-color: rgb(238, 238, 238);\">\n	&nbsp;</div>\n','<p>\n	User created an board as logged user in normal tab and user logged in as quest with incognito tab to shared link in the same board.</p>\n',2,1,'2016-11-10 13:47:00',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(529,43,1,1,1,'<div>\n	Date: 18.02.2015</div>\n<div>\n	&nbsp;</div>\n<div>\n	Comments:</div>\n<div>\n	Make sure guest can access a board</div>\n<div>\n	&nbsp;</div>\n<div>\n	End State:</div>\n<div>\n	Guest is in the board</div>\n<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n','<p>\n	A board has been created</p>\n<p>\n	Guest has the shared-link to the board</p>\n',2,1,'2016-11-10 13:47:00',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(534,415,1,1,1,'','',2,1,'2016-11-10 13:47:00',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(536,416,1,1,1,'','',2,1,'2016-11-10 13:47:00',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(538,417,1,1,1,'','',2,1,'2016-11-10 13:47:00',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(542,138,1,1,1,'<p>\n	<strong style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Module Name:</strong><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;_</span></p>\n<p>\n	<strong style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Dependencies:</strong><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;None</span><br style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\" />\n	<strong style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Requirements:</strong><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;Selenium, TestNG, BoardLoadTest10000.java (and&nbsp;</span><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Apache Maven for auto.reg.)</span><br style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\" />\n	<strong style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Test Data:</strong><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;Existing credentials: testuser001@mailinator.com:salasana1</span></p>\n<p>\n	<strong style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Test Summary:</strong><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;Load test for creating 10 000 Boards to Contriboard.</span></p>\n<p>\n	&nbsp;</p>\n','<p>\n	<strong style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; box-sizing: border-box;\">Pre-condition:</strong><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;</span></p>\n<p>\n	<strong style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; box-sizing: border-box;\">Post-condition:</strong><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;</span></p>\n<p>\n	<strong style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; box-sizing: border-box;\">Test Execution:</strong><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;Run with Maven</span></p>\n<p>\n	<span style=\"color: rgb(69, 83, 95); font-family: monospace, monospace; font-size: 16px; letter-spacing: 0.0799999982118607px; line-height: 24px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">mvn clean test -Dwebdriver.base.url=http://contriboard.n4sjamk.org/</span></p>\n<p>\n	<strong style=\"font-size: 16px; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; line-height: 25.6000003814697px; box-sizing: border-box;\">Pass / Fail:</strong><span style=\"font-size: 16px; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;Monitor the results from TestNG Result Web Page</span></p>\n<p style=\"font-size: 16px; box-sizing: border-box; margin-top: 0px; margin-bottom: 16px; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">\n	<strong style=\"box-sizing: border-box;\">Expected Result:</strong>&nbsp;Site responce times are low and there are no timeouts</p>\n',3,1,'2016-11-10 13:47:00',NULL,'1000-01-01 00:00:00',1,1,2,10.00),(544,139,1,1,1,'<p>\n	<strong style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Module Name:</strong><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;_</span></p>\n<p>\n	<strong style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Dependencies:</strong><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;None</span><br style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\" />\n	<strong style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Requirements:</strong><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;Selenium, TestNG, LoadTest200.java (and&nbsp;</span><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Apache Maven for auto.reg.)</span><br style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\" />\n	<strong style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Test Data:</strong><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;Existing credentials: &lt;testuser&lt;NNN&gt;@mailinator.com&gt; [001-200]</span></p>\n<p>\n	<strong style=\"box-sizing: border-box; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">Test Summary:</strong><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;Simulate load of 200 users on the site.</span></p>\n<p>\n	&nbsp;</p>\n','<p>\n	<strong style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; box-sizing: border-box;\">Pre-condition:</strong><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;</span></p>\n<p>\n	<strong style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; box-sizing: border-box;\">Post-condition:</strong><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;</span></p>\n<p>\n	<strong style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; box-sizing: border-box;\">Test Execution:</strong><span style=\"color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; font-size: 16px; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;Run with Maven</span></p>\n<p>\n	<span style=\"color: rgb(69, 83, 95); font-family: monospace, monospace; font-size: 16px; letter-spacing: 0.0799999982118607px; line-height: 24px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">mvn clean test -Dwebdriver.base.url=http://contriboard.n4sjamk.org/</span></p>\n<p>\n	<strong style=\"font-size: 16px; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; line-height: 25.6000003814697px; box-sizing: border-box;\">Pass / Fail:</strong><span style=\"font-size: 16px; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">&nbsp;Monitor the results from TestNG Result Web Page</span></p>\n<p style=\"font-size: 16px; box-sizing: border-box; margin-top: 0px; margin-bottom: 16px; color: rgb(51, 51, 51); font-family: \'Helvetica Neue\', Helvetica, \'Segoe UI\', Arial, freesans, sans-serif; line-height: 25.6000003814697px; background-color: rgb(255, 255, 255);\">\n	<strong style=\"box-sizing: border-box;\">Expected Result:</strong>&nbsp;Site responce times are low and there are no timeouts</p>\n',3,1,'2016-11-10 13:47:00',NULL,'1000-01-01 00:00:00',1,1,2,10.00),(548,434,1,1,1,'','',2,1,'2016-11-10 13:47:00',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(551,430,1,1,1,'','',2,1,'2016-11-10 13:47:00',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(554,431,1,1,1,'<p>\n	https://github.com/N4SJAMK/locust-contriboard</p>\n','',2,1,'2016-11-10 13:47:00',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(557,433,1,1,1,'<p>\n	https://github.com/N4SJAMK/contriboard-noise-tester</p>\n<p>\n	&nbsp;</p>\n<p>\n	<img alt=\"\" src=\"https://camo.githubusercontent.com/1620d0e3d70d71d0f4c52dca3eae465a4b1a609a/68747470733a2f2f7777772e6c7563696463686172742e636f6d2f7075626c69635365676d656e74732f766965772f35356164656161332d306531342d346263322d393763622d3338623230613030386237302f696d6167652e706e67\" style=\"width: 512px; height: 725px;\" /></p>\n','',2,1,'2016-11-10 13:47:00',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(565,436,1,1,1,'','',2,1,'2016-11-10 13:47:00',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(568,333,1,1,1,'<p>\n	testing resolution scalability</p>\n','<div>\n	-multiple screens with different resolutions or one highres monitor (uhd or above)</div>\n<div>\n	-safari web browser</div>\n<div>\n	&nbsp;</div>\n',2,1,'2016-11-10 13:47:00',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(582,336,1,1,1,'<p>\n	testing resolution scalability</p>\n','<div>\n	-multiple screens with different resolutions or one highres monitor (uhd or above)</div>\n<div>\n	-firefox web browser</div>\n',2,1,'2016-11-10 13:47:01',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(596,338,1,1,1,'<p>\n	testing resolution scalability</p>\n','<div>\n	-multiple screens with different resolutions or one highres monitor (uhd or above)</div>\n<div>\n	-opera web browser</div>\n',2,1,'2016-11-10 13:47:01',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(610,341,1,1,1,'<p>\n	testing scalability</p>\n','<div>\n	-multiple screens with different resolutions or one highres monitor (uhd or above)</div>\n<div>\n	-internet explorer web browser</div>\n',2,1,'2016-11-10 13:47:01',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(624,343,1,1,1,'<p>\n	testing that components scale right.</p>\n','<div>\n	-multiple screens with different resolutions or one highres monitor (uhd or above)</div>\n<div>\n	-google chrome web browser</div>\n',2,1,'2016-11-10 13:47:01',NULL,'1000-01-01 00:00:00',1,1,1,NULL),(638,92,1,1,1,'<p>\n	If system stays up -&gt; PASS</p>\n<p>\n	If system dies -&gt; FAIL</p>\n','',2,1,'2016-11-10 13:47:01',NULL,'1000-01-01 00:00:00',1,1,1,NULL);
/*!40000 ALTER TABLE `tcversions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testcase_keywords`
--

DROP TABLE IF EXISTS `testcase_keywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testcase_keywords` (
  `testcase_id` int(10) unsigned NOT NULL DEFAULT '0',
  `keyword_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`testcase_id`,`keyword_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testcase_keywords`
--

LOCK TABLES `testcase_keywords` WRITE;
/*!40000 ALTER TABLE `testcase_keywords` DISABLE KEYS */;
/*!40000 ALTER TABLE `testcase_keywords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testcase_relations`
--

DROP TABLE IF EXISTS `testcase_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testcase_relations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source_id` int(10) unsigned NOT NULL,
  `destination_id` int(10) unsigned NOT NULL,
  `relation_type` smallint(5) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT NULL,
  `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testcase_relations`
--

LOCK TABLES `testcase_relations` WRITE;
/*!40000 ALTER TABLE `testcase_relations` DISABLE KEYS */;
/*!40000 ALTER TABLE `testcase_relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testplan_platforms`
--

DROP TABLE IF EXISTS `testplan_platforms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testplan_platforms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `testplan_id` int(10) unsigned NOT NULL,
  `platform_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_testplan_platforms` (`testplan_id`,`platform_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Connects a testplan with platforms';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testplan_platforms`
--

LOCK TABLES `testplan_platforms` WRITE;
/*!40000 ALTER TABLE `testplan_platforms` DISABLE KEYS */;
/*!40000 ALTER TABLE `testplan_platforms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testplan_tcversions`
--

DROP TABLE IF EXISTS `testplan_tcversions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testplan_tcversions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `testplan_id` int(10) unsigned NOT NULL DEFAULT '0',
  `tcversion_id` int(10) unsigned NOT NULL DEFAULT '0',
  `node_order` int(10) unsigned NOT NULL DEFAULT '1',
  `urgency` smallint(5) NOT NULL DEFAULT '2',
  `platform_id` int(10) unsigned NOT NULL DEFAULT '0',
  `author_id` int(10) unsigned DEFAULT NULL,
  `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `testplan_tcversions_tplan_tcversion` (`testplan_id`,`tcversion_id`,`platform_id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testplan_tcversions`
--

LOCK TABLES `testplan_tcversions` WRITE;
/*!40000 ALTER TABLE `testplan_tcversions` DISABLE KEYS */;
INSERT INTO `testplan_tcversions` VALUES (1,640,9,1000,2,0,1,'2016-11-10 14:21:58'),(2,640,11,1010,2,0,1,'2016-11-10 14:21:58'),(3,640,13,1020,2,0,1,'2016-11-10 14:21:58'),(4,640,15,1030,2,0,1,'2016-11-10 14:21:58'),(5,640,17,1040,2,0,1,'2016-11-10 14:21:58'),(6,641,22,0,2,0,1,'2016-11-10 14:22:21'),(7,641,31,10,2,0,1,'2016-11-10 14:22:21'),(8,641,38,20,2,0,1,'2016-11-10 14:22:21'),(9,641,43,30,2,0,1,'2016-11-10 14:22:21'),(10,641,49,40,2,0,1,'2016-11-10 14:22:21'),(11,641,54,50,2,0,1,'2016-11-10 14:22:21'),(12,641,59,60,2,0,1,'2016-11-10 14:22:21'),(13,641,65,70,2,0,1,'2016-11-10 14:22:21'),(14,641,70,80,2,0,1,'2016-11-10 14:22:21'),(15,641,76,90,2,0,1,'2016-11-10 14:22:21'),(16,641,80,100,2,0,1,'2016-11-10 14:22:21'),(17,641,84,110,2,0,1,'2016-11-10 14:22:21'),(18,641,88,120,2,0,1,'2016-11-10 14:22:21'),(19,641,92,130,2,0,1,'2016-11-10 14:22:21'),(20,641,95,140,2,0,1,'2016-11-10 14:22:21'),(21,641,100,150,2,0,1,'2016-11-10 14:22:21'),(22,641,104,160,2,0,1,'2016-11-10 14:22:21'),(23,641,111,10,2,0,1,'2016-11-10 14:22:21'),(24,641,117,20,2,0,1,'2016-11-10 14:22:21'),(25,641,123,30,2,0,1,'2016-11-10 14:22:21'),(26,641,129,40,2,0,1,'2016-11-10 14:22:21'),(27,641,133,50,2,0,1,'2016-11-10 14:22:21'),(28,641,140,0,2,0,1,'2016-11-10 14:22:21'),(29,641,145,10,2,0,1,'2016-11-10 14:22:21'),(30,641,155,20,2,0,1,'2016-11-10 14:22:21'),(31,641,160,30,2,0,1,'2016-11-10 14:22:21'),(32,641,165,40,2,0,1,'2016-11-10 14:22:21'),(33,641,169,50,2,0,1,'2016-11-10 14:22:21'),(34,641,178,60,2,0,1,'2016-11-10 14:22:21'),(35,641,184,70,2,0,1,'2016-11-10 14:22:21'),(36,641,189,80,2,0,1,'2016-11-10 14:22:21'),(37,641,193,90,2,0,1,'2016-11-10 14:22:21'),(38,641,203,100,2,0,1,'2016-11-10 14:22:21'),(39,641,208,110,2,0,1,'2016-11-10 14:22:21'),(40,641,213,120,2,0,1,'2016-11-10 14:22:21'),(41,641,217,130,2,0,1,'2016-11-10 14:22:21'),(42,641,235,140,2,0,1,'2016-11-10 14:22:21'),(43,641,240,150,2,0,1,'2016-11-10 14:22:21'),(44,641,252,0,2,0,1,'2016-11-10 14:22:21'),(45,641,258,0,2,0,1,'2016-11-10 14:22:21'),(46,641,263,60,2,0,1,'2016-11-10 14:22:21'),(47,641,268,1010,2,0,1,'2016-11-10 14:22:21'),(48,641,276,0,2,0,1,'2016-11-10 14:22:21'),(49,641,286,20,2,0,1,'2016-11-10 14:22:21'),(50,641,299,50,2,0,1,'2016-11-10 14:22:21'),(51,641,304,90,2,0,1,'2016-11-10 14:22:21'),(52,641,310,110,2,0,1,'2016-11-10 14:22:21'),(53,641,314,120,2,0,1,'2016-11-10 14:22:21'),(54,641,331,20,2,0,1,'2016-11-10 14:22:21'),(55,641,339,30,2,0,1,'2016-11-10 14:22:21'),(56,641,344,40,2,0,1,'2016-11-10 14:22:21'),(57,641,349,50,2,0,1,'2016-11-10 14:22:21'),(58,641,354,60,2,0,1,'2016-11-10 14:22:21'),(59,641,361,80,2,0,1,'2016-11-10 14:22:21'),(60,641,368,90,2,0,1,'2016-11-10 14:22:21'),(61,641,372,100,2,0,1,'2016-11-10 14:22:21'),(62,641,378,110,2,0,1,'2016-11-10 14:22:21'),(63,641,384,130,2,0,1,'2016-11-10 14:22:21'),(64,641,391,140,2,0,1,'2016-11-10 14:22:21'),(65,641,400,150,2,0,1,'2016-11-10 14:22:21'),(66,641,406,160,2,0,1,'2016-11-10 14:22:21'),(67,641,411,170,2,0,1,'2016-11-10 14:22:21'),(68,641,416,190,2,0,1,'2016-11-10 14:22:21'),(69,641,419,200,2,0,1,'2016-11-10 14:22:21'),(70,641,422,210,2,0,1,'2016-11-10 14:22:21'),(71,641,428,220,2,0,1,'2016-11-10 14:22:21'),(72,641,431,230,2,0,1,'2016-11-10 14:22:21'),(73,641,437,250,2,0,1,'2016-11-10 14:22:21'),(74,641,441,260,2,0,1,'2016-11-10 14:22:21'),(75,641,445,280,2,0,1,'2016-11-10 14:22:21'),(76,641,451,300,2,0,1,'2016-11-10 14:22:21'),(77,641,455,310,2,0,1,'2016-11-10 14:22:21'),(78,641,460,320,2,0,1,'2016-11-10 14:22:21'),(79,641,462,330,2,0,1,'2016-11-10 14:22:21'),(80,641,465,340,2,0,1,'2016-11-10 14:22:21'),(81,641,478,0,2,0,1,'2016-11-10 14:22:21'),(82,641,483,10,2,0,1,'2016-11-10 14:22:21'),(83,641,488,20,2,0,1,'2016-11-10 14:22:21'),(84,641,491,30,2,0,1,'2016-11-10 14:22:21'),(85,641,496,40,2,0,1,'2016-11-10 14:22:21'),(86,641,503,0,2,0,1,'2016-11-10 14:22:21'),(87,641,508,0,2,0,1,'2016-11-10 14:22:21'),(88,641,514,10,2,0,1,'2016-11-10 14:22:21'),(89,641,519,20,2,0,1,'2016-11-10 14:22:21'),(90,641,524,30,2,0,1,'2016-11-10 14:22:21'),(91,641,529,40,2,0,1,'2016-11-10 14:22:21'),(92,641,534,0,2,0,1,'2016-11-10 14:22:21'),(93,641,536,10,2,0,1,'2016-11-10 14:22:21'),(94,641,538,20,2,0,1,'2016-11-10 14:22:21');
/*!40000 ALTER TABLE `testplan_tcversions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testplans`
--

DROP TABLE IF EXISTS `testplans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testplans` (
  `id` int(10) unsigned NOT NULL,
  `testproject_id` int(10) unsigned NOT NULL DEFAULT '0',
  `notes` text,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `is_open` tinyint(1) NOT NULL DEFAULT '1',
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  `api_key` varchar(64) NOT NULL DEFAULT '829a2ded3ed0829a2dedd8ab81dfa2c77e8235bc3ed0d8ab81dfa2c77e8235bc',
  PRIMARY KEY (`id`),
  UNIQUE KEY `testplans_api_key` (`api_key`),
  KEY `testplans_testproject_id_active` (`testproject_id`,`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testplans`
--

LOCK TABLES `testplans` WRITE;
/*!40000 ALTER TABLE `testplans` DISABLE KEYS */;
INSERT INTO `testplans` VALUES (640,2,'',1,1,1,'004ff446749c9029dce52a2c48272d985e5b56bf79ef8d6a8c809078fdf84859'),(641,2,'',1,1,1,'070b6a9a8aa9559d54f8f4fe889ddea5cf9f11b1fee39c1cc82eb635773f5a07');
/*!40000 ALTER TABLE `testplans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testproject_issuetracker`
--

DROP TABLE IF EXISTS `testproject_issuetracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testproject_issuetracker` (
  `testproject_id` int(10) unsigned NOT NULL,
  `issuetracker_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`testproject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testproject_issuetracker`
--

LOCK TABLES `testproject_issuetracker` WRITE;
/*!40000 ALTER TABLE `testproject_issuetracker` DISABLE KEYS */;
/*!40000 ALTER TABLE `testproject_issuetracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testproject_reqmgrsystem`
--

DROP TABLE IF EXISTS `testproject_reqmgrsystem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testproject_reqmgrsystem` (
  `testproject_id` int(10) unsigned NOT NULL,
  `reqmgrsystem_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`testproject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testproject_reqmgrsystem`
--

LOCK TABLES `testproject_reqmgrsystem` WRITE;
/*!40000 ALTER TABLE `testproject_reqmgrsystem` DISABLE KEYS */;
/*!40000 ALTER TABLE `testproject_reqmgrsystem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testprojects`
--

DROP TABLE IF EXISTS `testprojects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testprojects` (
  `id` int(10) unsigned NOT NULL,
  `notes` text,
  `color` varchar(12) NOT NULL DEFAULT '#9BD',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `option_reqs` tinyint(1) NOT NULL DEFAULT '0',
  `option_priority` tinyint(1) NOT NULL DEFAULT '0',
  `option_automation` tinyint(1) NOT NULL DEFAULT '0',
  `options` text,
  `prefix` varchar(16) NOT NULL,
  `tc_counter` int(10) unsigned NOT NULL DEFAULT '0',
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  `issue_tracker_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `reqmgr_integration_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `api_key` varchar(64) NOT NULL DEFAULT '0d8ab81dfa2c77e8235bc829a2ded3edfa2c78235bc829a27eded3ed0d8ab81d',
  PRIMARY KEY (`id`),
  UNIQUE KEY `testprojects_prefix` (`prefix`),
  UNIQUE KEY `testprojects_api_key` (`api_key`),
  KEY `testprojects_id_active` (`id`,`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testprojects`
--

LOCK TABLES `testprojects` WRITE;
/*!40000 ALTER TABLE `testprojects` DISABLE KEYS */;
INSERT INTO `testprojects` VALUES (2,'','',1,0,0,0,'O:8:\"stdClass\":4:{s:19:\"requirementsEnabled\";i:0;s:19:\"testPriorityEnabled\";i:1;s:17:\"automationEnabled\";i:1;s:16:\"inventoryEnabled\";i:0;}','TC',436,1,0,0,'67fa3c8a453892481f4adf07b2b3b0a75a99094b3da1583844fa28db9baf869c');
/*!40000 ALTER TABLE `testprojects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testsuites`
--

DROP TABLE IF EXISTS `testsuites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testsuites` (
  `id` int(10) unsigned NOT NULL,
  `details` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testsuites`
--

LOCK TABLES `testsuites` WRITE;
/*!40000 ALTER TABLE `testsuites` DISABLE KEYS */;
INSERT INTO `testsuites` VALUES (3,'<h2>\n	About Master Test Plan&nbsp;</h2>\n<p>\n	&nbsp;</p>\n<p>\n	You will find current &nbsp;master test plan from&nbsp;<a href=\"https://github.com/N4SJAMK/teamboard-meta/wiki/master-test-plan\">GitHub Wiki</a></p>\n<p>\n	&nbsp;</p>\n<p style=\"color: rgb(51, 51, 51); font-family: \'Open Sans\', sans-serif; font-size: 15px; line-height: 24px; margin: 0px 0px 25px; padding: 0px; text-align: justify; background-color: rgb(255, 255, 255);\">\n	Test plan is the project plan for the<a href=\"http://istqbexamcertification.com/what-is-fundamental-test-process-in-software-testing/\" style=\"color: rgb(237, 112, 43); text-decoration: none;\" title=\"What is fundamental test process in software testing?\">testing work</a>&nbsp;to be done. It is not a&nbsp;<strong><a href=\"http://istqbexamcertification.com/what-is-test-design-technique/\" style=\"color: rgb(237, 112, 43); text-decoration: none;\" title=\"What is test design technique?\">test design</a>&nbsp;specification,&nbsp;</strong>a collection of&nbsp;<strong>test</strong><strong>cases&nbsp;</strong>or a set of&nbsp;<strong>test procedures;&nbsp;</strong>in fact, most of our test plans do not address that level of detail. Many people have different definitions for test plans.</p>\n<p style=\"color: rgb(51, 51, 51); font-family: \'Open Sans\', sans-serif; font-size: 15px; line-height: 24px; margin: 0px 0px 25px; padding: 0px; text-align: justify; background-color: rgb(255, 255, 255);\">\n	Why it is required to write test plans? We have three main reasons to write the test plans:</p>\n<p style=\"color: rgb(51, 51, 51); font-family: \'Open Sans\', sans-serif; font-size: 15px; line-height: 24px; margin: 0px 0px 25px; padding: 0px; text-align: justify; background-color: rgb(255, 255, 255);\">\n	<strong>First,</strong>&nbsp;by writing a test plan it guides our thinking.&nbsp;<strong>&nbsp;</strong>Writing a test plan forces us to confront the challenges that await us and focus our thinking on important topics.<br />\n	Fred Brooks explains the importance of careful estimation and planning for testing in one of his book as follows:</p>\n<p style=\"color: rgb(51, 51, 51); font-family: \'Open Sans\', sans-serif; font-size: 15px; line-height: 24px; margin: 0px 0px 25px; padding: 0px; text-align: justify; background-color: rgb(255, 255, 255);\">\n	<em>Failure to allow enough time for system test, in particular, is peculiarly disastrous. Since the delay comes at the end of the schedule, no one is aware of schedule trouble until almost the delivery date [and] delay at this point has unusually severe &hellip; financial repercussions. The project is fully staffed, and cost-per day is maximum [as are the associated opportunity costs]. It is therefore very important to allow enough system test time in the original schedule.</em><br />\n	<em>[Brooks, 1995]</em></p>\n<p style=\"color: rgb(51, 51, 51); font-family: \'Open Sans\', sans-serif; font-size: 15px; line-height: 24px; margin: 0px 0px 25px; padding: 0px; text-align: justify; background-color: rgb(255, 255, 255);\">\n	By using a template for writing test plans helps us remember the important challenges. You can use the IEEE 829 test plan template shown in this chapter, use someone else&rsquo;s template, or create your own template over time.</p>\n<p style=\"color: rgb(51, 51, 51); font-family: \'Open Sans\', sans-serif; font-size: 15px; line-height: 24px; margin: 0px 0px 25px; padding: 0px; text-align: justify; background-color: rgb(255, 255, 255);\">\n	<strong>Second,&nbsp;</strong>the test planning process and the plan itself serve as the means of communication with other members of the project team,&nbsp;<a href=\"http://istqbexamcertification.com/what-are-the-roles-and-responsibilities-of-a-tester/\" style=\"color: rgb(237, 112, 43); text-decoration: none;\" title=\"What are the roles and responsibilities of a Tester?\">testers</a>, peers, managers and other stakeholders. This communication allows the test plan to influence the project team and the project team to influence the test plan, especially in the areas of organization-wide testing policies and motivations; test scope, objectives and critical areas to test; project and product risks, resource considerations and constraints; and the testability of the item under test. We can complete this communication by circulating one or two test plan drafts and through review meetings. Such a draft will include many notes such as the following:</p>\n<p style=\"color: rgb(51, 51, 51); font-family: \'Open Sans\', sans-serif; font-size: 15px; line-height: 24px; margin: 0px 0px 25px; padding: 0px; text-align: justify; background-color: rgb(255, 255, 255);\">\n	[To Be Determined: Jennifer: Please tell me what the plan is for releasing the test items into the test lab for each cycle of system test execution?]</p>\n<p style=\"color: rgb(51, 51, 51); font-family: \'Open Sans\', sans-serif; font-size: 15px; line-height: 24px; margin: 0px 0px 25px; padding: 0px; text-align: justify; background-color: rgb(255, 255, 255);\">\n	[Dave &ndash; please let me know which version of the test tool will be used for the regression tests of the previous increments.]</p>\n<p style=\"color: rgb(51, 51, 51); font-family: \'Open Sans\', sans-serif; font-size: 15px; line-height: 24px; margin: 0px 0px 25px; padding: 0px; text-align: justify; background-color: rgb(255, 255, 255);\">\n	As we keep note or document the answers to these kinds of questions, the test plan becomes a record of previous discussions and agreements between the testers and the rest of the project team.</p>\n<p style=\"color: rgb(51, 51, 51); font-family: \'Open Sans\', sans-serif; font-size: 15px; line-height: 24px; margin: 0px 0px 25px; padding: 0px; text-align: justify; background-color: rgb(255, 255, 255);\">\n	<strong>Third,&nbsp;</strong>the test plan helps us to manage change. During early phases of the project, as we gather more information, we revise our plans. As the project evolves and situations change, we adapt our plans. By updating the plan at major milestone helps us to keep testing aligned with project needs. As we run the tests, we make final adjustments to our plans based on the results. You might not have the time &ndash; or the energy &ndash; to update your test plans every time a change is made in the project, as some projects can be quite dynamic.</p>\n<p style=\"color: rgb(51, 51, 51); font-family: \'Open Sans\', sans-serif; font-size: 15px; line-height: 24px; margin: 0px 0px 25px; padding: 0px; text-align: justify; background-color: rgb(255, 255, 255);\">\n	At times it is better to write multiple test plans in some situations. For example, when we manage both integration and system&nbsp;<strong><a href=\"http://istqbexamcertification.com/what-are-software-testing-levels/\" style=\"color: rgb(237, 112, 43); text-decoration: none;\" title=\"What are Software Testing Levels?\">test levels</a>,&nbsp;</strong>those two test execution periods occur at different points in time and have different objectives. For some systems projects, a hardware test plan and a software test plan will address different techniques and tools as well as different audiences. However, there are chances that these test plans can get overlapped, hence, a master test plan should be made that addresses the common elements of both the test plans can reduce the amount of redundant documentation.</p>\n<p style=\"color: rgb(51, 51, 51); font-family: \'Open Sans\', sans-serif; font-size: 15px; line-height: 24px; margin: 0px 0px 25px; padding: 0px 0px 0px 30px; background-color: rgb(255, 255, 255);\">\n	<strong>IEEE 829 STANDARD TEST PLAN TEMPLATE</strong><br />\n	Test plan identifier&nbsp;<br />\n	Test deliverables<br />\n	Introduction&nbsp;<br />\n	Test tasks<br />\n	Test items&nbsp;<br />\n	Environmental needs<br />\n	Features to be tested&nbsp;<br />\n	Responsibilities<br />\n	Features not to be tested&nbsp;<br />\n	Staffing and training needs<br />\n	Approach Schedule<br />\n	Item pass/fail criteria<br />\n	Risks and contingencies<br />\n	Suspension and resumption criteria Approvals</p>\n<p>\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n'),(6,''),(7,''),(18,'<p>\n	You will find some information form&nbsp;<a href=\"https://github.com/N4SJAMK/teamboard-meta/wiki\">https://github.com/N4SJAMK/teamboard-meta/wiki</a></p>\n'),(19,''),(20,''),(109,''),(138,''),(250,''),(256,''),(266,''),(273,''),(274,''),(329,''),(476,''),(501,''),(506,''),(532,''),(539,''),(540,''),(545,''),(546,''),(549,''),(552,''),(555,''),(558,''),(559,''),(560,''),(561,''),(562,'<p>\n	Unit testing is done in the code level.</p>\n<p>\n	&nbsp;</p>\n<p>\n	Look for example here: ...</p>\n'),(563,'<h1 class=\"firstHeading\" id=\"firstHeading\" lang=\"en\" style=\"font-weight: normal; margin: 0px 0px 0.25em; overflow: visible; padding: 0px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(170, 170, 170); font-size: 1.8em; line-height: 1.3; font-family: \'Linux Libertine\', Georgia, Times, serif; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">\n	Exploratory testing</h1>\n<div class=\"mw-body-content\" id=\"bodyContent\" style=\"position: relative; line-height: 1.6; font-size: 0.875em; z-index: 0; color: rgb(37, 37, 37); font-family: sans-serif;\">\n	<div id=\"siteSub\" style=\"font-size: 12.8800001144409px;\">\n		From Wikipedia, the free encyclopedia</div>\n	<div id=\"contentSub\" style=\"font-size: 11.7600002288818px; line-height: 1.2em; margin: 0px 0px 1.4em 1em; color: rgb(84, 84, 84); width: auto;\">\n		&nbsp;</div>\n	<div class=\"mw-jump\" id=\"jump-to-nav\" style=\"overflow: hidden; height: 0px; zoom: 1; -webkit-user-select: none; margin-top: -1.4em; margin-bottom: 1.4em;\">\n		&nbsp;</div>\n	<div class=\"mw-content-ltr\" dir=\"ltr\" id=\"mw-content-text\" lang=\"en\" style=\"direction: ltr;\">\n		<p style=\"margin: 0.5em 0px; line-height: inherit;\">\n			<b>Exploratory testing</b>&nbsp;is an approach to&nbsp;<a href=\"https://en.wikipedia.org/wiki/Software_testing\" style=\"text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\" title=\"Software testing\">software testing</a>&nbsp;that is concisely described as simultaneous learning,&nbsp;<a href=\"https://en.wikipedia.org/wiki/Test_design\" style=\"text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\" title=\"Test design\">test design</a>&nbsp;and test execution.&nbsp;<a href=\"https://en.wikipedia.org/wiki/Cem_Kaner\" style=\"text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\" title=\"Cem Kaner\">Cem Kaner</a>, who coined the term in 1993,<sup class=\"reference\" id=\"cite_ref-Kaner7-11_1-0\" style=\"line-height: 1; font-size: 11.1999998092651px; unicode-bidi: -webkit-isolate;\"><a href=\"https://en.wikipedia.org/wiki/Exploratory_testing#cite_note-Kaner7-11-1\" style=\"text-decoration: none; color: rgb(11, 0, 128); white-space: nowrap; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">[1]</a></sup>&nbsp;now defines exploratory testing as &quot;a style of software testing that emphasizes the personal freedom and responsibility of the individual tester to continually optimize the quality of his/her work by treating test-related learning, test design, test execution, and test result interpretation as mutually supportive activities that run in parallel throughout the project.&quot;<sup class=\"reference\" id=\"cite_ref-2\" style=\"line-height: 1; font-size: 11.1999998092651px; unicode-bidi: -webkit-isolate;\"><a href=\"https://en.wikipedia.org/wiki/Exploratory_testing#cite_note-2\" style=\"text-decoration: none; color: rgb(11, 0, 128); white-space: nowrap; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">[2]</a></sup></p>\n		<p style=\"margin: 0.5em 0px; line-height: inherit;\">\n			While the software is being tested, the tester learns things that together with experience and&nbsp;<a href=\"https://en.wikipedia.org/wiki/Creativity\" style=\"text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\" title=\"Creativity\">creativity</a>&nbsp;generates new good tests to run. Exploratory testing is often thought of as a&nbsp;<a class=\"mw-redirect\" href=\"https://en.wikipedia.org/wiki/Black_box_testing\" style=\"text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\" title=\"Black box testing\">black box testing</a>&nbsp;technique. Instead, those who have studied it consider it a test&nbsp;<i>approach</i>&nbsp;that can be applied to any test technique, at any stage in the development process. The key is not the test technique nor the item being tested or reviewed; the key is the cognitive engagement of the tester, and the tester&#39;s responsibility for managing his or her time.<sup class=\"reference\" id=\"cite_ref-3\" style=\"line-height: 1; font-size: 11.1999998092651px; unicode-bidi: -webkit-isolate;\"><a href=\"https://en.wikipedia.org/wiki/Exploratory_testing#cite_note-3\" style=\"text-decoration: none; color: rgb(11, 0, 128); white-space: nowrap; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;\">[3]</a></sup></p>\n	</div>\n</div>\n<p>\n	&nbsp;</p>\n'),(566,'');
/*!40000 ALTER TABLE `testsuites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text_templates`
--

DROP TABLE IF EXISTS `text_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_templates` (
  `id` int(10) unsigned NOT NULL,
  `type` smallint(5) unsigned NOT NULL,
  `title` varchar(100) NOT NULL,
  `template_data` text,
  `author_id` int(10) unsigned DEFAULT NULL,
  `creation_ts` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `is_public` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `idx_text_templates` (`type`,`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Global Project Templates';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_templates`
--

LOCK TABLES `text_templates` WRITE;
/*!40000 ALTER TABLE `text_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `text_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_point` varchar(45) NOT NULL DEFAULT '',
  `start_time` int(10) unsigned NOT NULL DEFAULT '0',
  `end_time` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `session_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` VALUES (1,'/testlink/login.php',1478784166,1478784166,0,NULL),(2,'/testlink/login.php',1478784169,1478784169,0,NULL),(3,'/testlink/login.php',1478784169,1478784169,1,'34j6n1ip80p055d40u5e39fvk4'),(4,'/testlink/lib/general/mainPage.php',1478784169,1478784169,1,'34j6n1ip80p055d40u5e39fvk4'),(5,'/testlink/lib/project/projectEdit.php',1478784177,1478784177,1,'34j6n1ip80p055d40u5e39fvk4'),(6,'/lib/issuetrackers/issueTrackerEdit.php',1478784748,1478784748,1,'34j6n1ip80p055d40u5e39fvk4'),(7,'/lib/issuetrackers/issueTrackerEdit.php',1478784765,1478784765,1,'34j6n1ip80p055d40u5e39fvk4'),(8,'/lib/issuetrackers/issueTrackerEdit.php',1478784774,1478784774,1,'34j6n1ip80p055d40u5e39fvk4'),(9,'/lib/issuetrackers/issueTrackerEdit.php',1478784780,1478784780,1,'34j6n1ip80p055d40u5e39fvk4'),(10,'/lib/issuetrackers/issueTrackerEdit.php',1478784793,1478784793,1,'34j6n1ip80p055d40u5e39fvk4'),(11,'/lib/issuetrackers/issueTrackerView.php',1478784859,1478784859,1,'34j6n1ip80p055d40u5e39fvk4'),(12,'/testlink/lib/project/projectEdit.php',1478785568,1478785568,1,'34j6n1ip80p055d40u5e39fvk4'),(13,'/testlink/lib/project/projectEdit.php',1478785570,1478785570,1,'34j6n1ip80p055d40u5e39fvk4'),(14,'/testlink/lib/project/projectEdit.php',1478787590,1478787590,1,'34j6n1ip80p055d40u5e39fvk4'),(15,'/testlink/lib/project/projectEdit.php',1478787606,1478787606,1,'34j6n1ip80p055d40u5e39fvk4'),(16,'/testlink/lib/project/projectEdit.php',1478787608,1478787608,1,'34j6n1ip80p055d40u5e39fvk4'),(17,'/testlink/lib/project/projectEdit.php',1478787635,1478787635,1,'34j6n1ip80p055d40u5e39fvk4'),(18,'/testlink/lib/plan/planEdit.php',1478787666,1478787666,1,'34j6n1ip80p055d40u5e39fvk4'),(19,'/testlink/lib/plan/planEdit.php',1478787700,1478787700,1,'34j6n1ip80p055d40u5e39fvk4'),(20,'/testlink/lib/plan/planAddTC.php',1478787718,1478787718,1,'34j6n1ip80p055d40u5e39fvk4'),(21,'/testlink/lib/plan/planAddTC.php',1478787741,1478787744,1,'34j6n1ip80p055d40u5e39fvk4'),(22,'/testlink/lib/plan/buildEdit.php',1478787796,1478787796,1,'34j6n1ip80p055d40u5e39fvk4');
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_assignments`
--

DROP TABLE IF EXISTS `user_assignments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_assignments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(10) unsigned NOT NULL DEFAULT '1',
  `feature_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned DEFAULT '0',
  `build_id` int(10) unsigned DEFAULT '0',
  `deadline_ts` datetime DEFAULT NULL,
  `assigner_id` int(10) unsigned DEFAULT '0',
  `creation_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(10) unsigned DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `user_assignments_feature_id` (`feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_assignments`
--

LOCK TABLES `user_assignments` WRITE;
/*!40000 ALTER TABLE `user_assignments` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_assignments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_group`
--

DROP TABLE IF EXISTS `user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_user_group` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_group`
--

LOCK TABLES `user_group` WRITE;
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_group_assign`
--

DROP TABLE IF EXISTS `user_group_assign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_group_assign` (
  `usergroup_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `idx_user_group_assign` (`usergroup_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_group_assign`
--

LOCK TABLES `user_group_assign` WRITE;
/*!40000 ALTER TABLE `user_group_assign` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_group_assign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_testplan_roles`
--

DROP TABLE IF EXISTS `user_testplan_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_testplan_roles` (
  `user_id` int(10) NOT NULL DEFAULT '0',
  `testplan_id` int(10) NOT NULL DEFAULT '0',
  `role_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`testplan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_testplan_roles`
--

LOCK TABLES `user_testplan_roles` WRITE;
/*!40000 ALTER TABLE `user_testplan_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_testplan_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_testproject_roles`
--

DROP TABLE IF EXISTS `user_testproject_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_testproject_roles` (
  `user_id` int(10) NOT NULL DEFAULT '0',
  `testproject_id` int(10) NOT NULL DEFAULT '0',
  `role_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`testproject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_testproject_roles`
--

LOCK TABLES `user_testproject_roles` WRITE;
/*!40000 ALTER TABLE `user_testproject_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_testproject_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(30) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `role_id` int(10) unsigned NOT NULL DEFAULT '0',
  `email` varchar(100) NOT NULL DEFAULT '',
  `first` varchar(30) NOT NULL DEFAULT '',
  `last` varchar(30) NOT NULL DEFAULT '',
  `locale` varchar(10) NOT NULL DEFAULT 'en_GB',
  `default_testproject_id` int(10) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `script_key` varchar(32) DEFAULT NULL,
  `cookie_string` varchar(64) NOT NULL DEFAULT '',
  `auth_method` varchar(10) DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_login` (`login`),
  UNIQUE KEY `users_cookie_string` (`cookie_string`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='User information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','21232f297a57a5a743894a0e4a801fc3',8,'','Testlink','Administrator','en_GB',NULL,1,NULL,'c09ad56fa193efe37fee8b064dbbe24b21232f297a57a5a743894a0e4a801fc3','');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-10 14:25:45
